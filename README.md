# Welcome to MUTANT

MUTANT (MUlTidimensional Analysis Tool) – is an open-source (GPLv3) post processing tool. 
It enables the user to select fast, robust and target-oriented data visualisation, filtering 
and assessment options for any multitude data set.  The project was launched by 
the [*Vehicle Safety Institute*](https://www.tugraz.at/institute/vsi/) at *Graz University of Technology*.

## Table of Contents
  * [Licensing](#licensing)
  * [Installation](#installation)
  * [Run MUTANT](#run MUTANT) 
  * [Example data set](#example data set)
  * [Group Assessment](#group Assessment) 
  * [References](#references)


## Licensing

Copyright (C) 2019 TU Graz


MUTANT is a free software and published under the GNU GPL v3 (see the LICENSE file),
with the aim to encourage everyone to share their code as well.

You can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 
of the License, or
(at your option) any later version.. You should have
received a copy of the GNU General Public License
along with the source code. If not, see <http://www.gnu.org/licenses/>.

MUTANT is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the [acknowledgment](ACKNOWLEDGMENT.md) for details about the used external
libraries and funding within the MUTANT software project.







You can also checkout the available source code from the gitlab repository and change it to your needs.
Feel free to contact us if you want to push your changes to the upstream.

We are continously working on the documentation of the project, which can be obtained from 
the [wiki](https://gitlab.com/VSI-TUGraz/mutant/wikis/home).



## Installation


A description of the how to install MUTANT on your local machine can be 
found [here](https://gitlab.com/VSI-TUGraz/mutant/wikis/installation).


## Run MUTANT

To Run MUTANT and test the functionalities please follow the steps described 
[here](https://gitlab.com/VSI-TUGraz/mutant/-/wikis/Run%20MUTANT).


## Example data set

Examples of already existing results can be found
[here](https://gitlab.com/VSI-TUGraz/mutant/-/wikis/Examples).

## Group Assessment

Basic ideas about the functionality of Group Assessment can be found 
[here](https://gitlab.com/VSI-TUGraz/mutant/-/wikis/Group%20Assessment).


## References




Luttenberger P, Schachner M, Rajinovic S, Moser J, Leo C, Sinz W. 2019. [MUTANT - Functionality Report Version 2.0](doc/MUTANT_Functionality_Report_Version_2.pdf) 

Luttenberger P, Schlacher L, Schachner M, Sinz W. 2019 [MUTANT - Group Assessment Report Version 1.0](doc/MUTANT_Group_Assessment_Report.pdf)
