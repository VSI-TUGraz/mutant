Acknowledgements     {#pageAcknowledgements}
================

The MUTANT software makes use of the following external libraries and tools :


- [dash plot.ly](https://plot.ly/products/dash/)
- [colorlover](https://pypi.org/project/colorlover/)

