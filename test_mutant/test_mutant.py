import unittest
import argparse
import os

from test_readfiles import TestInitGlobals


class ReadableDir(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        prospective_dir = values
        if not os.path.isdir(prospective_dir):
            raise argparse.ArgumentTypeError("readable_dir:{0} is not a valid path".format(prospective_dir))
        if os.access(prospective_dir, os.R_OK):
            setattr(namespace, self.dest, prospective_dir)
        else:
            raise argparse.ArgumentTypeError("readable_dir:{0} is not a readable dir".format(prospective_dir))


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-pub", "--pub_path", action=ReadableDir, required=True, help="path to unit test_library")
    parser.add_argument("-suite", "--suite", required=True,
                        choices=['all',
                                 'readfiles'])

    args = parser.parse_args()
    pub = args.pub_path

    test_loader = unittest.TestLoader()
    globals_init_test_classes = [TestInitGlobals]
    # assessment_test_classes = [TestStrainStressAssessment]

    if args.suite == "all":
        test_classes_to_be_run = globals_init_test_classes

    elif args.suite == "readfiles":
        test_classes_to_be_run = globals_init_test_classes

    else:
        assert False

    for test_class_to_be_run in test_classes_to_be_run:
        test_names = test_loader.getTestCaseNames(test_class_to_be_run)
        suite = unittest.TestSuite()
        for test_name in test_names:
            suite.addTest(test_class_to_be_run(test_name, pub))

        result = unittest.TextTestRunner().run(suite)

