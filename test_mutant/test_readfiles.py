
import unittest
from mutant_test_case import MutantTestCase
from utils.readfiles import Dictionary


class TestInitGlobals(MutantTestCase):
    def test_input_files(self):
        rootdir = self._path
        tmp_dict, tmp_value = Dictionary.readFromFile(rootdir)
        self.assertNotEqual(tmp_dict, None)


