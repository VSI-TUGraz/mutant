import os
import os.path

import flask
import dash
import dash_bootstrap_components as dbc

from dash import dcc
from dash import html
from dash import dash_table as dt

import numpy as np
import plotly.graph_objs as go
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression

import colorlover as cl

from utils.utils import InputForCallbacks, FilterUtils
from utils.dash_core_html_components import HTMLComponent
from utils.define_globals import Globals
from utils.group_computation import Groups
from utils.helpers import SimulationFilterMetaInformation, SimulationFilterCriteria, SelectedSimulationInfo, \
    FilteredSimulationInfo, DiagramDataGraph, CriteriaData2DScatter, CriteriaDataBoxplot, CriteriaDataGraph, \
    GroupAssessment, Button
from utils.dash_core_html_components import HTMLBase


Globals.init_globals()
Groups.init_groups()

app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])
app.title = 'Mutant, VSI'


def get_all_groups_types():
    return [{'label': 'all', 'value': 'all'}, {'label': 'none', 'value': 'none'}]


@app.server.route('/favicon.ico')
def favicon():
    return flask.send_from_directory(os.path.join(app.server.root_path, 'img'),
                                     'favicon.ico')

# **********************************************************************************************************************
#
# APP LAYOUT PURE HTML
#
#
app.layout = HTMLBase.get_layout_content()

def update_clicked_data_list(clicked_input):
    if clicked_input is not None:
        Globals.clicked_data_ = clicked_input

########################################################################################################################
# SimulationFilterMetaInformation



@app.callback(
    dash.dependencies.Output('simulation_filter_meta_information_dropdown', 'value'),
    [dash.dependencies.Input('simulation_filter_meta_information_dropdown_radiobuttons', 'value')])
def update_simulation_filter_meta_information_dropdown(simulation_filter_meta_information_dropdown_radiobuttons):
    return SimulationFilterMetaInformation.update_simulation_filter_meta_information_dropdown\
        (simulation_filter_meta_information_dropdown_radiobuttons)

########################################################################################################################
# Button

@app.callback(
    dash.dependencies.Output('simulation_filter_criteria_report_graphs_button', 'children'),
    [dash.dependencies.Input('button_report_criteria_graphs', 'n_clicks')])
def filtered_simulation_export_plt(*args):
    return Button.filtered_simulation_export_plt(*args)

@app.callback(
    dash.dependencies.Output('simulation_filter_criteria_report_criteria_button', 'children'),
    InputForCallbacks.inputs_for_report_injury(Globals.list_filter_criteria_range_slider_ids_,
                                               Globals.list_of_global_dropdown_ids_))
def filtered_simulation_export_csv(*args):
    return Button.filtered_simulation_export_csv(*args)

########################################################################################################################
# FilteredSimulationInfo

@app.callback(
    dash.dependencies.Output('simulation_number', 'children'),
    InputForCallbacks.inputs_for_statistics(Globals.list_filter_criteria_range_slider_ids_,
                                            Globals.list_of_global_dropdown_ids_))
def update_filtered_simulation_info(*args):
    return FilteredSimulationInfo.update_filtered_simulation_info(*args)

########################################################################################################################
# SelectedSimulationInfo

@app.callback(
    dash.dependencies.Output('selected_simulation_info', 'children'),
    InputForCallbacks.list_of_inputs_for_info_part())
def update_selected_simulation_info(*args):
    reset_button = args[4]
    diagram_data_graph_1_clicked_data = args[2]
    diagram_data_graph_2_clicked_data = args[3]
    criteria_data_2D_scatter_clicked_data = args[1]
    reset_clicked = False

    if reset_button > Globals.selected_simulation_info_reset_highlights_clicks_counter:
        Globals.selected_simulation_info_reset_highlights_clicks_counter = reset_button
        reset_clicked = True

    DiagramDataGraph.set_highlights(diagram_data_graph_1_clicked_data, diagram_data_graph_2_clicked_data,
                                    criteria_data_2D_scatter_clicked_data, reset_clicked)

    return SelectedSimulationInfo.update_selected_simulation_info(*args)

@app.callback(
    dash.dependencies.Output('selected_simulation_info_criteria_without_limits', 'children'),
    InputForCallbacks.inputs_for_criteria("criteria_data_graph_dropdown", "criteria_data_graph_group_by",
                                          "criteria_data_graph_dropdown_operation",
                                          'criteria_data_graph_input_visualisation_maximum',
                                          Globals.list_filter_criteria_range_slider_ids_,
                                          Globals.list_of_global_dropdown_ids_))
def update_selected_simulation_info_criteria_without_limits(*args):
    return SelectedSimulationInfo.update_selected_simulation_info_criteria_without_limits(*args)

########################################################################################################################
# SimulationFilterCriteria

@app.callback(
    dash.dependencies.Output('simulation_filter_criteria_dropdown_multiple_selection', 'value'),
    [dash.dependencies.Input('simulation_filter_criteria_dropdown_radiobuttons', 'value')] +
    [dash.dependencies.Input('simulation_filter_meta_information_dropdown', 'value')])
def update_simulation_filter_criteria_dropdown_multiple_selection(criteria_radiobutton_value, body_regions):
    return SimulationFilterCriteria.update_simulation_filter_criteria_dropdown_multiple_selection(
        criteria_radiobutton_value, body_regions)

@app.callback(
    dash.dependencies.Output('simulation_filter_criteria_sliders', 'children'),
    [dash.dependencies.Input('simulation_filter_criteria_dropdown_multiple_selection', 'value')])
def update_simulation_filter_criteria_sliders(selected_simulation_criteria):
    return SimulationFilterCriteria.update_simulation_filter_criteria_sliders(selected_simulation_criteria)

########################################################################################################################
# DiagramDataGraph

@app.callback(
    dash.dependencies.Output('diagram_data_graph_1_dropdown', 'options'),
    [dash.dependencies.Input('simulation_filter_meta_information_dropdown', 'value')])
def update_diagram_data_graph_1_dropdown_list(selected_body_regions):
    return DiagramDataGraph.update_diagram_data_graph_dropdown_list(Globals.working_dict_, selected_body_regions)


@app.callback(
    dash.dependencies.Output('diagram_data_graph_2_dropdown', 'options'),
    [dash.dependencies.Input('simulation_filter_meta_information_dropdown', 'value')])
def update_diagram_data_graph_2_dropdown_list(selected_body_regions):
    return DiagramDataGraph.update_diagram_data_graph_dropdown_list(Globals.working_dict_, selected_body_regions)

@app.callback(
    dash.dependencies.Output('diagram_data_graph_1', 'figure'),
    dash.dependencies.Input('criteria_data_2D_scatter', 'clickData'),
    dash.dependencies.Input('diagram_data_graph_1', 'clickData'),
    dash.dependencies.Input('diagram_data_graph_2', 'clickData'),
    dash.dependencies.Input('reset_button', 'n_clicks'),
    InputForCallbacks.inputs_for_graphs("diagram_data_graph_1_dropdown_x", "diagram_data_graph_1_dropdown_y",
                                        "diagram_data_graph_1_dropdown_x", "diagram_data_graph_1",
                                        "diagram_data_graph_1_dropdown", "diagram_data_graph_1_group_by",
                                        Globals.list_filter_criteria_range_slider_ids_,
                                        Globals.list_of_global_dropdown_ids_))
def update_diagram_data_graph_1(criteria_data_2D_scatter_clicked_data, diagram_data_graph_1_clicked_data,
                                diagram_data_graph_2_clicked_data, reset_button_clicks, *args):

    reset_clicked = False

    if reset_button_clicks > Globals.diagram_data_1_reset_highlights_clicks_counter:
        Globals.diagram_data_1_reset_highlights_clicks_counter = reset_button_clicks
        reset_clicked = True

    DiagramDataGraph.set_highlights(diagram_data_graph_1_clicked_data, diagram_data_graph_2_clicked_data,
                                    criteria_data_2D_scatter_clicked_data, reset_clicked)

    return DiagramDataGraph.update_diagram_data_graph(*args)

@app.callback(
    dash.dependencies.Output('diagram_data_graph_2', 'figure'),
    dash.dependencies.Input('criteria_data_2D_scatter', 'clickData'),
    dash.dependencies.Input('diagram_data_graph_1', 'clickData'),
    dash.dependencies.Input('diagram_data_graph_2', 'clickData'),
    dash.dependencies.Input('reset_button', 'n_clicks'),
    InputForCallbacks.inputs_for_graphs("diagram_data_graph_2_dropdown_x", "diagram_data_graph_2_dropdown_y",
                                        "diagram_data_graph_2_dropdown_x", "diagram_data_graph_2",
                                        "diagram_data_graph_2_dropdown", "diagram_data_graph_2_group_by",
                                        Globals.list_filter_criteria_range_slider_ids_,
                                        Globals.list_of_global_dropdown_ids_))
def update_diagram_data_graph_2(criteria_data_2D_scatter_clicked_data, diagram_data_graph_1_clicked_data,
                                diagram_data_graph_2_clicked_data, reset_button_clicks,  *args):
    reset_clicked = False

    if reset_button_clicks > Globals.diagram_data_2_reset_highlights_clicks_counter:
        Globals.diagram_data_2_reset_highlights_clicks_counter = reset_button_clicks
        reset_clicked = True

    DiagramDataGraph.set_highlights(diagram_data_graph_1_clicked_data, diagram_data_graph_2_clicked_data,
                                    criteria_data_2D_scatter_clicked_data, reset_clicked)
    return DiagramDataGraph.update_diagram_data_graph(*args)

@app.callback(
    dash.dependencies.Output('diagram_data_graph_1_dropdown', 'disabled'),
    [dash.dependencies.Input('diagram_data_graph_1_check_box_x_y', 'value')])
def update_diagram_data_graph_1_check_box_x_y_dropdown(graph_1_check_box_x_y_value):
    return DiagramDataGraph.update_diagram_data_graph_check_box_x_y(graph_1_check_box_x_y_value)


@app.callback(
    dash.dependencies.Output('diagram_data_graph_2_dropdown', 'disabled'),
    [dash.dependencies.Input('diagram_data_graph_2_check_box_x_y', 'value')])
def update_diagram_data_graph_2_check_box_x_y_dropdown(graph_2_check_box_x_y_value):
    return DiagramDataGraph.update_diagram_data_graph_check_box_x_y(graph_2_check_box_x_y_value)

@app.callback(
    dash.dependencies.Output('diagram_data_graph_1_dropdown_x', 'disabled'),
    [dash.dependencies.Input('diagram_data_graph_1_check_box_x_y', 'value')])
def update_diagram_data_graph_1_check_box_x_y_dropdown_x(graph_1_check_box_x_y_value):
    return not(DiagramDataGraph.update_diagram_data_graph_check_box_x_y(graph_1_check_box_x_y_value))

@app.callback(
    dash.dependencies.Output('diagram_data_graph_2_dropdown_x', 'disabled'),
    [dash.dependencies.Input('diagram_data_graph_2_check_box_x_y', 'value')])
def update_diagram_data_graph_2_check_box_x_y_dropdown_x(graph_2_check_box_x_y_value):
    return not(DiagramDataGraph.update_diagram_data_graph_check_box_x_y(graph_2_check_box_x_y_value))

@app.callback(
    dash.dependencies.Output('diagram_data_graph_1_dropdown_y', 'disabled'),
    [dash.dependencies.Input('diagram_data_graph_1_check_box_x_y', 'value')])
def update_diagram_data_graph_1_check_box_x_y_dropdown_y(graph_1_check_box_x_y_value):
    return not(DiagramDataGraph.update_diagram_data_graph_check_box_x_y(graph_1_check_box_x_y_value))

@app.callback(
    dash.dependencies.Output('diagram_data_graph_2_dropdown_y', 'disabled'),
    [dash.dependencies.Input('diagram_data_graph_2_check_box_x_y', 'value')])
def update_diagram_data_graph_2_check_box_x_y_dropdown_x(graph_2_check_box_x_y_value):
    return not(DiagramDataGraph.update_diagram_data_graph_check_box_x_y(graph_2_check_box_x_y_value))

@app.callback(
    dash.dependencies.Output('diagram_data_graph_1_dropdown_y', 'options'),
    [dash.dependencies.Input('diagram_data_graph_1_dropdown_x', 'value')])
def update_diagram_data_graph_1_dropdown_y(graph_1_dropdown_x_value):
    return DiagramDataGraph.update_diagram_data_graph_dropdown_x_y(graph_1_dropdown_x_value)

@app.callback(
    dash.dependencies.Output('diagram_data_graph_2_dropdown_y', 'options'),
    [dash.dependencies.Input('diagram_data_graph_2_dropdown_x', 'value')])
def update_diagram_data_graph_2_dropdown_y(graph_2_dropdown_x_value):
    return DiagramDataGraph.update_diagram_data_graph_dropdown_x_y(graph_2_dropdown_x_value)

@app.callback(
    dash.dependencies.Output('diagram_data_graph_1_dropdown_x', 'options'),
    [dash.dependencies.Input('diagram_data_graph_1_dropdown_y', 'value')])
def update_diagram_data_graph_1_dropdown_x(graph_1_dropdown_y_value):
    return DiagramDataGraph.update_diagram_data_graph_dropdown_x_y(graph_1_dropdown_y_value)

@app.callback(
    dash.dependencies.Output('diagram_data_graph_2_dropdown_x', 'options'),
    [dash.dependencies.Input('diagram_data_graph_2_dropdown_y', 'value')])
def update_diagram_data_graph_2_dropdown_x(graph_2_dropdown_y_value):
    return DiagramDataGraph.update_diagram_data_graph_dropdown_x_y(graph_2_dropdown_y_value)

########################################################################################################################
# CriteriaDataBoxplot

@app.callback(
    dash.dependencies.Output('criteria_data_boxplot_dropdown', 'options'),
    [dash.dependencies.Input('criteria_data_boxplot_dropdown', 'value')])
def update_criteria_data_boxplot_dropdown_limit(criteria_data_boxplot_dropdown_value):
    return CriteriaDataBoxplot.update_criteria_data_boxplot_dropdown_limit(criteria_data_boxplot_dropdown_value)


@app.callback(
    dash.dependencies.Output('criteria_data_boxplot', 'figure'),
    InputForCallbacks.list_of_box_plot_inputs(Globals.list_filter_criteria_range_slider_ids_,
                                              Globals.list_of_global_dropdown_ids_))
def update_criteria_data_boxplot(*args):
    return CriteriaDataBoxplot.update_criteria_data_boxplot(*args)

@app.callback(
    dash.dependencies.Output('helper_div_2', 'children'),
    [dash.dependencies.Input('criteria_data_boxplot', 'clickData')])
def update_clicked_data_list_criteria_data_boxplot(box_plot_hover_graph_clicked_input):
    return update_clicked_data_list(box_plot_hover_graph_clicked_input)

########################################################################################################################
# CriteriaData2DScatter

@app.callback(
    dash.dependencies.Output('criteria_data_2D_scatter_dropdown_x', 'options'),
    [dash.dependencies.Input('criteria_data_2D_scatter_dropdown_y', 'value')])
def update_criteria_data_2D_scatter_dropdown_x_list(selector_2D_2_value):
    return CriteriaData2DScatter.update_criteria_data_2D_scatter_dropdown_list(selector_2D_2_value)

@app.callback(
    dash.dependencies.Output('criteria_data_2D_scatter_dropdown_y', 'options'),
    [dash.dependencies.Input('criteria_data_2D_scatter_dropdown_x', 'value')])
def update_criteria_data_2D_scatter_dropdown_y_list(selector_2D_1_value):
    return CriteriaData2DScatter.update_criteria_data_2D_scatter_dropdown_list(selector_2D_1_value)

@app.callback(
    dash.dependencies.Output('criteria_data_2D_scatter', 'figure'),
    dash.dependencies.Input('criteria_data_2D_scatter', 'clickData'),
    dash.dependencies.Input('diagram_data_graph_1', 'clickData'),
    dash.dependencies.Input('diagram_data_graph_2', 'clickData'),
    dash.dependencies.Input('reset_button', 'n_clicks'),
    InputForCallbacks.list_of_2d_graph_inputs(Globals.list_filter_criteria_range_slider_ids_,
                                              Globals.list_of_global_dropdown_ids_))
def update_clicked_data_list_criteria_data_2D_scatter(criteria_data_2D_scatter, diagram_data_graph_1,
                                                      diagram_data_graph_2, reset_button,  *args):
    reset_clicked = False

    if reset_button > Globals.criteria_data_2D_scatter_reset_highlights_clicks_counter:
        Globals.criteria_data_2D_scatter_reset_highlights_clicks_counter = reset_button
        reset_clicked = True

    DiagramDataGraph.set_highlights(diagram_data_graph_1, diagram_data_graph_2, criteria_data_2D_scatter, reset_clicked)
    return CriteriaData2DScatter.update_criteria_data_2D_scatter(*args)

########################################################################################################################
# CriteriaDataGraph

@app.callback(
    dash.dependencies.Output('criteria_data_graph_dropdown', 'value'),
    [dash.dependencies.Input('criteria_data_graph_radiobutton', 'value')])
def update_criteria_data_graph_dropdown_multiple_selection(criteria_graph_radiobutton_value):
    return CriteriaDataGraph.update_criteria_data_graph_dropdown_multiple_selection(criteria_graph_radiobutton_value)

@app.callback(
    dash.dependencies.Output('criteria_data_graph', 'figure'),
    InputForCallbacks.inputs_for_criteria("criteria_data_graph_dropdown", "criteria_data_graph_group_by",
                                          "criteria_data_graph_dropdown_operation",
                                          'criteria_data_graph_input_visualisation_maximum',
                                          Globals.list_filter_criteria_range_slider_ids_,
                                          Globals.list_of_global_dropdown_ids_))
def update_criteria_data_graph(*args):
    return CriteriaDataGraph.update_criteria_data_graph(*args)

########################################################################################################################
# GroupAssessment

@app.callback(dash.dependencies.Output('group_assessment_in_depth_tables', 'children'),
              [dash.dependencies.Input('group_assessment_in_depth_dropdown', 'value')] +
              InputForCallbacks.inputs_for_table(Globals.list_filter_criteria_range_slider_ids_,
                                                 Globals.list_of_global_dropdown_ids_))
def display_in_depth_tables(selected_groups, *args):
    return GroupAssessment.display_in_depth_tables(selected_groups, *args)

@app.callback(dash.dependencies.Output('group_assessment_tables_div', 'children'),
              [dash.dependencies.Input('group_assessment_groups_dropdown', 'value')] +
              InputForCallbacks.inputs_for_table(Globals.list_filter_criteria_range_slider_ids_,
                                                 Globals.list_of_global_dropdown_ids_))
def display_tables(selected_groups, *args):
    return GroupAssessment.display_tables(selected_groups, *args)

@app.callback(
    dash.dependencies.Output('group_assessment_groups_dropdown', 'value'),
    [dash.dependencies.Input('group_assessment_filter_groups_radiobutton', 'value')])
def update_dropdown_groups_dropdown(groups_radiobutton_value):
    return GroupAssessment.get_group_in_dropdown_by_radio_button_selector(groups_radiobutton_value)

@app.callback(
    dash.dependencies.Output('group_assessment_groups_button', 'children'),
    [dash.dependencies.Input('group_assessment_report_groups_button', 'n_clicks')] +
    InputForCallbacks.inputs_for_table(Globals.list_filter_criteria_range_slider_ids_,
                                       Globals.list_of_global_dropdown_ids_))
def export(n_clicks, *args):
    return GroupAssessment.group_assessment_export__csv(n_clicks, *args)

########################################################################################################################
if __name__ == '__main__':
    app.run_server(port=Globals.args.server_port, debug=False)
