import dash
import numpy as np
from utils.define_globals import Globals
from utils.constants import DataDictionaryConstants


class InputForCallbacks:

    @staticmethod
    def inputs_for_statistics(list_injury_criteria_range_slider_ids, list_of_dropdowns):
        """ Merges the two lists to create an input list for the callback function that updates the filtered simulation
        info section.
        Args:
            list_injury_criteria_range_slider_ids (list): List of criteria from the input file
            list_of_dropdowns (list): List of values that can be selected in the "Group by" dropdown
        Returns:
            ls (list): Result of merging the injury criteria slider ids list and dropdowns lists
        """
        ls = []
        ls += [dash.dependencies.Input(dropdown, 'value') for dropdown in list_of_dropdowns]
        ls += [dash.dependencies.Input(criteria, 'value') for criteria in list_injury_criteria_range_slider_ids]
        return ls

    @staticmethod
    def inputs_for_table(list_injury_criteria_range_slider_ids, list_of_dropdowns):
        """ Adds the two lists on simulation_filter_criteria_dropdown_multiple_selection dash input to create an input
         list for the callback function that updates table.
        Args:
            list_injury_criteria_range_slider_ids (list): List of criteria from the input file
            list_of_dropdowns (list): List of values that can be selected in the "Group by" dropdown
        Returns:
            ls (list): Result of adding the injury criteria slider ids list and dropdowns lists on
            simulation_filter_criteria_dropdown_multiple_selection dash input
        """
        ls = []
        ls.append(dash.dependencies.Input('simulation_filter_criteria_dropdown_multiple_selection', 'value'))
        ls += [dash.dependencies.Input(dropdown, 'value') for dropdown in list_of_dropdowns]
        ls += [dash.dependencies.Input(criteria, 'value') for criteria in list_injury_criteria_range_slider_ids]
        return ls

    @staticmethod
    def inputs_for_report_injury(list_injury_criteria_range_slider_ids, list_of_dropdowns):
        """ Adds the two lists on button_report_criteria dash input to create an input list for the callback function
        that updates table.
        Args:
            list_injury_criteria_range_slider_ids (list): List of criteria from the input file
            list_of_dropdowns (list): List of values that can be selected in the "Group by" dropdown
        Returns:
            ls (list): Result of adding the injury criteria slider ids list and dropdowns lists on
            button_report_criteria dash input
        """
        ls = []
        ls.append(dash.dependencies.Input('button_report_criteria', 'n_clicks'))
        ls += [dash.dependencies.Input(dropdown, 'value') for dropdown in list_of_dropdowns]
        ls += [dash.dependencies.Input(criteria, 'value') for criteria in list_injury_criteria_range_slider_ids]
        return ls

    @staticmethod
    def inputs_for_graphs(name_diagram_dropdown_x, name_diagram_dropdown_y, name_diagram_dropdown_checkbox, graph_name,
                          diagram_dropdown, diagram_legend,
                          list_injury_criteria_range_slider_ids, list_of_dropdowns):
        """ Creates a list that is used as callback input to update the data graph diagram.
        Args:
            name_diagram_dropdown_x (str): Dropdown to select x values for the corresponding graph
            (diagram_data_graph_1_dropdown_x or diagram_data_graph_2_dropdown_x)
            name_diagram_dropdown_y (str): Dropdown to select y values for the corresponding graph
            diagram_data_graph_1_dropdown_y or diagram_data_graph_2_dropdown_y
            name_diagram_dropdown_checkbox (str): Checkbox for diagram data graph 1 or diagram data graph 2
            graph_name (str): Name of graph (diagram_data_graph_1 or diagram_data_graph_2)
            diagram_dropdown (str): Dropdown to select criteria (diagram_data_graph_1_dropdown or
            diagram_data_graph_1_dropdown)
            diagram_legend (str): Dropdown to select 'Group by' option (diagram_data_graph_1_group_by or
            diagram_data_graph_2_group_by)
            list_injury_criteria_range_slider_ids (list): List of criteria from the input file
            list_of_dropdowns (list): List of values that can be selected in the "Group by" dropdown
        Returns:
            ls (list): Input list for callback function for updating diagram data graph (all inputs that affect
            a given graph are added to the list, because when something changes in one of the components from the
            input list, the graph needs to be updated)
        """
        name_second = 'diagram_data_graph_2' if graph_name == 'diagram_data_graph_1' else 'diagram_data_graph_1'
        ls = [dash.dependencies.Input(diagram_dropdown, 'value')]
        ls.append(dash.dependencies.Input('simulation_filter_criteria_dropdown_multiple_selection', 'value'))
        ls.append(dash.dependencies.Input(name_second, 'clickData'))
        ls.append(dash.dependencies.Input('criteria_data_boxplot', 'clickData'))
        ls.append(dash.dependencies.Input('criteria_data_2D_scatter', 'clickData'))
        ls.append(dash.dependencies.Input(name_diagram_dropdown_x, 'value'))
        ls.append(dash.dependencies.Input(name_diagram_dropdown_y, 'value'))
        ls.append(dash.dependencies.Input(name_diagram_dropdown_checkbox, 'disabled'))
        ls.append(dash.dependencies.Input(graph_name, 'clickData'))
        ls.append(dash.dependencies.Input(diagram_legend, 'value'))
        ls.append(dash.dependencies.Input('reset_button', 'n_clicks'))
        ls += [dash.dependencies.Input(dropdown, 'value') for dropdown in list_of_dropdowns]
        ls += [dash.dependencies.Input(criteria, 'value') for criteria in list_injury_criteria_range_slider_ids]
        return ls

    @staticmethod
    def inputs_for_criteria(criteria_data_graph_dropdown, criteria_data_graph_dropdown_group_by,
                            criteria_data_graph_dropdown_operation, criteria_data_graph_input_visualisation_maximum,
                            list_injury_criteria_range_slider_ids, list_of_dropdowns):
        """ Creates a list that is used as callback input to update the criteria data graph.
        Args:
            criteria_data_graph_dropdown (str): Name of dropdown to select criteria
            criteria_data_graph_dropdown_group_by (str): Name of dropdown to select 'Group by' option
            criteria_data_graph_dropdown_operation (str): Name of dropdown to operation
            criteria_data_graph_input_visualisation_maximum (str): Name of select section for visualisation maximum
            list_injury_criteria_range_slider_ids (list): List of criteria from the input file
            list_of_dropdowns (list): List of values that can be selected in the "Group by" dropdown
        Returns:
            ls (list): Input list for callback function for updating criteria data graph (all inputs that affect
            a given graph are added to the list, because when something changes in one of the components from the
            input list, the graph needs to be updated)
        """
        ls = [dash.dependencies.Input(criteria_data_graph_dropdown, 'value')]
        ls += [dash.dependencies.Input(criteria_data_graph_dropdown_group_by, 'value')]
        ls += [dash.dependencies.Input(criteria_data_graph_dropdown_operation, 'value')]
        ls += [dash.dependencies.Input(criteria_data_graph_input_visualisation_maximum, 'value')]
        ls += [dash.dependencies.Input(dropdown, 'value') for dropdown in list_of_dropdowns]
        ls += [dash.dependencies.Input(criteria, 'value') for criteria in list_injury_criteria_range_slider_ids]
        return ls

    @staticmethod
    def list_of_box_plot_inputs( list_injury_criteria_range_slider_ids, list_of_dropdowns):
        """ Creates a list that is used as callback input to update the box plot graph.
        Args:
            list_injury_criteria_range_slider_ids (list): List of criteria from the input file
            list_of_dropdowns (list): List of values that can be selected in the "Group by" dropdown
        Returns:
            ls (list): Input list for callback function for updating box plot graph (all inputs that affect a given
            graph are added to the list, because when something changes in one of the components from the input list,
            the graph needs to be updated)
        """
        ls = [dash.dependencies.Input('criteria_data_boxplot_dropdown', 'value')]
        ls.append(dash.dependencies.Input('simulation_filter_criteria_dropdown_multiple_selection', 'value'))
        ls.append(dash.dependencies.Input('criteria_data_boxplot', 'clickData'))

        ls += [dash.dependencies.Input(dropdown, 'value') for dropdown in list_of_dropdowns]
        ls += [dash.dependencies.Input(criteria, 'value') for criteria in list_injury_criteria_range_slider_ids]
        return ls

    @staticmethod
    def list_of_2d_graph_inputs(list_injury_criteria_range_slider_ids, list_of_dropdowns):
        """ Creates a list that is used as callback input to update the criteria 2D scatter graph.
        Args:
            list_injury_criteria_range_slider_ids (list): List of criteria from the input file
            list_of_dropdowns (list): List of values that can be selected in the "Group by" dropdown
        Returns:
            ls (list): Input list for callback function for updating criteria 2D scatter graph (all inputs that affect
            a given graph are added to the list, because when something changes in one of the components from the
            input list, the graph needs to be updated)
        """
        ls = [dash.dependencies.Input('criteria_data_2D_scatter_group_by', 'value')]
        ls.append(dash.dependencies.Input('criteria_data_2D_scatter_dropdown_x', 'value'))
        ls.append(dash.dependencies.Input('criteria_data_2D_scatter_dropdown_y', 'value'))
        ls.append(dash.dependencies.Input('simulation_filter_criteria_dropdown_multiple_selection', 'value'))
        ls.append(dash.dependencies.Input('criteria_data_2D_scatter', 'clickData'))
        ls.append(dash.dependencies.Input('diagram_data_graph_1', 'clickData'))
        ls.append(dash.dependencies.Input('diagram_data_graph_2', 'clickData'))
        ls.append(dash.dependencies.Input('criteria_data_boxplot', 'clickData'))
        ls.append(dash.dependencies.Input('reset_button', 'n_clicks'))
        #ls.append(dash.dependencies.Input('2D_legend_dropdown', 'value'))
        #ls.append(dash.dependencies.Input('2D_linear_regression', 'value'))
        #ls.append(dash.dependencies.Input('2D_R_squared', 'value'))
        ls += [dash.dependencies.Input(dropdown, 'value') for dropdown in list_of_dropdowns]
        ls += [dash.dependencies.Input(criteria, 'value') for criteria in list_injury_criteria_range_slider_ids]
        return ls

    @staticmethod
    def list_of_2d_r_squared_inputs(list_injury_criteria_range_slider_ids, list_of_dropdowns):
        ls = [dash.dependencies.Input('selector_2D_1', 'value')]
        ls.append(dash.dependencies.Input('selector_2D_2', 'value'))
        ls.append(dash.dependencies.Input('2D_linear_regression', 'value'))
        ls += [dash.dependencies.Input(dropdown, 'value') for dropdown in list_of_dropdowns]
        ls += [dash.dependencies.Input(criteria, 'value') for criteria in list_injury_criteria_range_slider_ids]
        return ls

    # def list_of_pdf_report_inputs(list_injury_criteria_range_slider_ids, list_of_dropdowns):
    #     ls = [dash.dependencies.Input('pdf_button', 'n_clicks')]
    #     ls+= [dash.dependencies.Input('tables_div', 'children')]
    #     ls += [dash.dependencies.Input('groups_dropdown', 'value')]
    #     ls += [dash.dependencies.Input(dropdown, 'value') for dropdown in list_of_dropdowns]
    #     ls += [dash.dependencies.Input(criteria, 'value') for criteria in list_injury_criteria_range_slider_ids]
    #     return ls

    @staticmethod
    def list_of_inputs_for_info_part():
        """ Creates an input list for the selected simulation info part.
        Returns:
            ls (list): List of inputs that affect the update of the info part (clicked data on graphs and reset button)
        """
        ls = [dash.dependencies.Input('criteria_data_boxplot', 'clickData')]
        ls.append(dash.dependencies.Input('criteria_data_2D_scatter', 'clickData'))
        ls.append(dash.dependencies.Input('diagram_data_graph_1', 'clickData'))
        ls.append(dash.dependencies.Input('diagram_data_graph_2', 'clickData'))
        ls.append(dash.dependencies.Input('reset_button', 'n_clicks'))
        return ls

class SliderUtils:
    # ******************************************************************************************************************
    # input:    ls = [0.02, 0.067636, 0.0753]
    # output:   dictionary with min, max, value labels, etc
    #
    @staticmethod
    def get_slider_config(ls):
        """ Creates a list that is used as callback input to update the criteria 2D scatter graph.
        Args:
            ls (list): List with data from input files
        Returns:
            d (dict): Dictionary with data on the given slider (min and max, as well as the range of values to be
            marked)
        """
        rounding_digits = 5
        label_stepsize = 10 ** ((-1) * rounding_digits)

        # calculate the label step size, dpending on the values of the list
        # general the list should be less or equal to 10
        # important:
        #       steps should be in the decadal ... meaning it should be divisible through 10**(x) x [-5,5]
        #Todo: Rounding is wrong, rounds the upper limit to far down and the lower limit to far up.
        while True:
            rounded_minimum = round(np.nanmin(ls), rounding_digits)
            rounded_maximum = round(np.nanmax(ls), rounding_digits)
            nr_labels = (rounded_maximum + label_stepsize - rounded_minimum) / label_stepsize
            if nr_labels > 11:
                rounding_digits -= 1
                label_stepsize = 10 ** ((-1) * rounding_digits)

            else:
                if np.nanmax(ls) > rounded_maximum:
                    rounded_maximum += label_stepsize
                if np.nanmin(ls) < rounded_minimum:
                    rounded_minimum -= label_stepsize
                break

        labels = np.arange(rounded_minimum, rounded_maximum + label_stepsize, label_stepsize)

        # avoids overlapping of labels at the beginning and the end
        if np.abs(np.min(labels) - np.min(ls)) > 0.4 * label_stepsize and len(labels) < 5:
            labels = np.append(labels, np.nanmin(ls))

        if np.abs(np.max(labels) - np.max(ls)) > 0.4 * label_stepsize and len(labels) < 5:
            labels = np.append(labels, np.nanmax(ls))

        labels.sort()

        # the amount of digits to be displayed should be variable and coresponds to the length of the number
        format_ = '%.' + str(int(np.log10(label_stepsize) * -1) + 1) + 'f' if np.log10(label_stepsize) < 1 else '%.0f'

        #
        #
        marks = {i if i % 1 != 0 else int(i): {'label': format_ % i if i % 1 != 0 else str(int(i))} for i in
                 list(labels)}

        #
        # case of no variance in the data min and max are almost equal
        #
        if max(labels) - min(labels) < 0.0001:
            marks = {min(labels) if min(labels) % 1 != 0 else int(min(labels)): {
                'labels': format_ % min(labels) if min(labels) % 1 != 0 else str(int(min(labels)))}}

        d = {
            'marks': marks,  # marks that should be shown for adjustment
            'min': min(marks) if max(marks) - min(marks) > 0.00001 else max(marks),  # minimum value for the slider
            'max': max(marks),  # maximum value for the slider
            'step': label_stepsize * 0.01,
            'value': [min(ls), max(ls)]  # value that is initially set on the slider
        }
        return d

class FilterUtils:
    # input     : dictionary of considered criteria:
    #               numeric : lower and upper values
    #               string  : list of selected strings
    # return    : list of simulation ids, which meet filter criteria
    #

    #
    # def get_boolean_filter_dict_for_criteria(d_global_dropdown, d_global_slider, d_injury):
    #
    @staticmethod
    def get_boolean_filter_dict_for_criteria(_list_injury_criteria_range_slider_ids_,
                                             _list_of_global_dropdown_ids,
                                             working_dict,
                                             criteria_slider_values, global_dropdown_values):
        """ Creates a dictionary with boolean values depending on the corresponding input parameters
        Args:
            _list_injury_criteria_range_slider_ids_ (list): List of criteria from the input file
            _list_of_global_dropdown_ids (list): List of values that can be selected in the "Group by" dropdown
            working_dict (dict): Dictionary with data from input files
            criteria_slider_values (tuple): Tuple with values from criteria input file
            global_dropdown_values (tuple): Tuple with groups how data can be grouped depending on the selected
            option from "Group by" dropdown
        Returns:
            {**boolean_global_dropdown, **boolean_injury} (dict): Dictionary with data from
            _list_injury_criteria_range_slider_ids_ and criteria_slider_values with array with boolean values for each
            of the corresponding elements depending on input values.
        """
        assert (len(_list_injury_criteria_range_slider_ids_) == len(criteria_slider_values))
        if len(global_dropdown_values) == 0:
            global_dropdown_values = [[]] * len(_list_of_global_dropdown_ids)
        assert (len(_list_of_global_dropdown_ids) == len(global_dropdown_values))

        # define the dictionaries for the filters
        d_injury = {i: values for (i, values) in zip(_list_injury_criteria_range_slider_ids_, criteria_slider_values)}

        d_global_dropdown = {i.replace("dropdown_", ""): values for (i, values) in
                             zip(_list_of_global_dropdown_ids, global_dropdown_values)}

        nr_simulation = Globals.getNumOfSimulation()

        # TODO
        # same routine!

        boolean_global_dropdown = {key: np.full((nr_simulation), True) for key in d_global_dropdown.keys()}
        for key in boolean_global_dropdown.keys():
            for index, key_wd in enumerate(working_dict[key]):  # todo might also effect other simulations
                if key_wd is None:
                    continue
                boolean_global_dropdown[key][index] = key_wd in d_global_dropdown[key]

        boolean_injury = {key: np.full((nr_simulation), True) for key in d_injury.keys()}
        for key in boolean_injury.keys():
            body_region = key.split(':')[0]
            injury_crit = key.split(':')[1]
            for index, key_wd in enumerate(working_dict[DataDictionaryConstants.PARENT_PART][body_region]
                                           [DataDictionaryConstants.DICT_KEY_CRITERIA][injury_crit][
                                               DataDictionaryConstants.DATA_KEY_VALUE]):  # todo might also effect other simulations
                if key_wd is None or d_injury[key][0] is None:
                    continue
                boolean_injury[key][index] = d_injury[key][0] <= key_wd <= d_injury[key][1]
        return {**boolean_global_dropdown, **boolean_injury}
