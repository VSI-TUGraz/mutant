
class DataDictionaryConstants(object):
    PARENT_PART = "PARENT_PART"
    BODY_REGION_HEAD = "HEAD"
    BODY_REGION_NECK = "NECK"
    BODY_REGION_PELVIS = "PELVIS"
    BODY_REGION_CHEST = "CHEST"
    BODY_REGION_FEMUR = "FEMUR"
    BODY_REGION_KNEE = "KNEE"
    BODY_REGION_TIBIA = "TIBIA"
    BODY_REGION_BELT = "BELT"

    DATA_KEY_VALUE = "Value"
    DATA_KEY_TIME = "Time"
    DATA_KEY_RISK = "Risk"
    DATA_KEY_TYPE = "Type"
    DATA_KEY_LIMITS = "Limits"
    DATA_KEY_WEIGHT = "Weight"

    DICT_KEY_CRITERIA = "CRITERIA"
    DICT_KEY_DIAGRAMS = "DIAGRAMS"
    DICT_KEY_ONE_COLUMN = "ONE_COLUMN"
    DICT_KEY_SIM_NAME = "SimulationName"
    DICT_KEY_PJOINT = "PJoint"
    DICT_KEY_LOADCASE = "Loadcase"
    DICT_KEY_SUBMARINING = "Submarining"
    VISUAL_ASSESSMENT = "Visual_Assessment"
    DICT_KEY_WARNING = "Warning"
    DICT_KEY_LOADCASE_MODIFIER = "Loadcase_Modifier"
    ON = "ON"
    OFF = "OFF"
    YES = "YES"
    NO = "NO"
    PJOINT_OVER_BASELINERISK = "PJoint / Baseline_risk"
    DICT_KEY_LIMIT_SENSITIVITY = "Limit_Sensitivity"
    DICT_KEY_MODEL_INFO = "Model_Info"
    DICT_KEY_H350 = "H350"
    DICT_KEY_THUMS = "Thums"
    DICT_KEY_WS = "WS"
    DICT_KEY_LCM_GROUPS = "LCM_Groups"


    GROUP_1 = "Group1"
    GROUP_2 = "Group2"
    GROUP_3 = "Group3"
    GROUP_4 = "Group4"
    GROUP_5 = "Group5"
    GROUP_6 = "Group6"
    GROUP_7 = "Group7"
    GROUP_8 = "Group8"
    OVERALL = "Overall"
    SIMULATIONS = "Simulations"
    LOADCASE_RATING = "Loadcase Rating"
    NAME = "Name"
    RATING_STEPS = "Rating_Steps"
    LOADCASE = "Loadcase"
    LOADCASE_LCA = "Loadcase (LCA)"
    BASELINE_RISK = "Baseline_Risk"
    MODIFIED_RATING = "Modified_Rating"
    STAR_RATING = "Star Rating"
    MOD_RATING_LIMITS = "Modified_Rating_Limits"

    INJURY_CRITERIA = "INJURY"
    LOAD_CRITERIA = "LOAD"
    KINEMATIC_CRITERIA = "KINEMATIC"

    FILE = "file"

class DropdownConstants():
    BEST_WORST = 'best-worst'
    MAX_MIN = 'max-min'
    MAX_MEAN = 'max-mean'
    MEAN_MIN = 'mean-min'

class ParsingConstants():
    SIMULATIONS = "simulations"
    DEST_DIR = "destination_dir"
    GROUPS_DIR = "groups_dir"
    SIM = "sim"
    SCRIPT_FILE = "script_file"
    LIMITS_FILE = "criteria_limits_file"