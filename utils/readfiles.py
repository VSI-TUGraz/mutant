'''
  This file_name is used to read from dictionary and files,
  Input: csv files
  Output: dictionary with all neccessery files

'''
import os
import pandas as pd
import numpy as np
import ast
from utils.constants import DataDictionaryConstants
import pint
from pint import UnitRegistry
import json
import csv
import copy


class Dictionary:

    @staticmethod
    def _get_infos_from_header_crit_string(crit_info_string, limits_dict = None):
        """ Creates a list of specific criteria from the input file so that the list contains the following elements:
        'Part of', 'identifier', 'Name' and limits
        Args:
            crit_info_string (str): Criteria from the input file in the form: 'Part of:identifier:Name'
            limits_dict (dict): Dictionary with data from limits input file.
        Returns:
            to_return (list): List with 'Part of', 'identifier', 'Name' and limits as elements
        """
        crit_info = crit_info_string.split(':')
        if limits_dict is not None:
            if (crit_info[0] in limits_dict.keys()) \
            and (crit_info[1] in limits_dict[crit_info[0]].keys())\
            and (crit_info[2] in limits_dict[crit_info[0]][crit_info[1]]):
                crit_info.append(str(limits_dict[crit_info[0]][crit_info[1]][crit_info[2]]))
        _parsing_def = [str, str, str, ast.literal_eval]
        to_return = [None] * len(_parsing_def)
        for i, (info, parse_to) in enumerate(zip(crit_info, _parsing_def)):
            to_return[i] = parse_to(info)
        return to_return

    #Check if number is digit
    @staticmethod
    def _is_float(value):
        """ Checks whether the value is float or not and returns True or False depending on it.
        Args:
            value (str or float): Variable whose type needs to be checked
        Returns:
            True or False: True if value is float, otherwise False
        """
        try:
            float(value)
            return True
        except ValueError:
            return False

    @staticmethod
    def harmonize_unitsin_dynasaur_data_dict(data_dynasaur_dict):
        """
        Args:
            data_dynasaur_dict (dict): Dictionary in which the data from the input files are loaded
        """
        ureg = UnitRegistry()
        parent_parts = data_dynasaur_dict[DataDictionaryConstants.PARENT_PART]
        for body_region in parent_parts:
            diags_dict = parent_parts[body_region][DataDictionaryConstants.DICT_KEY_DIAGRAMS]
            for diag_name in diags_dict:
                for data_cont in diags_dict[diag_name]:
                    try:
                        Q_ = ureg.Quantity
                        x = Q_(data_cont["x"], data_cont["x_unit"])
                        x_conv = x.to_base_units()
                        data_cont["x"] = np.array(x_conv)
                        data_cont["x_unit"] = str(x_conv.units)
                    except (pint.errors.DimensionalityError, ValueError):
                        pass
                    try:
                        Q_ = ureg.Quantity
                        y = Q_(data_cont["y"], data_cont["y_unit"])
                        y_conv = y.to_base_units()
                        data_cont["y"] = np.array(y_conv)
                        data_cont["y_unit"] = str(y_conv.units)
                    except (pint.errors.DimensionalityError, ValueError, pint.errors.UndefinedUnitError):
                        pass


    @staticmethod
    def check_limits_json(temp_dict):
        """ Checks the name of the json file (ie checks the form of the dictionary in which the json file is loaded).
        Args:
            temp_dict (dict): Dictionary where data from json file is stored
        Returns:
            True or False: If the file does not have the required form then the function returns False,
             otherwise it returns True.
        """
        for key in temp_dict.keys():
            if not isinstance(key, str):
                print(f"Error parsing limits file - {key} type not string")
                return False
            for innerkey in temp_dict[key].keys():
                if not isinstance(innerkey, str):
                    print(f"Error parsing limits file - {innerkey} type not string")
                    return False
                for innerinnerkey in temp_dict[key][innerkey].keys():
                    if not isinstance(innerinnerkey, str):
                        print(f"Error parsing limits file - {innerinnerkey} type not string")
                        return False
                    if not isinstance(temp_dict[key][innerkey][innerinnerkey], list):
                        print(f"Error parsing limits file - {temp_dict[key][innerkey][innerinnerkey]} type not list")
                        return False
        return True

    @staticmethod
    def checkEmptyFile(file_path):
        """ Checks the form of the info file.
        Args:
            file_path (str): Path where the file to be checked is located
        Returns:
            True or False: If the file is empty then the function returns False,
             otherwise it returns True.
        """
        file_name = os.path.basename(os.path.splitext(file_path)[0])
        if os.path.getsize(file_path) <= 0:
            print(f"{file_name} file is empty (file path: " + str(file_path) + ")")
            return True
        return False

    @staticmethod
    def checkFileFormat(df, file_path):
        """ Returns whether the diagram or criteria (depending on the parameter passed) file
        is valid by checking the data types in the dataframe.
        Args:
             df (pandas DataFrame): DataFrame with data from input file
            file_path (str): Path where the file to be checked is located
        Returns:
            True or False: If the file does not have the required form then the function returns False,
             otherwise it returns True.
        """
        file = open(file_path)
        file_name = os.path.basename(os.path.splitext(file_path)[0])
        reader = csv.reader(file)
        num_of_lines = len(list(reader))
        if file_name == "criteria":
            if (num_of_lines < 3):
                print(f"Error parsing {file_name} file - missing rows (file path: " + str(file_path) + ")")
                return False
        elif file_name == "diagram":
            if (num_of_lines < 4):
                print(f"Error parsing {file_name} file - missing rows (file path: " + str(file_path) + ")")
                return False
        for keys in df.keys():
            tmp_keys = keys
            for key in tmp_keys:
                if 'Unnamed' in key:
                    print(f"Error parsing {file_name} file - missing key! (file path: " + str(file_path) + ")")
                    return False
                if Dictionary._is_float(key):
                    print(f"Error parsing {file_name} file - {key} is float, key should be string! (file path: "
                          + str(file_path) + ")")
                    return False
                if not isinstance(key, str):
                    print(f"Error parsing {file_name} file - {key} type not string (file path: "
                          + str(file_path) + ")")
                    return False
            if file_name == "criteria":
                if not isinstance(df[keys][0], str):
                    print(f"Error parsing {file_name} file - {df[keys][0]} type not string (file path: "
                          + str(file_path) + ")")
                    return False
                if not Dictionary._is_float(df[keys][1]):
                    print(f"Error parsing {file_name} file - {df[keys][0]} type not float (file path: "
                          + str(file_path) + ")")
                    return False

            if file_name == "diagram" and not Dictionary._is_float(df[keys][0]):
                        print(f"Error parsing {file_name} file - {df[keys][0]} type not float")
                        return False
            for value in df[keys][1:]:
                if not Dictionary._is_float(value):
                    print(f"Error parsing {file_name} file - {value} type not float")
                    return False
        return True

    @staticmethod
    def checkInfoFile(df, file_path):
        """ Checks the form of the info file.
        Args:
            df (pandas Data Frame): Data Frame with data from input info file
            file_path (str): Path where the file to be checked is located
        Returns:
            True or False: If the file does not have the required form then the function returns False,
             otherwise it returns True.
        """
        if os.path.getsize(file_path) <= 0:
            print("Info file is empty (file path: " + str(file_path) + ")")
            return False
        if (len(df.keys()) < 2):
            print("Error parsing info file - missing keys (file path: " + str(file_path) + ")")
            return False
        for i in range(len(df[df.keys()[0]])):
            if df.iloc[i][0] == '' or pd.isnull(df.iloc[i][0]):
                print("Error parsing info file - missing parameters (file path: " + str(file_path) + ")")
                return False
            if df.iloc[i][1] == '' or pd.isnull(df.iloc[i][1]):
                print("Error parsing info file - missing parameters (file path: " + str(file_path) + ")")
                return False
        return True

    @staticmethod
    def readInfoFile(file_path, dynasaur_data_dict, no_simulations, simulation_index):
        """ Checks the form of the info file and if the file is valid updates the
        dynasaur_data_dict dictionary with the data read from the file.
        Args:
            file_path (str): Path where the file to be checked is located
            dynasaur_data_dict (dict): Dictionary where data from all input files is stored
            no_simulations (int): Total number of simulations from the simulation directory
            simulation_index (int): Simulation index
        Returns:
            True or False: If the file does not have the required form then the function returns False,
             otherwise it returns True.
        """
        dynasaur_data_dict_temp = copy.deepcopy(dynasaur_data_dict)
        df = pd.read_csv(file_path, delimiter=':')
        if not Dictionary.checkInfoFile(df, file_path):
            return False
        if (df.keys()[0]) not in dynasaur_data_dict_temp.keys():
            dynasaur_data_dict_temp[df.keys()[0]] = [None] * no_simulations
        dynasaur_data_dict_temp[df.keys()[0]][simulation_index] = df.keys()[1]

        for (cnt, sim_description) in enumerate(df[df.keys()[0]].values):
            if (sim_description) not in dynasaur_data_dict_temp.keys():
                dynasaur_data_dict_temp[sim_description] = [None] * no_simulations
            value = df[df.keys()[1]].values[cnt]
            dynasaur_data_dict_temp[sim_description][simulation_index] = value
        dynasaur_data_dict.update(dynasaur_data_dict_temp)
        return True


    @staticmethod
    def readDiagramFile(file_path, dynasaur_data_dict, no_simulations, simulation_index):
        """ Checks the form of the diagram file and if the file is valid updates the
        dynasaur_data_dict dictionary with the data read from the file.
        Args:
            file_path (str): Path where the file to be checked is located
            dynasaur_data_dict (dict): Dictionary where data from all input files is stored
            no_simulations (int): Total number of simulations from the simulation directory
            simulation_index (int): Simulation index
        Returns:
            True or False: If the file does not have the required form then the function returns False,
             otherwise it returns True.
        """
        dynasaur_data_dict_temp = copy.deepcopy(dynasaur_data_dict)
        if os.path.getsize(file_path) > 0:
            file = open(file_path)
            reader = csv.reader(file)
            num_of_lines = len(list(reader))
            if (num_of_lines < 4):
                print("Error parsing diagram file - missing rows (file path: " + str(file_path) + ")")
                return False
            df = pd.read_csv(file_path, delimiter=";", header=[0, 1, 2, 3])
            if not Dictionary.checkFileFormat(df, file_path):
                return False
            if DataDictionaryConstants.PARENT_PART not in dynasaur_data_dict_temp.keys():
                dynasaur_data_dict_temp = {DataDictionaryConstants.PARENT_PART: {}}

            for (cnt, diagram_header) in enumerate(df.keys()):
                body_region = diagram_header[0]
                if body_region not in dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART].keys():
                    dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART].update({body_region: {
                        DataDictionaryConstants.DICT_KEY_DIAGRAMS: {},
                        DataDictionaryConstants.DICT_KEY_CRITERIA: {},
                        DataDictionaryConstants.DICT_KEY_ONE_COLUMN: {}}})

                diagram_temp_none_x = {'x': np.array([None])}
                x_x = df[diagram_header].values
                diagram_temp_x = {'x': x_x}
                diag_name_x = ":".join(diagram_header)
                if diag_name_x not in dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART][body_region] \
                        [DataDictionaryConstants.DICT_KEY_ONE_COLUMN].keys():
                    dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART][body_region] \
                        [DataDictionaryConstants.DICT_KEY_ONE_COLUMN][diag_name_x] =\
                        [diagram_temp_none_x] * no_simulations


                dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART][body_region] \
                    [DataDictionaryConstants.DICT_KEY_ONE_COLUMN][diag_name_x][simulation_index] = diagram_temp_x

                if cnt % 2 == 0:  # Diagram data given in (x,y) pairs, every criterium exists 2 times
                    diagram_temp_none = {'x': np.array([None]), 'y': np.array([None]), 'x_name': '',
                                         'y_name': '', 'x_unit': '', 'y_unit': ''}
                    x = df[diagram_header].values
                    diagram_temp = {'x': x, 'x_name': diagram_header[2], 'x_unit': diagram_header[3],
                                    'y': None, 'y_name': df.keys()[cnt + 1][2], 'y_unit': df.keys()[cnt + 1][3]}

                    # diag_name = df.keys()[cnt].split(':')[2] + "_" + df.keys()[cnt + 1].split(':')[2]
                    diag_name = diagram_header[1]
                    if diag_name not in dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART][body_region] \
                            [DataDictionaryConstants.DICT_KEY_DIAGRAMS].keys():
                        dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART][body_region] \
                            [DataDictionaryConstants.DICT_KEY_DIAGRAMS][diag_name] = \
                            [diagram_temp_none] * no_simulations

                    dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART][body_region] \
                        [DataDictionaryConstants.DICT_KEY_DIAGRAMS][diag_name][simulation_index] = diagram_temp
                else:
                    y = df[diagram_header].values
                    dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART][body_region] \
                        [DataDictionaryConstants.DICT_KEY_DIAGRAMS][diag_name][simulation_index]['y'] = y
            dynasaur_data_dict.update(dynasaur_data_dict_temp)
            return True
        else:
            print("Diagram file is empty (file path: " + str(file_path) + ")")
            return False


    @staticmethod
    def readCriteriaFile(file_path, dynasaur_data_dict, no_simulations, simulation_index, temp_dict):
        """ Checks the form of the criteria file and if the file is valid updates the
        dynasaur_data_dict dictionary with the data read from the file.
        Args:
            file_path (str): Path where the file to be checked is located
            dynasaur_data_dict (dict): Dictionary where data from all input files is stored
            no_simulations (int): Total number of simulations from the simulation directory
            simulation_index (int): Simulation index
        Returns:
            True or False: If the file does not have the required form then the function returns False,
             otherwise it returns True.
        """
        dynasaur_data_dict_temp = copy.deepcopy(dynasaur_data_dict)
        if os.path.getsize(file_path) > 0:
            file = open(file_path)
            reader = csv.reader(file)
            num_of_lines = len(list(reader))
            if (num_of_lines < 3):
                print("Error parsing criteria file - missing rows (file path: " + str(file_path) + ")")
                return False
            df = pd.read_csv(file_path, delimiter=";", header=[0, 1, 2])
            if not Dictionary.checkFileFormat(df, file_path):
                return False
            if DataDictionaryConstants.PARENT_PART not in dynasaur_data_dict_temp.keys():
                dynasaur_data_dict_temp = {DataDictionaryConstants.PARENT_PART: {}}
            for (counter, header_criteria) in enumerate(df.keys()):
                header_criteria_string = ":".join(header_criteria)
                body_region, crit_type, criterion_name, limits = \
                    Dictionary._get_infos_from_header_crit_string(header_criteria_string, temp_dict)

                if body_region not in dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART].keys():
                    dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART].update({body_region: {
                        DataDictionaryConstants.DICT_KEY_DIAGRAMS: {},
                        DataDictionaryConstants.DICT_KEY_CRITERIA: {},
                        DataDictionaryConstants.DICT_KEY_ONE_COLUMN: {}}})

                if criterion_name not in dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART][body_region] \
                        [DataDictionaryConstants.DICT_KEY_CRITERIA]:
                    dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART][body_region] \
                        [DataDictionaryConstants.DICT_KEY_CRITERIA][criterion_name] = \
                        {DataDictionaryConstants.DATA_KEY_TYPE: crit_type,
                         DataDictionaryConstants.DATA_KEY_LIMITS: limits}

                for cnt, observed_data_name in enumerate(df[df.keys()[0]].values):

                    # assert observed_data_name in [DataDictionaryConstants.DATA_KEY_VALUE]

                    if DataDictionaryConstants.DATA_KEY_VALUE not in \
                            dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART] \
                                    [body_region][DataDictionaryConstants.DICT_KEY_CRITERIA][criterion_name].keys():
                        dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART][body_region] \
                            [DataDictionaryConstants.DICT_KEY_CRITERIA][criterion_name] \
                            [DataDictionaryConstants.DATA_KEY_VALUE] = [None] * no_simulations

                    value = df[df.keys()[counter]].values[cnt]

                    dynasaur_data_dict_temp[DataDictionaryConstants.PARENT_PART][body_region] \
                        [DataDictionaryConstants.DICT_KEY_CRITERIA][criterion_name][
                        DataDictionaryConstants.DATA_KEY_VALUE][simulation_index] = \
                        float(value) if Dictionary._is_float(value) else value
            dynasaur_data_dict.update(dynasaur_data_dict_temp)
            return True
        else:
            print("Criteria file is empty (file path: " + str(file_path) + ")")
            return False


    @staticmethod
    def readFromFile(rootdir, limits_file = None):
        """
        Args:
            rootdir (str): Path to the destination directory
            limits_file (dict): Path to the limits input file
        Returns:
            None, 0 or dynasaur_data_dict, no_simulations (dict, int): Returns None, 0 when the destination directory
            is not valid for some reason. if it returns dynasaur_data_dict, no_simulations means that the input files
            from the destination directory have been read and that the data from the input files has been saved in
            dynasaur_data_dict dictionary while the no_simulations is number of simulations in the destination
            directory.
        """
        if not os.path.exists(rootdir):
            print(f'The root directory: {rootdir} does not exists!')
            return None, 0
        if len(os.listdir(rootdir)) == 0:
            print(f'The root directory: {rootdir} is empty!')
            return None, 0

        rootdir_object = os.scandir(rootdir)
        for sub_folder in rootdir_object:
            file_names = [i for i in os.listdir(sub_folder)] if sub_folder.is_dir() else []
            sub_folder_exist = True if sub_folder.is_dir() else False

        if not sub_folder_exist or not any(file in file_names for file in ('info.csv', 'criteria.csv', 'diagram.csv')):
            print(f'The root directory: {rootdir} either does not have the appropriate sub folders or '
                  f'the sub folders do not have the appropriate csv files!')
            print("The basic root directory set up is described with following example:")
            print("    Main folder: Destination Directory(e.g. „Parameter_Study_X“)")
            print("    I")
            print("    +--------- Sub folder: Simulation / Test1(e.g. „Parameter_Simulation_x1“)")
            print("    I    + ------------- Three result files: info.csv, criteria.csv, diagram.csv")
            print("    (…)")
            print("    +--------- Sub folder: Simulation / Testn(e.g. „Parameter_Simulation_xn“)")
            print("    I    + ------------- Three result files: info.csv, criteria.csv, diagram.csv")
            return None, 0

        temp_dict = None
        if limits_file is not None:
            if not limits_file.endswith(".json"):
                print(f'Limits file is not json file!')
                return None, 0
            elif not (os.path.isfile(limits_file) and os.path.getsize(limits_file) > 0):
                print("File with limits data is empty or non-existent json file (file path: " + str(limits_file) + ")")
                return None, 0

            with open(limits_file, 'r') as limits_file:
                json_object = limits_file.readlines()
                json_object_string = "".join(json_object)
                try:
                    temp_dict = json.loads(json_object_string)
                    if not Dictionary.check_limits_json(temp_dict):
                        temp_dict = None

                except ValueError as e:
                    print("Decoding limits file has failed (file path: " + str(limits_file) + ")")
                    exit(e)

        simulation_directories = [i for i in os.listdir(rootdir) if os.path.isdir(os.path.join(rootdir, i))]
        no_simulations = len(simulation_directories)
        simulation_index = 0  # variable reflecting the current simulation directory
        dynasaur_data_dict = {}
        any_csv_file = False
        for subdir, dirs, files in os.walk(rootdir):
            check_files = [False, False, False]
            for file_name in files:
                if DataDictionaryConstants.DICT_KEY_SIM_NAME not in dynasaur_data_dict.keys():
                    dynasaur_data_dict[DataDictionaryConstants.DICT_KEY_SIM_NAME] = [None] * no_simulations
                if (os.path.join(rootdir, subdir) != rootdir):
                    dynasaur_data_dict[DataDictionaryConstants.DICT_KEY_SIM_NAME][simulation_index] = \
                        os.path.basename(subdir)
                file_path = os.path.join(subdir, file_name)
                if file_name == 'info.csv':
                    if Dictionary.readInfoFile(file_path, dynasaur_data_dict, no_simulations, simulation_index):
                        check_files[0] = True
                    else:
                        continue
                elif file_name == 'diagram.csv':
                    if Dictionary.readDiagramFile(file_path, dynasaur_data_dict, no_simulations, simulation_index):
                        check_files[1] = True
                    else:
                        continue
                elif file_name == 'criteria.csv':
                    if Dictionary.readCriteriaFile(file_path, dynasaur_data_dict,
                                                      no_simulations, simulation_index, temp_dict):
                        check_files[2] = True
                    else:
                        continue
                else:
                    print(file_name + ' is not a file to visualise!')
            any_csv_file = any_csv_file or any(check_files)
            if any(check_files):
                simulation_index += 1

            if not all(check_files) and len(files) != 0 and dirs==[]:
                print("In directory " + str(subdir) + " criteria, info or diagram file missing!")
        if not any_csv_file:
            print(f'The root directory: {rootdir} does not have required csv files!')
            return None, 0
        Dictionary.harmonize_unitsin_dynasaur_data_dict(dynasaur_data_dict)
        return dynasaur_data_dict, no_simulations
