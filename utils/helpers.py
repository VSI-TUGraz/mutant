import os
import datetime
import time
import csv

import numpy as np
from utils.define_globals import Globals
from utils.group_computation import Groups
import colorlover as cl
from utils.utils import FilterUtils
from utils.constants import DataDictionaryConstants, DropdownConstants
import copy
from dash import html
from utils.dash_core_html_components import HTMLComponent
import matplotlib.pyplot as plt
import pandas as pd
from dash import dash_table as dt
import plotly.graph_objs as go


class SimulationFilterMetaInformation:
    @staticmethod
    def update_simulation_filter_meta_information_dropdown(simulation_filter_meta_information_dropdown_radiobuttons):
        """ Checks the selected radiobutton and returns either list of all body regions or empty list.
        Args:
            simulation_filter_meta_information_dropdown_radiobuttons (str): The value selected with the radiobutton
        Returns:
            Globals.body_regions_ or [] (list): If 'all' is selected it returns the Globals.body_regions_ list,
            otherwise it returns an empty list.
        """
        if simulation_filter_meta_information_dropdown_radiobuttons == 'all':
            return Globals.body_regions_
        else:
            return []


class Button:
    @staticmethod
    def filtered_simulation_export_csv(*args):
        """ Creates a csv file in the destination directory and writes criteria values for the selected simulations.
        Args:
            args (tuple): Tuple with data from the global configuration part and criteria files
        Returns:
            If there is no click the function just returns, otherwise it writes the data to a csv file.
        """
        click = args[0]
        if not click:
            return

        global_dropdown_values = args[1:-len(Globals.list_filter_criteria_range_slider_ids_)]
        criteria_slider_values = args[1 + len(Globals.list_of_global_dropdown_ids_):]

        boolean_filter_dict = FilterUtils.get_boolean_filter_dict_for_criteria(
            Globals.list_filter_criteria_range_slider_ids_,
            Globals.list_of_global_dropdown_ids_,
            Globals.working_dict_,
            criteria_slider_values,
            global_dropdown_values)

        ts = time.time()
        st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H-%M-%S')
        file_name = os.path.join(Globals.rootdir_, 'report_' + st + '.csv')

        with open(file_name, 'w', newline="\n") as csvfile:

            row = ['simulation_name']
            writer = csv.writer(csvfile, delimiter=';')
            for body_region in Globals.working_dict_[DataDictionaryConstants.PARENT_PART].keys():
                for injury_crit in Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region] \
                        [DataDictionaryConstants.DICT_KEY_CRITERIA].keys():
                    row.append(body_region + ':' + injury_crit)
            writer.writerow(row)
            for sim in range(Globals.getNumOfSimulation()):
                if all([boolean_filter_dict[key][sim] for key in boolean_filter_dict.keys()]):
                    row = [Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][sim]]
                    for body_region in Globals.working_dict_[DataDictionaryConstants.PARENT_PART].keys():
                        for injury_crit in Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region] \
                                [DataDictionaryConstants.DICT_KEY_CRITERIA].keys():
                            row.append(Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region] \
                                           [DataDictionaryConstants.DICT_KEY_CRITERIA][injury_crit] \
                                           [DataDictionaryConstants.DATA_KEY_VALUE][sim])
                    writer.writerow(row)

    @staticmethod
    def filtered_simulation_export_plt(*args):
        """ Creates plots for appropriate criteria and simulations.
        Args:
            args (tuple): Tuple with the number of clicks
        Returns:
            If there is no click the function just returns, otherwise it creates plots
        """
        click = args[0]
        if not click:
            return

        sim_names = [Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][sim]
                     for sim in range(Globals.getNumOfSimulation())]
        for body_region in Globals.working_dict_[DataDictionaryConstants.PARENT_PART].keys():
            diags = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region][
                DataDictionaryConstants.DICT_KEY_DIAGRAMS]
            for diag_name in diags:

                f = plt.figure()
                for sim_name, diag in zip(sim_names, diags[diag_name]):
                    plt.plot(diag["x"], diag["y"], label=sim_name)

                path_to_save = os.path.join(Globals.rootdir_, (body_region + "_" + diag_name + ".png"))
                plt.legend()
                plt.grid()
                plt.xlabel(diag["x_name"] + "[" + diag["x_unit"] + "]")
                plt.ylabel(diag["y_name"] + "[" + diag["y_unit"] + "]")
                plt.title(diag_name)
                plt.savefig(path_to_save)
                plt.close(f)


class FilteredSimulationInfo:
    @staticmethod
    def update_filtered_simulation_info(*args):
        """ Depending on the other parameters in the global configuration section returns a list of data to be
        displayed in the statistics section.
        Args:
            args (tuple): Tuple with data from the global configuration part and criteria files
        Returns:
            list_of_children (list): List of data to be displayed in the statistics section
        """
        global_dropdown_values = args[0:-len(Globals.list_filter_criteria_range_slider_ids_)]
        criteria_slider_values = args[0 + len(Globals.list_of_global_dropdown_ids_):]

        boolean_filter_dict = FilterUtils.get_boolean_filter_dict_for_criteria(
            Globals.list_filter_criteria_range_slider_ids_,
            Globals.list_of_global_dropdown_ids_,
            Globals.working_dict_,
            criteria_slider_values,
            global_dropdown_values)
        # get the names of the simulation that meet the filter criteria

        remaining_simulations = [Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][sim] for sim in
                                 range(Globals.getNumOfSimulation()) if all([boolean_filter_dict[key][sim]
                                                                             for key in boolean_filter_dict.keys()])]

        list_of_children = [html.P(className='filtered_simulation_info_title',
                                   children= ['Number of all simulations  [{}]'.format(Globals.getNumOfSimulation())]),
                            html.P('Number of filtered  simulations  [{}]'.format(len(remaining_simulations)))]

        sim_names = [html.P(sim_name) for sim_name in remaining_simulations]
        list_of_children.append(html.Div(className='filtered_simulation_info_list',children=sim_names))

        return list_of_children

########################################################################################################################

class SelectedSimulationInfo:
    @staticmethod
    def update_selected_simulation_info(*args):
        """ Depending on the clicked data value function returns either an empty list or a list of data to be
        displayed in the info section.
        Args:
            args (tuple): Tuple with information about the line the user clicked on
        Returns:
            children (list) or new_children (list): If clicked data is 0 then the function returns children
            (empty list), otherwise it returns new_children (list of data to be displayed in the info section)
        """
        children = []
        new_children = []
        reset_button_ = args[4]


        for key in Globals.working_dict_.keys():
            if key != DataDictionaryConstants.PARENT_PART:
                children.append(html.P(key))
        for sim in range(Globals.getNumOfSimulation()):
            if Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][sim] == Globals.clicked_data_string:
                new_children.append(html.Div(className='filtered_simulation_info_title', children='Info:'))

                for child in children:
                    value = str(Globals.working_dict_[child.children][sim])
                    new_children.append(html.P(className='filtered_simulation_info_list',children = [child.children +
                                                                                                     ': ' + value]))
                new_children.append(html.P(className='filtered_simulation_info_list',
                                           children = ['Name: ' +
                                                       Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME]
                                                                            [sim]]))
                return new_children

    @staticmethod
    def update_selected_simulation_info_criteria_without_limits(*args):
        """ Checks which of the selected criteria do not have limits data.
        Args:
            args (tuple): Tuple with selected options and data from the global configuration part and criteria files
        Returns:
            list_of_crit_without_limits (list): List of criteria that have no data limits
        """
        criteria_to_be_shown = args[0]
        labels_without_limits = []
        for label in criteria_to_be_shown:
            if Globals.working_dict_[DataDictionaryConstants.PARENT_PART][label.split(':')[0]] \
                    [DataDictionaryConstants.DICT_KEY_CRITERIA][label.split(':')[1]] \
                    [DataDictionaryConstants.DATA_KEY_LIMITS] is None:
                labels_without_limits.append(label.split(':')[1])
        list_of_crit_without_limits = [
            html.P(className='filtered_simulation_info_title',children=['Criteria with missing limits:'])]

        if not labels_without_limits:
            list_of_crit_without_limits.append(
                html.Div(className='filtered_simulation_info_title',
                         children="No criteria with missing limits from selected criteria"))
        else:
            sim_names = [html.P(sim_name) for sim_name in labels_without_limits]
            list_of_crit_without_limits.append(html.Div(className='filtered_simulation_info_list', children=sim_names))
        return list_of_crit_without_limits


class SimulationFilterCriteria:

    @staticmethod
    def update_simulation_filter_criteria_dropdown_multiple_selection(criteria_radiobutton_value, body_regions):
        """ Creates a list of criteria names to be displayed in the criteria dropdown in relation to the selected
        body regions and the selected criteria radiobutton.
        Args:
            criteria_radiobutton_value (str): The value selected with the radiobutton
            body_regions (list): List of selected body regions
        Returns:
            dropdown_criteria (list): Criteria names to be displayed in the criteria dropdown
        """
        dropdown_criteria = CriteriaDataGraph.update_criteria_data_graph_dropdown_multiple_selection\
            (criteria_radiobutton_value)
        temp_list = copy.deepcopy(dropdown_criteria)
        for body_region in temp_list:
            if body_region.split(":")[0] not in body_regions:
                dropdown_criteria.remove(body_region)
        return dropdown_criteria

    @staticmethod
    def update_simulation_filter_criteria_sliders(selected_simulation_criteria):
        """ Returns a list of sliders to be presented in relation to the selected criteria in the criteria dropdown.
        Args:
            selected_simulation_criteria (list): List of criteria names selected in the criteria dropdown
        Returns:
            children_injury (list): List of sliders
        """
        children_injury = []
        all_injury_criteria = Globals.list_filter_criteria_range_slider_ids_
        for injury_crit in all_injury_criteria:
            body_region = injury_crit.split(':')[0]
            injury_crit_short = injury_crit.split(':')[1]
            float_boolean = [isinstance(v, float) for v in Globals.working_dict_[DataDictionaryConstants.PARENT_PART]
            [body_region][DataDictionaryConstants.DICT_KEY_CRITERIA][injury_crit_short]
            [DataDictionaryConstants.DATA_KEY_VALUE]]
            if any(float_boolean):
                disabled = body_region + ':' + injury_crit_short not in selected_simulation_criteria
                children_injury.append(HTMLComponent.get_simulation_filter_criteria_slider_div(Globals.working_dict_,
                                                                    body_region, injury_crit_short, disabled))
        return children_injury


class DiagramDataGraph:
    @staticmethod
    def set_highlights(diagram_data_graph_1_clicked_data_tmp, diagram_data_graph_2_clicked_data_tmp,
                       criteria_data_2D_scatter_clicked_data_tmp, clicked_reset):

        clicked_data_temp_list = [diagram_data_graph_1_clicked_data_tmp, diagram_data_graph_2_clicked_data_tmp,
                                  criteria_data_2D_scatter_clicked_data_tmp]
        nones_in_clicke_data = clicked_data_temp_list.count(None)

        if nones_in_clicke_data == len(clicked_data_temp_list):
            return

        if clicked_reset:
            Globals.clicked_data_string = None  # ""
            Globals.criteria_data_2D_scatter_reset_clicked_data = criteria_data_2D_scatter_clicked_data_tmp
            Globals.diagram_data_graph_1_reset_clicked_data = diagram_data_graph_1_clicked_data_tmp
            Globals.diagram_data_graph_2_reset_clicked_data = diagram_data_graph_2_clicked_data_tmp
            return

        if (Globals.criteria_data_2D_scatter_reset_clicked_data != criteria_data_2D_scatter_clicked_data_tmp or \
                Globals.diagram_data_graph_1_reset_clicked_data != diagram_data_graph_1_clicked_data_tmp or \
                Globals.diagram_data_graph_2_reset_clicked_data != diagram_data_graph_2_clicked_data_tmp):
            pass
        else:
            Globals.clicked_data_string = None  # ""
            return

        clicked_data_globals_list = [Globals.diagram_data_graph_1_clicked_data,
                                     Globals.diagram_data_graph_2_clicked_data,
                                     Globals.criteria_data_2D_scatter_clicked_data]

        for index in range(len(clicked_data_temp_list)):
            if clicked_data_temp_list[index]:
                if not clicked_data_globals_list[index]:
                    clicked_data_globals_list[index] = clicked_data_temp_list[index]
                    Globals.clicked_data_string = clicked_data_temp_list[index]['points'][-1]['text']
                elif clicked_data_globals_list[index]:
                    if clicked_data_temp_list[index] != clicked_data_globals_list[index]:
                        clicked_data_globals_list[index] = clicked_data_temp_list[index]
                        Globals.clicked_data_string = clicked_data_temp_list[index]['points'][-1]['text']
        Globals.diagram_data_graph_1_clicked_data = clicked_data_globals_list[0]
        Globals.diagram_data_graph_2_clicked_data = clicked_data_globals_list[1]
        Globals.criteria_data_2D_scatter_clicked_data = clicked_data_globals_list[2]

    @staticmethod
    def update_diagram_data_graph_dropdown_list(working_dict, selected_body_regions):
        """ Creates a list of criteria names (depending on the selected body regions) that will be used for
        the dropdown for data visualization graphs.
        Args:
            working_dict (dict): Dictionary with data from input files
            selected_body_regions (list): List of selected body regions
        Returns:
            criteria_names (list) or None: Criteria names to be displayed in the dropdown depending on
            the selected body regions, if no body region is selected the function returns None
        """
        if not selected_body_regions:
            return None
        list_of_diagrams = []
        for body_region in selected_body_regions:
            for cnt, diagrams in enumerate(list(working_dict[DataDictionaryConstants.PARENT_PART][body_region]
                                                [DataDictionaryConstants.DICT_KEY_DIAGRAMS])):
                list_of_diagrams.append({'label': diagrams, 'value': diagrams})

        # removes duplicates
        criteria_names = [ii for n, ii in enumerate(list_of_diagrams) if ii not in list_of_diagrams[:n]]
        return criteria_names

    @staticmethod
    def get_color_by_group(graph_legend, value):
        """ If the graph_legend or value is None the function returns 'grey', otherwise it will return another color
        (to differentiate if according to the given grouping the components have different values).
        Args:
            graph_legend (str): Which type of grouping is selected in the "Group by" dropdown
            value (str): Value depending on the selected grouping
        Returns:
            color_to_return (str): the color that the function returns depending on the input parameters
        """
        color_to_return = 'grey'
        if graph_legend is None or value is None:
            return color_to_return

        group_by_ids = list(
            set([i for i in Globals.working_dict_[graph_legend.replace('dropdown_', "")] if i is not None]))
        len(group_by_ids)

        if len(group_by_ids) > 9:
            return color_to_return

        amount_colors = '3' if len(group_by_ids) <= 3 else str(len(group_by_ids))
        cols = cl.scales[amount_colors]['qual']['Set1']

        for i in range(len(group_by_ids)):
            if group_by_ids[i] == value:
                color_to_return = cols[i]
                break
        return color_to_return

    @staticmethod
    def update_diagram_data_graph(*args):
        """ Returns data for the diagram data graph depending on the selected options and data from the
        criteria file.
        Args:
            args (tuple): Tuple with selected options and data from the global configuration part and criteria files
        Returns:
            dict(data=data, layout=layout) : Dictionary with data for the diagram data graph
        """
        data = []
        name = args[0]
        click_data_2d_scatter = args[4]
        name_drop_down_diagram_x = args[5]
        name_drop_down_diagram_y = args[6]
        check_box_value_true_false = args[7]
        graph_legend = args[9]

        global_dropdown_values = args[11:-len(Globals.list_filter_criteria_range_slider_ids_)]
        criteria_slider_values = args[11 + len(Globals.list_of_global_dropdown_ids_):]

        layout = {'height': 500, 'margin': {'l': 60, 'b': 40, 't': 30, 'r': 30},
                  'hovermode': 'closest',
                  'title': '      ©vsi, tugraz',
                  'xaxis': {'title': ''},
                  'yaxis': {'title': ''},
                  'showlegend': True}

        if (check_box_value_true_false and name is None) or (not check_box_value_true_false and
                                                             (name_drop_down_diagram_x is None or
                                                              name_drop_down_diagram_y is None)):
            return {'data': data, 'layout': layout}

        # all criteria that have to be met!
        boolean_filter_dict = FilterUtils.get_boolean_filter_dict_for_criteria(
            Globals.list_filter_criteria_range_slider_ids_,
            Globals.list_of_global_dropdown_ids_,
            Globals.working_dict_,
            criteria_slider_values,
            global_dropdown_values)

        if check_box_value_true_false:
            layout['title'] = '' if name is None else name
        else:
            layout['title'] = '' if name_drop_down_diagram_x is None or name_drop_down_diagram_y is None else \
                name_drop_down_diagram_x.split(':')[2] + ' ' + name_drop_down_diagram_y.split(':')[2]

        layout['title'] += '      ©vsi, tugraz'
        diagram_data = []
        legends_list = []
        for sim in range(Globals.getNumOfSimulation()):
            if check_box_value_true_false:
                for body_region in Globals.working_dict_[DataDictionaryConstants.PARENT_PART].keys():
                    if name in Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region] \
                            [DataDictionaryConstants.DICT_KEY_DIAGRAMS].keys():
                        x = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region] \
                            [DataDictionaryConstants.DICT_KEY_DIAGRAMS][name][sim]['x']
                        y = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region] \
                            [DataDictionaryConstants.DICT_KEY_DIAGRAMS][name][sim]['y']
                        x_name = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region] \
                            [DataDictionaryConstants.DICT_KEY_DIAGRAMS][name][sim]['x_name']
                        y_name = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region] \
                            [DataDictionaryConstants.DICT_KEY_DIAGRAMS][name][sim]['y_name']
                        diagram_data.append([x, y, x_name, y_name])
                        break
            else:
                body_region_x = name_drop_down_diagram_x.split(':')[0]
                body_region_y = name_drop_down_diagram_y.split(':')[0]
                x = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region_x] \
                    [DataDictionaryConstants.DICT_KEY_ONE_COLUMN][name_drop_down_diagram_x][sim]['x']
                y = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region_y] \
                    [DataDictionaryConstants.DICT_KEY_ONE_COLUMN][name_drop_down_diagram_y][sim]['x']
                diagram_data.append([x, y, name_drop_down_diagram_x.split(':')[-1],
                                     name_drop_down_diagram_y.split(':')[-1]])

            met_criteria = [boolean_filter_dict[key][sim] for key in boolean_filter_dict.keys()]
            if all(met_criteria):
                #if True:
                color_to_curve = 'grey'
                showledgend = False
                legend_name = ''
                if graph_legend is not None:
                    color_to_curve = DiagramDataGraph.get_color_by_group(graph_legend,
                                                                         Globals.working_dict_[
                                                                         graph_legend.replace("dropdown_","")][sim])
                    showledgend = Globals.working_dict_[graph_legend.replace("dropdown_", "")][sim] not in legends_list
                    legends_list.append(Globals.working_dict_[graph_legend.replace("dropdown_", "")][sim])
                    legend_name = Globals.working_dict_[graph_legend.replace("dropdown_", "")][sim]

                if Globals.clicked_data_string:
                    color_to_curve = 'black' if Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][sim] == \
                                              Globals.clicked_data_string else color_to_curve

                layout['xaxis']['title'] = diagram_data[sim][2]
                layout['yaxis']['title'] = diagram_data[sim][3]

                graph = dict(
                    legendgroup=legend_name,
                    name=legend_name,
                    showlegend=showledgend,
                    mode='lines',
                    text=[Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][sim]] *
                         len(diagram_data[sim][0]),
                    x=diagram_data[sim][0],
                    y=diagram_data[sim][1],
                    marker={"color": color_to_curve},
                    unselected={
                        'marker': {
                            'opacity': 0.1,
                        },
                        'textfont': {
                            # make text transparent when not selected
                            'color': 'rgba(0, 0, 0, 0)'
                        }
                    }
                )
                data.append(graph)
            else:
                pass
        return dict(data=data, layout=layout)

    @staticmethod
    def update_diagram_data_graph_check_box_x_y(check_box_x_y_value):
        """ Checks if the "separate x-y" option is selected
        Args:
            value (list): List that is empty if the "separate x-y" option is not selected,
            otherwise it has an element "selected".
        Returns:
            True or False (bool): Returns True if it has an element selected, otherwise returns False.
        """
        if check_box_x_y_value is not None and len(check_box_x_y_value) != 0:
            if check_box_x_y_value[0] == 'selected':
                return True
        return False

    @staticmethod
    def update_diagram_data_graph_dropdown_x_y(value):
        """ Returns a list of criteria selected in the dropdown and not in the
        Globals.option_for_simulation_dropdown_x_y_ list
        Args:
            value (str): Selected value in "x" or "y" dropdown
        Returns:
            new_list_of_criteria (list): List of criteria selected in the dropdown and not in the
            Globals.option_for_simulation_dropdown_x_y_ list
        """
        new_list_of_criteria = []
        for criteria in Globals.option_for_simulation_dropdown_x_y_:
            if criteria['value'] != value:
                new_list_of_criteria.append({'label': criteria['label'], 'value': criteria['value']})
        return new_list_of_criteria


class CriteriaDataBoxplot:
    @staticmethod
    def update_criteria_data_boxplot( *args):
        """ Returns data for the criteria data boxplot graph depending on the selected options and data from the
        criteria file.
        Args:
            args (tuple): Tuple with selected options and data from the global configuration part and criteria files
        Returns:
            dict(data=data_to_graph, layout=go.Layout(kwargs)) : Dictionary with data for the box plot graph
        """
        values_from_dropdown_menu = args[0]
        global_dropdown_values = args[3:-len(Globals.list_filter_criteria_range_slider_ids_)]
        criteria_slider_values = args[3 + len(Globals.list_of_global_dropdown_ids_):]

        data_to_graph = []
        values_list = []
        labels = []

        kwargs = {'xaxis': dict(domain=[0.2, 0.9]), 'height': 500, 'margin': {'l': 40, 'b': 60, 't': 30, 'r': 30},
                  'title': 'Boxplot ' + '  ©vsi, tugraz', 'hovermode': 'closest', 'showlegend': False}

        if values_from_dropdown_menu is None or len(values_from_dropdown_menu) == 0:
            return dict(data=data_to_graph, layout=go.Layout(kwargs))

        boolean_filter_dict = FilterUtils.get_boolean_filter_dict_for_criteria(
            Globals.list_filter_criteria_range_slider_ids_,
            Globals.list_of_global_dropdown_ids_,
            Globals.working_dict_,
            criteria_slider_values,
            global_dropdown_values)

        # input for go.layout
        color_def = ['rgb(148, 103, 189)', 'rgb(107,174,214)', 'rgb(9,56,125)']
        for y_axis_cnt, value_from_dropdown in enumerate(values_from_dropdown_menu):
            check_legend_flag = False
            for sim in range(Globals.getNumOfSimulation()):
                color_to_curve = 'grey'
                criteria_met = [boolean_filter_dict[key][sim] for key in boolean_filter_dict.keys()]
                if all(criteria_met):
                    values_list.append(Globals.working_dict_[DataDictionaryConstants.PARENT_PART]
                                       [value_from_dropdown.split(":")[0]][DataDictionaryConstants.DICT_KEY_CRITERIA]
                                       [value_from_dropdown.split(":")[1]][DataDictionaryConstants.DATA_KEY_VALUE][sim])
                    labels.append(Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][sim])

                    if Globals.clicked_data_:
                        color_to_curve = 'black' if Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][
                                                      sim] == \
                                                  Globals.clicked_data_['points'][-1]['text'] else color_to_curve

            grph = 'yaxis' + (str(y_axis_cnt + 1) if y_axis_cnt > 0 else '')
            overlay = 'y' if y_axis_cnt > 0 else None
            show_grid_bool = not (y_axis_cnt > 0)
            side_variable = 0.8 if y_axis_cnt > 1 else 0
            y_axis = dict(
                overlaying=overlay,
                showgrid=show_grid_bool,
                position=side_variable + y_axis_cnt * 0.1,
                tickfont=dict(
                    color=color_def[y_axis_cnt]
                )
            )
            kwargs[grph] = y_axis
            graph = go.Box(
                y=values_list,
                yaxis='y' + (str(y_axis_cnt + 1) if y_axis_cnt > 0 else ''),
                jitter=0.3,
                boxpoints='all',
                whiskerwidth=0.2,
                text=labels,
                name=value_from_dropdown.split(":")[1],
                #marker=dict(color=["blue","green", "black"] ),
                #line=dict(color="green"))
                line=dict(color=color_def[y_axis_cnt]))
            data_to_graph.append(graph)
            values_list.clear()
            labels.clear()
        return dict(data=data_to_graph, layout=go.Layout(kwargs))

    @staticmethod
    def update_criteria_data_boxplot_dropdown_limit(criteria_data_boxplot_dropdown_value):
        """ Checks if 3 criteria names are selected in the boxplot dropdown.
        Args:
            boxplot_dropdown_value (list): List of criteria names selected in the boxplot dropdown
        Returns:
             boxplot_dropdown_list (list) or Globals.options_for_filter_criteria_dropdown_ (list): If 3 criteria are
             selected in the boxplot dropdown then the function returns list of selected criteria names which are
             in the Globals.options_for_filter_criteria_dropdown_ list, otherwise the function returns
             Globals.options_for_filter_criteria_dropdown_ list
        """
        if len(criteria_data_boxplot_dropdown_value) == 3:
            boxplot_dropdown_list = [sel for sel in Globals.options_for_filter_criteria_dropdown_ if
                                     sel['value'] in criteria_data_boxplot_dropdown_value]
            return boxplot_dropdown_list
        return Globals.options_for_filter_criteria_dropdown_


class CriteriaData2DScatter:

    @staticmethod
    def update_criteria_data_2D_scatter(*args):
        """ Creates list of data for the criteria data 2D scatter plot depending on the selected options and data from
        the criteria file.
        Args:
            args (tuple): Tuple with selected options and data from the global configuration part and criteria files
        Returns:
            dict(data=data, layout=layout) (dict): Dictionary with data for the graph
        """
        graph_legend = args[0]
        crit_1 = args[1]
        crit_2 = args[2]

        global_dropdown_values = args[9:-len(Globals.list_filter_criteria_range_slider_ids_)]
        criteria_slider_values = args[9 + len(Globals.list_of_global_dropdown_ids_):]

        data = []
        labels = []

        x_axis_name = '' if crit_1 is None or len(crit_1) == 0 else crit_1.split(':')[0] + " " + crit_1.split(':')[1]
        y_axis_name = '' if crit_2 is None or len(crit_2) == 0 else crit_2.split(':')[0] + " " + crit_2.split(':')[1]
        width_to_graph = 500 if (crit_1 is None or len(crit_1) == 0) or (crit_2 is None or len(crit_2) == 0) else 500

        layout = {'height': 500, 'margin': {'l': 60, 'b': 50, 't': 30, 'r': 30},
                  'title': '2D Point' + '  ©vsi, tugraz', 'hovermode': 'closest', 'xaxis': {'title': x_axis_name},
                  'yaxis': {'title': y_axis_name}, 'showlegend': True}

        if crit_1 is None or crit_2 is None:
            return dict(data=data, layout=layout)

        if len(crit_1) == 0 or len(crit_2) == 0:
            return dict(data=data, layout=layout)

        boolean_filter_dict = FilterUtils.get_boolean_filter_dict_for_criteria(
            Globals.list_filter_criteria_range_slider_ids_,
            Globals.list_of_global_dropdown_ids_,
            Globals.working_dict_,
            criteria_slider_values,
            global_dropdown_values)
        legends_list = []
        for sim in range(Globals.getNumOfSimulation()):
            color_to_point = 'grey'
            crit_1_name = crit_1.split(':')[1]
            first_name_body_region = crit_1.split(':')[0]
            crit_2_name = crit_2.split(':')[1]
            second_name_body_region = crit_2.split(':')[0]

            criteria_met = [boolean_filter_dict[key][sim] for key in boolean_filter_dict.keys()]

            if all(criteria_met):
                value_x = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][first_name_body_region] \
                    [DataDictionaryConstants.DICT_KEY_CRITERIA][crit_1_name] \
                    [DataDictionaryConstants.DATA_KEY_VALUE][sim]
                value_y = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][second_name_body_region] \
                    [DataDictionaryConstants.DICT_KEY_CRITERIA][crit_2_name] \
                    [DataDictionaryConstants.DATA_KEY_VALUE][sim]
                showlegend = False
                legend_name = ''
                if graph_legend is not None:
                    color_to_point = DiagramDataGraph.get_color_by_group(graph_legend,
                                                                         Globals.working_dict_[
                                                                             graph_legend.replace("dropdown_", "")][
                                                                             sim])
                    showlegend = Globals.working_dict_[graph_legend.replace("dropdown_", "")][sim] not in legends_list
                    legends_list.append(Globals.working_dict_[graph_legend.replace("dropdown_", "")][sim])
                    legend_name = Globals.working_dict_[graph_legend.replace("dropdown_", "")][sim]

                if Globals.clicked_data_string:
                    color_to_point = 'black' if Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][sim] == \
                                              Globals.clicked_data_string else color_to_point

                labels.append(Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][sim])

                graph = dict(
                    legendgroup=legend_name,
                    name=legend_name,
                    showlegend=showlegend,
                    mode='markers',
                    text=[Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][sim]],
                    x=[value_x],
                    y=[value_y],
                    opacity=0.7,
                    marker={
                        'symbol': 'diamond',
                        'size': 15,
                        'line': {'width': 0.5, 'color': 'white'},
                        'color': color_to_point
                    },
                    unselected={
                        'marker': {
                            'opacity': 0.3,
                        },
                        'textfont': {
                            # make text transparent when not selected
                            'color': 'rgba(0, 0, 0, 0)'
                        }
                    }
                )
                data.append(graph)

            else:
                pass
        return dict(data=data, layout=layout)

    @staticmethod
    def update_criteria_data_2D_scatter_dropdown_list(value):
        """ Creates a list of criteria selected in the criteria data 2D scatter dropdown and not in the
        Globals.options_for_filter_criteria_dropdown_additional_label_body_region_ list
        Args:
            value (str): Selected value in criteria data 2D scatter dropdown
        Returns:
            new_list_of_criteria (list): List of criteria selected in the criteria data 2D scatter dropdown and not
            in the Globals.options_for_filter_criteria_dropdown_additional_label_body_region_ list
        """
        new_list_of_criteria = []
        for criteria in Globals.options_for_filter_criteria_dropdown_additional_label_body_region_:
            if criteria['value'] != value:
                new_list_of_criteria.append({'label': criteria['label'], 'value': criteria['value']})
        return new_list_of_criteria


class CriteriaDataGraph:
    @staticmethod
    def get_global_injury_criteria():
        """ Creates a list in which each element contains the body region and the criteria name
        Returns:
            list_of_criteria (list): List all criteria from the input file
        """
        list_of_criteria = []
        for body_region in Globals.working_dict_[DataDictionaryConstants.PARENT_PART].keys():
            for criteria in list(Globals.working_dict_[DataDictionaryConstants.PARENT_PART]
                                 [body_region][DataDictionaryConstants.DICT_KEY_CRITERIA]):
                list_of_criteria.append(body_region + ':' + criteria)
        return list_of_criteria

    @staticmethod
    def get_value_from_normalization(data_to_be_plotted, calculation_method, labels_to_be_plotted,
                                     crit_dict_min_mean_max):
        """ Creates a list of criteria selected in the criteria data 2D scatter dropdown and not in the
        Globals.options_for_filter_criteria_dropdown_additional_label_body_region_ list
        Args:
            data_to_be_plotted (numpy nd-array): Simulation criteria data
            calculation_method (str): Operation selected in dropdown
            labels_to_be_plotted (numpy nd-array): Array of body regions and criteria names
            crit_dict_min_mean_max (dict): Dictionary of criteria names and corresponding sorted values
        Returns:
            return_factors, implicit (numpy nd-array, numpy nd-array): Array with the values obtained with the
            corresponding simulation criteria data in relation to which operation is selected in dropdown,
            Array with True/False values that serve to put a circle on the graph in the appropriate place
        """
        return_factors = np.ones(len(labels_to_be_plotted))
        if calculation_method is None:
            return data_to_be_plotted / return_factors, None

        implicit = np.full(len(labels_to_be_plotted), False)
        # TODO: Check if we can ignore waring messages?
        np.seterr(all='ignore')
        for nr, label in enumerate(labels_to_be_plotted):
            max = crit_dict_min_mean_max[label][2]
            mean_crit = crit_dict_min_mean_max[label][1]
            min = crit_dict_min_mean_max[label][0]
            if Globals.working_dict_[DataDictionaryConstants.PARENT_PART][label.split(':')[0]] \
                    [DataDictionaryConstants.DICT_KEY_CRITERIA][label.split(':')[1]] \
                    [DataDictionaryConstants.DATA_KEY_LIMITS] is not None:
                best = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][label.split(':')[0]] \
                    [DataDictionaryConstants.DICT_KEY_CRITERIA][label.split(':')[1]] \
                    [DataDictionaryConstants.DATA_KEY_LIMITS][0]
                mean = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][label.split(':')[0]] \
                    [DataDictionaryConstants.DICT_KEY_CRITERIA][label.split(':')[1]] \
                    [DataDictionaryConstants.DATA_KEY_LIMITS][1]
                worst = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][label.split(':')[0]] \
                    [DataDictionaryConstants.DICT_KEY_CRITERIA][label.split(':')[1]] \
                    [DataDictionaryConstants.DATA_KEY_LIMITS][2]
                value = data_to_be_plotted[nr]
                if calculation_method == DropdownConstants.BEST_WORST:
                    return_factors[nr] = (value - worst) / (best - worst)
                elif Globals.nr_simulations == 1:
                    return_factors[nr] = None
                elif Globals.nr_simulations > 1 and max == mean and max == min:
                    return_factors[nr] = None
                elif calculation_method == DropdownConstants.MAX_MIN:
                    if best < worst:
                        return_factors[nr] = (value - max) / (min - max)
                    else:
                        return_factors[nr] = (value - min) / (max - min)
                elif calculation_method == DropdownConstants.MAX_MEAN:
                    if best < worst:
                        return_factors[nr] = (value - max) / (mean_crit - max)
                    else:
                        return_factors[nr] = (value - min) / (mean_crit - min)
                elif calculation_method == DropdownConstants.MEAN_MIN:
                    if best < worst:
                        return_factors[nr] = (value - mean_crit) / (min - mean_crit)
                    else:
                        return_factors[nr] = (value - mean_crit) / (max - mean_crit)
            else:
                value = data_to_be_plotted[nr]
                if Globals.nr_simulations == 1:
                    return_factors[nr] = None
                elif Globals.nr_simulations > 1 and max == mean_crit and max == min:
                    return_factors[nr] = None
                elif calculation_method == DropdownConstants.BEST_WORST:
                    return_factors[nr] = None
                elif calculation_method == DropdownConstants.MAX_MIN:
                    return_factors[nr] = (value - min) / (max - min)
                elif calculation_method == DropdownConstants.MAX_MEAN:
                    return_factors[nr] = (value - mean_crit) / (max - mean_crit)
                elif calculation_method == DropdownConstants.MEAN_MIN:
                    return_factors[nr] = (value - min) / (mean_crit - min)
                implicit[nr] = True
        return return_factors, implicit

    @staticmethod
    def get_min_max_from_prefiltered_data(boolean_filter_dict, data_grid, criteria_names):
        """ For each criteria it sorts the values from the smallest to the largest and returns a dictionary of criteria
        names and corresponding sorted values.
        Args:
             boolean_filter_dict (dict): Dictionary with boolean values over which determines which criteria should be
                                         considered
             data_grid (numpy nd-array): Numpy array with the values to be sorted
             criteria_names (list): List of criteria names from input files
        Returns:
             sorted_crit_values_dict (dict): Dictionary of criteria names and corresponding sorted values.
        """
        consider_criteria = np.array(
            [[boolean_filter_dict[key][sim_nr] for key in boolean_filter_dict.keys()] for
             sim_nr, simulation_criteria_data
             in enumerate(data_grid)])

        simulations_to_consider = np.where(np.array([True if np.all(i) else False for i in consider_criteria]))
        sorted_crit_values_dict = {}
        if len(data_grid[simulations_to_consider]) != 0:
            max_values = np.nanmax(data_grid[simulations_to_consider], axis=0)
            min_values = np.nanmin(data_grid[simulations_to_consider], axis=0)
            mean_values = np.nanmean(data_grid[simulations_to_consider], axis=0)
            sorted_crit_values_dict = {key: [min_values[nr], mean_values[nr], max_values[nr]] for nr, key in
                                       enumerate(criteria_names)}
            return sorted_crit_values_dict
        return sorted_crit_values_dict

    @staticmethod
    def update_criteria_data_graph(*args):
        """ Creates list of data for the criteria data graph plot depending on the selected options and data from
        the criteria file.
        Args:
            args (tuple): Tuple with selected options and data from the global configuration part and criteria files
        Returns:
             {'data': data, 'layout': {'height': 600, 'margin': {'l': 30, 'b': 160, 't': 30, 'r': 150}}} (dict):
             Dictionary with data for criteria data graph
        """
        criteria_do_be_shown = args[0]
        graph_legend = args[1]
        calculation_method = args[2]
        visualisation_maximum = args[3]

        global_dropdown_values = args[4:-len(Globals.list_filter_criteria_range_slider_ids_)]
        criteria_slider_values = args[4 + len(Globals.list_of_global_dropdown_ids_):]

        data = []
        # 1) extract boolean filter criteria according to the selected filters by the user
        #
        boolean_filter_dict = FilterUtils.get_boolean_filter_dict_for_criteria(
            Globals.list_filter_criteria_range_slider_ids_,
            Globals.list_of_global_dropdown_ids_,
            Globals.working_dict_,
            criteria_slider_values,
            global_dropdown_values)

        criteria_names = CriteriaDataGraph.get_global_injury_criteria()
        nr_simulations = len(Globals.working_dict_[DataDictionaryConstants.PARENT_PART][criteria_names[0].split(':')[0]]
                             [DataDictionaryConstants.DICT_KEY_CRITERIA][criteria_names[0].split(':')[1]]
                             [DataDictionaryConstants.DATA_KEY_VALUE])
        nr_criteria = len(CriteriaDataGraph.get_global_injury_criteria())

        criteria_names_to_display = criteria_names.copy()
        for index, criteria in enumerate(criteria_names_to_display):
            if criteria.split(':')[0] in Globals.working_dict_[DataDictionaryConstants.PARENT_PART].keys():
                if Globals.working_dict_[DataDictionaryConstants.PARENT_PART][criteria.split(':')[0]] \
                        [DataDictionaryConstants.DICT_KEY_CRITERIA][criteria.split(':')[1]] \
                        [DataDictionaryConstants.DATA_KEY_LIMITS] is not None:
                    limits = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][criteria.split(':')[0]] \
                        [DataDictionaryConstants.DICT_KEY_CRITERIA][criteria.split(':')[1]] \
                        [DataDictionaryConstants.DATA_KEY_LIMITS]
                else:
                    criteria_names_to_display[index] += '_()_()'
                    continue
                value = [a for a in Globals.working_dict_[DataDictionaryConstants.PARENT_PART][criteria.split(':')[0]] \
                    [DataDictionaryConstants.DICT_KEY_CRITERIA][criteria.split(':')[1]] \
                    [DataDictionaryConstants.DATA_KEY_VALUE] if a is not None]
                if all(n < 0 for n in limits) and all(n < 0 for n in value):
                    criteria_names_to_display[index] += "_(-)_"
                elif all(n > 0 for n in limits) and all(n > 0 for n in value):
                    criteria_names_to_display[index] += "_(+)_"
                else:
                    criteria_names_to_display[index] += "_()_"

                if abs(limits[0]) <= abs(limits[2]):
                    criteria_names_to_display[index] += "(b<)"
                elif abs(limits[0]) > abs(limits[2]):
                    criteria_names_to_display[index] += "(b>)"
                else:
                    criteria_names_to_display[index] += "()"

        # 2) extract the criteria values from each simulation
        #
        data_grid = np.zeros(shape=(nr_simulations, nr_criteria))
        overall_injury_index = 0
        for body_region in Globals.working_dict_[DataDictionaryConstants.PARENT_PART].keys():
            for criteria in list(Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region][
                                     DataDictionaryConstants.DICT_KEY_CRITERIA]):
                copy_list = copy.deepcopy(Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region][
                                              DataDictionaryConstants.DICT_KEY_CRITERIA][criteria] \
                                              [DataDictionaryConstants.DATA_KEY_VALUE])
                for i, value in enumerate(copy_list):
                    if value == '-' or value is None:
                        copy_list[i] = np.nan

                data_grid[:, overall_injury_index] = copy_list

                # for i in data_grid
                overall_injury_index += 1

        # 3) get max, mean, min from all criteria, with respect to the filter criteria
        #
        crit_dict_min_mean_max = CriteriaDataGraph.get_min_max_from_prefiltered_data(boolean_filter_dict, data_grid,
                                                                                     criteria_names)
        # 4) go through all simulations and filter data that should be shown
        #
        legends_list = []
        for sim_nr, simulation_criteria_data in enumerate(data_grid):

            data_idx_to_be_plotted = np.where(np.isfinite(simulation_criteria_data))
            assert len(data_idx_to_be_plotted) > 0
            data_idx_to_be_plotted = data_idx_to_be_plotted[0]

            dropdown_idx_to_be_plotted = np.array([i for x in criteria_do_be_shown
                                                   for i, name in enumerate(criteria_names) if x == name])

            # find the subset of the dropdown selection and the available data
            idx_to_be_plotted = np.intersect1d(dropdown_idx_to_be_plotted, data_idx_to_be_plotted)

            # check if all filter criteria are met!
            met_criteria = [boolean_filter_dict[key][sim_nr] for key in boolean_filter_dict.keys()]

            if len(idx_to_be_plotted) != 0 and all(met_criteria):
                color_to_curve = 'grey'
                showlegend = False
                legend_name = ''
                if graph_legend is not None:
                    color_to_curve = DiagramDataGraph.get_color_by_group(graph_legend,
                                                                         Globals.working_dict_[
                                                                             graph_legend.replace('dropdown_', "")]
                                                                         [sim_nr])
                    showlegend = Globals.working_dict_[graph_legend.replace('dropdown_', "")][
                                     sim_nr] not in legends_list
                    legends_list.append(Globals.working_dict_[graph_legend.replace('dropdown_', "")][sim_nr])
                    legend_name = Globals.working_dict_[graph_legend.replace('dropdown_', "")][sim_nr]

                data_to_be_plotted = simulation_criteria_data
                labels_to_be_plotted = np.array(criteria_names)

                data_to_be_plotted, implicit = CriteriaDataGraph.get_value_from_normalization(data_to_be_plotted,
                                                                                              calculation_method,
                                                                                              labels_to_be_plotted,
                                                                                              crit_dict_min_mean_max)

                if visualisation_maximum is not None:
                    for ind, dat_val in enumerate(data_to_be_plotted):
                        if dat_val <= visualisation_maximum and dat_val >= (-visualisation_maximum):
                            pass
                        else:
                            if len(np.where(idx_to_be_plotted == ind)[0]) == 0:
                                continue
                            index_to_be_deleted = np.where(idx_to_be_plotted == ind)[0][0]
                            idx_to_be_plotted = np.delete(idx_to_be_plotted, index_to_be_deleted)

                symbols = np.full(len(labels_to_be_plotted), 'diamond-open')
                colors_for_symbols = np.full(len(labels_to_be_plotted), 'black')

                symbols[np.where(implicit)] = 'circle'
                colors_for_symbols[np.where(implicit)] = 'grey'
                d = {'type': 'scatter',
                     'legendgroup': legend_name,
                     'mode': 'lines+markers',
                     'name': legend_name,
                     'x': np.array(criteria_names_to_display)[idx_to_be_plotted],
                     'y': data_to_be_plotted[idx_to_be_plotted],
                     'showlegend': showlegend,
                     'line': dict(
                         color=color_to_curve),
                     'marker': dict(symbol=symbols, color=colors_for_symbols)}
                data.append(d)
        return {'data': data, 'layout': {'height': 600, 'margin': {'l': 30, 'b': 160, 't': 30, 'r': 150}}}

    @staticmethod
    def update_criteria_data_graph_dropdown_multiple_selection(criteria_graph_radiobutton_value):
        """ Goes through all the criteria from the input files and returns a list of criteria depending
        on the criteria radiobutton.
        Args:
            criteria_graph_radiobutton_value (str): The value selected with the radiobutton
        Returns:
            crit_in_dropdown (list): List of criteria in relation to criteria radiobutton
        """
        crit_in_dropdown = []
        for key in Globals.working_dict_[DataDictionaryConstants.PARENT_PART].keys():
            crits = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][key] \
                [DataDictionaryConstants.DICT_KEY_CRITERIA]
            for crit in crits:
                type_ = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][key] \
                    [DataDictionaryConstants.DICT_KEY_CRITERIA][crit] \
                    [DataDictionaryConstants.DATA_KEY_TYPE]
                if type_ == criteria_graph_radiobutton_value or criteria_graph_radiobutton_value == 'all':
                    crit_in_dropdown.append(key + ':' + crit)
        return crit_in_dropdown


class GroupAssessment:
    @staticmethod
    def group_assessment_export__csv(n_clicks, *args):
        global_dropdown_values = args[1:-len(Globals.list_filter_criteria_range_slider_ids_)]
        criteria_slider_values = args[1 + len(Globals.list_of_global_dropdown_ids_):]

        boolean_filter_dict = FilterUtils.get_boolean_filter_dict_for_criteria(
            Globals.list_filter_criteria_range_slider_ids_,
            Globals.list_of_global_dropdown_ids_,
            Globals.working_dict_,
            criteria_slider_values,
            global_dropdown_values)

        remaining_simulations = [Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][sim] for sim in
                                 range(Globals.getNumOfSimulation()) if
                                 all([boolean_filter_dict[key][sim] for key in boolean_filter_dict.keys()])]

        if n_clicks > 0 and Globals.groups_file_location != None:
            for model in Globals.model_list_:
                Groups.print_groups_to_csv(model)
                Groups.print_rated_groups_to_csv(remaining_simulations, model)
        return

    @staticmethod
    def update_table(df, remaining_simulations):
        simulations = []
        for model in Globals.model_list_:
            for sim in Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME]:
                simulations.append(sim + '-' + model)

        df.set_index(DataDictionaryConstants.SIMULATIONS, inplace=True)
        for sim in simulations:
            if sim not in remaining_simulations and sim in df.index.values:
                df.drop(sim, inplace=True)

        if len(remaining_simulations) == 0:
            return pd.DataFrame()
        return df

    @staticmethod
    def get_group_in_dropdown_by_radio_button_selector(groups_radiobutton_value):
        groups_in_dropdown = []

        if groups_radiobutton_value == 'all':
            groups_in_dropdown = [Groups.get_group_name(group) for group in Globals.group_list_]
            groups_in_dropdown += [DataDictionaryConstants.LOADCASE_RATING]
        return groups_in_dropdown

    @staticmethod
    def get_table(df, selected_groups, remaining_simulations, id_prefix):
        df.index = np.arange(0, len(df))

        df = GroupAssessment.update_table(df, remaining_simulations)
        df = df.reset_index()
        if df.empty:
            children = [dt.DataTable(id=id_prefix, columns=[{"name": i, "id": i, "deletable": True, 'hidden': True}
                                                            if i in selected_groups + ['Simulations']
                                                            else {"name": i, "id": i, "deletable": True, 'hidden': True}
                                                            for i in df.columns], data=df.to_dict('records'))]

        else:
            children = [
                dt.DataTable(id=id_prefix, columns=[{"name": i, "id": i, "deletable": True, 'hidden': False}
                                                    if i in selected_groups + ['Simulations']
                                                    else {"name": i, "id": i, "deletable": True, 'hidden': True}
                                                    for i in df.columns],
                             data=df.to_dict('records'),
                             style_data_conditional=Globals.get_table_styles(df), filter_action='native',
                             sort_action='native',
                             row_deletable=True, style_data={'textAlign': 'center'},
                             fixed_rows={'headers': True, 'data': 0},
                             style_table={'overflowX': 'scroll'},
                             style_cell={'width': '150px', 'minWidth': '0px', 'maxWidth': '150px',
                                         'whiteSpace': 'normal'}
                             )
            ]
        return children

    @staticmethod
    def display_tables(selected_groups, *args):
        tables = []

        renamed_selected_groups = [Groups.get_group_name(x) for x in selected_groups]
        global_dropdown_values = args[1:-len(Globals.list_filter_criteria_range_slider_ids_)]
        criteria_slider_values = args[1 + len(Globals.list_of_global_dropdown_ids_):]

        boolean_filter_dict = FilterUtils.get_boolean_filter_dict_for_criteria(
            Globals.list_filter_criteria_range_slider_ids_,
            Globals.list_of_global_dropdown_ids_,
            Globals.working_dict_,
            criteria_slider_values,
            global_dropdown_values)
        # get the names of the simulation that meet the filter criteria

        temp_simulations = [Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][sim]
                            for sim in range(Globals.getNumOfSimulation())
                            if all([boolean_filter_dict[key][sim] for key in boolean_filter_dict.keys()])]
        remaining_simulations = []
        for model_nr, model in enumerate(Globals.model_list_):
            for sim in temp_simulations:
                remaining_simulations.append(sim + '-' + model)
        if Globals.groups_file_location is not None:
            df = GroupAssessment.get_table(Groups.get_main_dataframe(), renamed_selected_groups, remaining_simulations,
                           id_prefix=("table_groups"))
        else:
            return

        tables.append(html.Div(className="table_groups", children=df))

        return tables

    @staticmethod
    def update_in_depth_table(df, remaining_simulations, id_prefix):
        simulations = []
        for model in Globals.model_list_:
            for sim in Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME]:
                simulations.append(sim + '-' + model)
        if int(id_prefix[-1]) != 9:
            df.set_index(Groups.get_group_name("Group" + str(int(id_prefix[-1]))), inplace=True)
        else:
            df.set_index(DataDictionaryConstants.LOADCASE_LCA, inplace=True)
        for sim in simulations:
            if sim not in remaining_simulations and sim in df.index.values:
                df.drop(sim, inplace=True)

        if len(remaining_simulations) == 0:
            return pd.DataFrame()
        return df

    @staticmethod
    def get_in_depth_table(df, selected_groups, remaining_simulations, id_prefix):
        df.index = np.arange(0, len(df))

        df = GroupAssessment.update_in_depth_table(df, remaining_simulations, id_prefix)
        df = df.reset_index()
        if df.empty:
            children = [dt.DataTable(id=id_prefix, columns=[
                {"name": i, "id": i, "deletable": True, 'hidden': True} for i in df.columns],
                                     data=df.to_dict('records'))]

        elif id_prefix != "table_5":
            children = [
                dt.DataTable(id=id_prefix, columns=[
                    {"name": i, "id": i, "deletable": True, 'hidden': False} for i in df.columns],
                             data=df.to_dict('records'),
                             style_data_conditional=Globals.get_table_styles(df), filter_action='native',
                             sort_action='native',
                             row_deletable=True, style_data={'textAlign': 'center'},
                             fixed_rows={'headers': True, 'data': 0},
                             style_table={'overflowX': 'scroll'},
                             style_cell={'width': '150px', 'minWidth': '0px', 'maxWidth': '150px',
                                         'whiteSpace': 'normal'}
                             )
            ]
        else:
            children = [
                dt.DataTable(id=id_prefix, columns=[
                    {"name": i, "id": i, "deletable": True, 'hidden': False} for i in df.columns],
                             data=df.to_dict('records'),
                             style_data_conditional=Globals.get_table_styles_group_5(df), filter_action='native',
                             sort_action='native',
                             row_deletable=True, style_data={'textAlign': 'center'},
                             fixed_rows={'headers': True, 'data': 0},
                             style_table={'overflowX': 'scroll'},
                             style_cell={'width': '150px', 'minWidth': '0px', 'maxWidth': '150px',
                                         'whiteSpace': 'normal'}
                             )]
        return children


    @staticmethod
    def display_in_depth_tables(selected_groups, *args):
        tables = []

        global_dropdown_values = args[1:-len(Globals.list_filter_criteria_range_slider_ids_)]
        criteria_slider_values = args[1 + len(Globals.list_of_global_dropdown_ids_):]

        boolean_filter_dict = FilterUtils.get_boolean_filter_dict_for_criteria(
            Globals.list_filter_criteria_range_slider_ids_,
            Globals.list_of_global_dropdown_ids_,
            Globals.working_dict_,
            criteria_slider_values,
            global_dropdown_values)

        temp_simulations = [Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][sim] for sim in
                            range(Globals.getNumOfSimulation()) if
                            all([boolean_filter_dict[key][sim] for key in boolean_filter_dict.keys()])]
        remaining_simulations = []
        for model_nr, model in enumerate(Globals.model_list_):
            for sim in temp_simulations:
                remaining_simulations.append(sim + '-' + model)
        if Globals.groups_file_location is not None:
            rename_selected_groups = []

            for group in Globals.group_list_:
                if group == DataDictionaryConstants.DICT_KEY_LOADCASE:
                    if DataDictionaryConstants.LOADCASE_LCA in selected_groups:
                        rename_selected_groups.append(DataDictionaryConstants.DICT_KEY_LOADCASE)
                elif DataDictionaryConstants.NAME in Globals.groups_info_dict_[Globals.model_list_[0]][group].keys() and \
                        Globals.groups_info_dict_[Globals.model_list_[0]][group][DataDictionaryConstants.NAME][0] in \
                        selected_groups:
                    rename_selected_groups.append(group)
                else:
                    if group in selected_groups:
                        rename_selected_groups.append(group)

            for group in rename_selected_groups:
                if group != DataDictionaryConstants.DICT_KEY_LOADCASE:
                    tables.append(html.Div(className=("table_group_" + group[-1]),
                                           children=GroupAssessment.get_in_depth_table(
                                               Groups.get_detailed_dataframe(group),
                                               rename_selected_groups,
                                               remaining_simulations,
                                               id_prefix=("table_" + group[-1]))))
                    tables.append(html.Br())
                else:
                    tables.append(html.Div(className="table_group_loadcase",
                                           children=GroupAssessment.get_in_depth_table(
                                               Groups.get_detailed_dataframe(group),
                                               rename_selected_groups,
                                               remaining_simulations,
                                               id_prefix=("table_" + "9"))))
                    tables.append(html.Br())
        else:
            return
        return tables

