import os
import numpy as np
import pandas as pd
import math
import copy

from utils.constants import DataDictionaryConstants
from utils.define_globals import Globals
from functools import reduce


class Groups:
    groups_dict_ = {}
    dataframe_dict_ = {}
    options_for_group_dropdown_ = []
    options_for_in_depth_group_dropdown_ = []
    nr_groups = 0

    @staticmethod
    def init_groups():
        if Globals.groups_file_location != None:
            Groups.set_num_of_groups(len(Globals.group_list_))
            for model in Globals.model_list_:
                Groups.groups_dict_[model] = {}
            Groups.get_groups()
            Groups.get_dataframe_dict()

    @staticmethod
    def get_num_of_groups():
        """ Returns the number of groups saved in Groups.nr_groups
        Returns:
            Groups.nr_groups (int): Number of groups
        """
        return Groups.nr_groups

    @staticmethod
    def set_num_of_groups(nr_groups):
        """ Sets Groups.nr_groups to nr_groups number
        Args:
            nr_groups (int): The number to which Groups.nr_groups should be set
        """
        Groups.nr_groups = nr_groups

    @staticmethod
    def get_dataframe_dict():
        """ Updates the Groups.dataframe_dict_ with information from the input group file.
        """
        if Globals.groups_file_location != None:
            merge_dict = {}
            #outer_merge = partial(pd.concat, axis = 1)#partial(pd.merge, how='outer')

            for model in Globals.model_list_:
                dataframes = []
                for group in Globals.group_list_:
                    dataframes.append(Groups.get_in_depth_dataframe(model, group))
                Groups.dataframe_dict_[model] = dataframes
            for i in range(Groups.get_num_of_groups()):
                merge_list = [item[i] for item in Groups.dataframe_dict_.values()] #if not item[i].empty]

                merge_dict[i] = (pd.concat(merge_list, axis = 1, sort = False))#.rename(columns =renamer())#reduce(outer_merge, merge_list)
                merge_dict[i].index.name = DataDictionaryConstants.SIMULATIONS
                merge_dict[i] = (merge_dict[i].replace(np.nan, '-')).reset_index()
            Groups.dataframe_dict_["Merged"] = merge_dict

    #computes the groups for all groups in the group file.
    @staticmethod
    def get_groups():
        for model in Globals.model_list_:
            for group in Globals.group_list_:
                if group == DataDictionaryConstants.DICT_KEY_LOADCASE or \
                        len(Globals.groups_info_dict_[model][group].keys()) != 0:
                    if group != DataDictionaryConstants.GROUP_5 and group != DataDictionaryConstants.DICT_KEY_LOADCASE:
                        Groups.get_group_quotients(model, group)
                        #if group != DataDictionaryConstants.GROUP_7: #group != DataDictionaryConstants.GROUP_6 and group != DataDictionaryConstants.GROUP_7:
                        Groups.normalize_values(model, group)
                        Groups.get_group_sum(model, group)
                        if group != DataDictionaryConstants.GROUP_5 and group != \
                                DataDictionaryConstants.DICT_KEY_LOADCASE:
                            Groups.get_region_sum(model, group)
                        if group == DataDictionaryConstants.GROUP_6 or group == DataDictionaryConstants.GROUP_7:
                            Groups.count_criteria(model, group)
                    else:
                        if group != DataDictionaryConstants.DICT_KEY_LOADCASE:
                            Groups.get_group_quotients(model, group)
                            Groups.normalize_values(model, group)
                            Groups.get_group_5(model)
                            Groups.change_rr_to_stars(model)

        for model in Globals.model_list_:
            Groups.get_loadcase(model)
            Groups.modify_loadcase(model)
        Groups.groups_dict_ = Groups.truncate_values(Groups.groups_dict_)

        Groups.options_for_group_dropdown_ = [{'label': Groups.get_group_name(group),
                                               'value': Groups.get_group_name(group)}
                                              for group in Globals.group_list_] + \
                                             [{'label':DataDictionaryConstants.LOADCASE_RATING,
                                               'value': DataDictionaryConstants.LOADCASE_RATING}]
        Groups.options_for_in_depth_group_dropdown_ = [{'label': Groups.get_group_name(group),
                                                        'value': Groups.get_group_name(group)}
                                                       for group in Globals.group_list_]

    @staticmethod
    def print_groups_to_csv(MODEL):
        for group in Globals.group_list_:
            df = Groups.get_in_depth_dataframe(MODEL, group)
            if not df.empty:
                if not os.path.exists(os.path.join(Globals.groups_file_location, MODEL)):
                    os.makedirs(os.path.join(Globals.groups_file_location, MODEL))
                df.to_csv(os.path.join(Globals.groups_file_location, MODEL,
                                       Groups.get_group_name(group) + '.csv'), sep=';')

    #return dataframes
    @staticmethod
    def get_in_depth_dataframe(MODEL, GROUP):
        filter_list = list(Groups.filter_for_model_info(MODEL))
        simulation_list = Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME]
        remaining_simulations = []
        for index, filter in enumerate(filter_list):
            if filter == 1:
                remaining_simulations.append(simulation_list[index]+"-"+MODEL)

        data_for_empty_dataframe = {DataDictionaryConstants.OVERALL : [np.nan for x in remaining_simulations]}
        df = pd.DataFrame.from_dict(data_for_empty_dataframe, orient ='index', columns = remaining_simulations)

        ###################GROUPS 1-4 and 7-8############################
        if GROUP != DataDictionaryConstants.GROUP_5 and GROUP != DataDictionaryConstants.GROUP_6 and GROUP != \
                DataDictionaryConstants.GROUP_7 and GROUP != DataDictionaryConstants.DICT_KEY_LOADCASE:
            if GROUP in Globals.groups_info_dict_[MODEL].keys():
                if len(Groups.groups_dict_[MODEL][GROUP].keys()) != 1:
                    print_data = {}

                    for body_part in Groups.groups_dict_[MODEL][GROUP]:
                        if body_part != DataDictionaryConstants.OVERALL and body_part != \
                                DataDictionaryConstants.DICT_KEY_WARNING and body_part != Globals.visual_assessment:
                            forbidden_index = []
                            filtered_data = list(Groups.groups_dict_[MODEL][GROUP][body_part][
                                                     DataDictionaryConstants.OVERALL])
                            for i, x in enumerate(filter_list):
                                if x == 1:
                                    continue
                                else:
                                    forbidden_index.append(i)

                            filtered_data = [filtered_data[i] for i, e in enumerate(filtered_data) if
                                             i not in forbidden_index]
                            print_data[body_part] = filtered_data
                        if body_part == Globals.visual_assessment:
                            forbidden_index = []
                            filtered_data = list(Groups.groups_dict_[MODEL][GROUP][body_part])
                            for i, x in enumerate(filter_list):
                                if x == 1:
                                    continue
                                else:
                                    forbidden_index.append(i)
                            filtered_data = [filtered_data[i] for i, e in enumerate(filtered_data) if
                                             i not in forbidden_index]
                            print_data[body_part] = filtered_data

                    filtered_data = list(Groups.groups_dict_[MODEL][GROUP][
                                             DataDictionaryConstants.OVERALL])
                    filtered_data = [filtered_data[i] for i, e in enumerate(filtered_data) if i not in forbidden_index]
                    print_data[DataDictionaryConstants.OVERALL] = filtered_data

                    df = pd.DataFrame.from_dict(print_data, orient='index',
                                                columns=remaining_simulations)
                    df.index.name = Groups.get_group_name(GROUP)

                    return df
                    #dataframes.append(df.reset_index())

            ################## GROUP 5 ############################
        if GROUP == DataDictionaryConstants.GROUP_5:
            print_data = {}

            if GROUP in Globals.groups_info_dict_[MODEL].keys():
                for body_part in Groups.groups_dict_[MODEL][GROUP]:
                    forbidden_index = []
                    if body_part in Globals.body_regions_:
                        filtered_data = list(Groups.groups_dict_[MODEL][GROUP][body_part]
                                             [DataDictionaryConstants.OVERALL])
                    else:
                        filtered_data = list(Groups.groups_dict_[MODEL][GROUP][body_part])

                    for i, x in enumerate(filter_list):
                        if x == 1:
                            continue
                        else:
                            forbidden_index.append(i)
                    filtered_data = [filtered_data[i] for i, e in enumerate(filtered_data) if i not in forbidden_index]
                    print_data[body_part] = filtered_data
                df = pd.DataFrame.from_dict(print_data, orient='index',
                                            columns=remaining_simulations)
                df.index.name = Groups.get_group_name(GROUP)
                #df.to_csv(Globals.groups_file_location + '\\' + MODEL + '\\' + 'group_5.csv', sep=';')
                return df
                #dataframes.append(df.reset_index())

        ################### GROUP 6 ############################
        if GROUP == DataDictionaryConstants.GROUP_6:
            print_data = {}
            forbidden_index = []
            if GROUP in Globals.groups_info_dict_[MODEL].keys():
                filtered_data = list(
                    Groups.groups_dict_[MODEL][GROUP][DataDictionaryConstants.OVERALL])
                for i, x in enumerate(filter_list):
                    if x == 1:
                        continue
                    else:
                        forbidden_index.append(i)
                filtered_data = [filtered_data[i] for i, e in enumerate(filtered_data) if i not in forbidden_index]
                print_data[DataDictionaryConstants.OVERALL] = filtered_data
                df = pd.DataFrame.from_dict(print_data, orient='index',
                                            columns=remaining_simulations)
                df.index.name = Groups.get_group_name(GROUP)
                #df.to_csv(Globals.groups_file_location + '\\' + MODEL + '\\' + 'group_6.csv', sep=';')
                return df
                #dataframes.append(df.reset_index())

        # ################### GROUP 7 ############################
        if GROUP == DataDictionaryConstants.GROUP_7:
            print_data = {}
            forbidden_index = []
            if GROUP in Globals.groups_info_dict_[MODEL].keys():
                filtered_data_1 = list(
                    Groups.groups_dict_[MODEL][GROUP][DataDictionaryConstants.OVERALL])
                filtered_data_2 = list(Groups.groups_dict_[MODEL][GROUP][
                                           Globals.visual_assessment])
                for i, x in enumerate(filter_list):
                    if x == 1:
                        continue
                    else:
                        forbidden_index.append(i)
                filtered_data_1 = [filtered_data_1[i] for i, e in enumerate(filtered_data_1)
                                   if i not in forbidden_index]
                filtered_data_2 = [filtered_data_2[i] for i, e in enumerate(filtered_data_2)
                                   if i not in forbidden_index]
                print_data[DataDictionaryConstants.OVERALL] = filtered_data_1
                print_data[Globals.visual_assessment] = filtered_data_2
                df = pd.DataFrame.from_dict(print_data, orient='index',
                                            columns=remaining_simulations)
                df.index.name = Groups.get_group_name(GROUP)
                #df.to_csv(Globals.groups_file_location + '\\' + MODEL + '\\' + 'group_7.csv', sep=';')
                return df
                #dataframes.append(df.reset_index())

        ################### LOADCASE ############################
        if GROUP == DataDictionaryConstants.DICT_KEY_LOADCASE:
            print_data = {}
            forbidden_index = []
            if GROUP in Globals.group_list_:
                filtered_data_1 = list(
                    Groups.groups_dict_[MODEL][GROUP][DataDictionaryConstants.OVERALL])
                filtered_data_2 = list(Groups.groups_dict_[MODEL][GROUP][
                                           DataDictionaryConstants.DICT_KEY_LCM_GROUPS])
                for i, x in enumerate(filter_list):
                    if x == 1:
                        continue
                    else:
                        forbidden_index.append(i)
                filtered_data_1 = [filtered_data_1[i] for i, e in enumerate(filtered_data_1)
                                   if i not in forbidden_index]
                filtered_data_2 = [filtered_data_2[i] for i, e in enumerate(filtered_data_2)
                                   if i not in forbidden_index]
                print_data[DataDictionaryConstants.OVERALL] = filtered_data_1
                print_data[DataDictionaryConstants.DICT_KEY_LCM_GROUPS] = filtered_data_2
                df = pd.DataFrame.from_dict(print_data, orient='index',
                                            columns=remaining_simulations)
                df.index.name = Groups.get_group_name(GROUP)
                #df.to_csv(Globals.groups_file_location + '\\' + MODEL + '\\' + 'loadcase.csv', sep=';')
                return df
                #dataframes.append(df.reset_index())
        return df


    @staticmethod
    def get_group_quotients(MODEL, GROUP):
        criteria_dict = {}

        if len(Globals.groups_info_dict_[MODEL][GROUP][DataDictionaryConstants.DATA_KEY_LIMITS]) != 2:
            print("Error, please enter valid limit bounds.")
        else:
            criteria_limit_str_best = Globals.groups_info_dict_[MODEL][GROUP] \
                                                               [DataDictionaryConstants.DATA_KEY_LIMITS][0]
            criteria_limit_str_worst = Globals.groups_info_dict_[MODEL][GROUP] \
                                                                [DataDictionaryConstants.DATA_KEY_LIMITS][1]

        for body_region in Globals.groups_info_dict_[MODEL][GROUP][DataDictionaryConstants.DICT_KEY_CRITERIA].keys():
            criteria_dict[body_region] = {}
            for counter, criteria in enumerate(
                    Globals.groups_info_dict_[MODEL][GROUP][DataDictionaryConstants.DICT_KEY_CRITERIA][body_region]):
                # convert to numpy array for convienence on NaN value manipulation
                values = np.array(Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region][
                                      DataDictionaryConstants.DICT_KEY_CRITERIA][criteria][
                                      DataDictionaryConstants.DATA_KEY_VALUE], dtype=np.float)

                limits = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region][
                    DataDictionaryConstants.DICT_KEY_CRITERIA][criteria][DataDictionaryConstants.DATA_KEY_LIMITS]
                if limits is not None:
                    criteria_limits = np.array(limits, dtype=np.float)

                    if criteria_limits.size == 0:
                        continue

                    if criteria_limit_str_worst == 'worst':
                        criteria_limit_worst = criteria_limits[2]
                    elif criteria_limit_str_worst == 'middle':
                        criteria_limit_worst = criteria_limits[1]
                    elif criteria_limit_str_worst == 'best':
                        criteria_limit_worst = criteria_limits[0]

                    if criteria_limit_str_best == 'worst':
                        criteria_limit_best = criteria_limits[2]
                    elif criteria_limit_str_best == 'middle':
                        criteria_limit_best = criteria_limits[1]
                    elif criteria_limit_str_best == 'best':
                        criteria_limit_best = criteria_limits[0]

                    # Computation of the quotients
                    numerator = values - criteria_limit_worst
                    denominator = criteria_limit_best - criteria_limit_worst
                    quotient = numerator / denominator

                    criteria_dict[body_region][
                        Globals.groups_info_dict_[MODEL][GROUP][DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                 [body_region][counter]] = quotient

        Groups.groups_dict_[MODEL][GROUP] = criteria_dict

    @staticmethod
    def normalize_values(MODEL, GROUP):
        for body_region in Groups.groups_dict_[MODEL][GROUP]:
            for criteria in Groups.groups_dict_[MODEL][GROUP][body_region]:
                if body_region != DataDictionaryConstants.OVERALL:
                    Groups.groups_dict_[MODEL][GROUP][body_region][criteria] = \
                        [1 if x > 1 else 0 if x < 0 else x for x in Groups.groups_dict_[MODEL][GROUP]
                                                                                       [body_region][criteria]]

    @staticmethod
    def get_group_sum(MODEL, GROUP):
        group_overall = np.zeros(Globals.getNumOfSimulation())

        count_zeros = np.zeros(Globals.getNumOfSimulation())
        count_criteria = np.zeros(Globals.getNumOfSimulation())

        for body_region in Groups.groups_dict_[MODEL][GROUP].keys():
            for criteria in Groups.groups_dict_[MODEL][GROUP][body_region]:
                cnt_crit_is_nan = pd.notna(Groups.groups_dict_[MODEL][GROUP][body_region][criteria])
                cnt_crit_is_nan = np.array(list(map(int, cnt_crit_is_nan)))
                count_criteria = np.add(count_criteria, cnt_crit_is_nan)

                group_overall += Groups.groups_dict_[MODEL][GROUP][body_region][criteria]
                temp_counter = np.where(np.array(Groups.groups_dict_[MODEL][GROUP][body_region][criteria]) == 0, 1, 0)
                count_zeros += temp_counter

        count_zeros = 1 - np.where(count_zeros != 0, 1, 0)

        group_overall = np.divide(group_overall, count_criteria, out=np.zeros_like(group_overall),
                                  where=count_criteria != 0)
        group_overall *= count_zeros

        Groups.groups_dict_[MODEL][GROUP][DataDictionaryConstants.OVERALL] = group_overall

    @staticmethod
    def get_region_sum(MODEL, GROUP):
        for body_region in Globals.groups_info_dict_[MODEL][GROUP][DataDictionaryConstants.DICT_KEY_CRITERIA].keys():
            values = np.zeros(Globals.getNumOfSimulation())
            count_criteria = np.zeros(Globals.getNumOfSimulation())
            count_zeros = np.zeros(Globals.getNumOfSimulation())
            for criteria in Groups.groups_dict_[MODEL][GROUP][body_region]:
                quotient = Groups.groups_dict_[MODEL][GROUP][body_region][criteria]
                cnt_crit_is_nan = pd.notna(quotient)
                cnt_crit_is_nan = np.array(list(map(int, cnt_crit_is_nan)))
                count_criteria = np.add(count_criteria, cnt_crit_is_nan)
                temp_counter = np.where(np.array(quotient) == 0, 1, 0)
                count_zeros += temp_counter
                values += quotient

            count_zeros = 1 - np.where(count_zeros != 0, 1, 0)
            values = np.divide(values, count_criteria, out=np.zeros_like(values),
                               where=count_criteria != 0)

            values *= count_zeros
            Groups.groups_dict_[MODEL][GROUP][body_region][DataDictionaryConstants.OVERALL] = values

    @staticmethod
    def get_group_5(MODEL):
        criteria_dict = {}
        risk = np.ones(Globals.getNumOfSimulation())
        for body_region in Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5][
            DataDictionaryConstants.DICT_KEY_CRITERIA].keys():
            criteria_dict[body_region] = {}
            body_region_risk = np.ones(Globals.getNumOfSimulation())
            warning_counter = 0
            for counter, criteria in enumerate(Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5][
                                                   DataDictionaryConstants.DICT_KEY_CRITERIA][body_region]):

                if warning_counter > 0:
                    print("Warning: More than one risk value for the model " + MODEL + " for the subgroup " +
                          body_region + " defined.")
                warning_counter += 1

                values = np.array(Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region]
                                  [DataDictionaryConstants.DICT_KEY_CRITERIA][criteria]
                                  [DataDictionaryConstants.DATA_KEY_VALUE])

                values = [np.nan if isinstance(x, str) else np.nan if x is None else x for x in
                                              values]

                body_region_risk = np.multiply(body_region_risk, np.ones(Globals.nr_simulations) - values)
                risk = np.multiply(risk, np.ones(Globals.nr_simulations) - values)
            Groups.groups_dict_[MODEL][DataDictionaryConstants.GROUP_5][body_region] \
                                [DataDictionaryConstants.OVERALL] = list(1-body_region_risk)

        if len(Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5]
               [DataDictionaryConstants.DICT_KEY_CRITERIA]) == 0:
            risk = np.array([np.nan for x in risk])

        Groups.groups_dict_[MODEL][DataDictionaryConstants.GROUP_5][DataDictionaryConstants.DICT_KEY_PJOINT] = \
            list(1 - risk)
        risk_rate = (1 - risk) / Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5][
            DataDictionaryConstants.BASELINE_RISK][0]

        Groups.groups_dict_[MODEL][DataDictionaryConstants.GROUP_5][DataDictionaryConstants.PJOINT_OVER_BASELINERISK] =\
            risk_rate
        limit_1 = \
        Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5][DataDictionaryConstants.MOD_RATING_LIMITS][1]
        limit_2 = \
        Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5][DataDictionaryConstants.MOD_RATING_LIMITS][0]
        # Set RR to 0 if value > upper limit, to 1 if value < lower limit +
        # calculation between defined group file ncap limits
        risk_rate = [0 if x >= limit_1 else 1 if x <= limit_2 else ((x - limit_1) / (limit_2 - limit_1))
                     for x in risk_rate]
        Groups.groups_dict_[MODEL][DataDictionaryConstants.GROUP_5][DataDictionaryConstants.MODIFIED_RATING] = risk_rate

    @staticmethod
    def count_criteria(MODEL, GROUP):
        Groups.groups_dict_[MODEL][GROUP][DataDictionaryConstants.DICT_KEY_WARNING] = {}
        for i in range(Globals.nr_simulations):
            Groups.groups_dict_[MODEL][GROUP][DataDictionaryConstants.DICT_KEY_WARNING][
                Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][i]] = {}
        counter_1 = np.zeros(Globals.getNumOfSimulation())
        counter_2 = np.zeros(Globals.getNumOfSimulation())
        counter_3 = np.zeros(Globals.getNumOfSimulation())
        criteria_limit_str = Globals.groups_info_dict_[MODEL][GROUP][DataDictionaryConstants.DATA_KEY_LIMITS][0]
        for body_region in Globals.groups_info_dict_[MODEL][GROUP][DataDictionaryConstants.DICT_KEY_CRITERIA].keys():
            for criteria in Globals.groups_info_dict_[MODEL][GROUP][DataDictionaryConstants.DICT_KEY_CRITERIA] \
                                                     [body_region]:

                if Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region][
                    DataDictionaryConstants.DICT_KEY_CRITERIA][criteria][
                    DataDictionaryConstants.DATA_KEY_LIMITS] is not None:

                    criteria_limits = np.array(Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region][
                                                   DataDictionaryConstants.DICT_KEY_CRITERIA][criteria][
                                                   DataDictionaryConstants.DATA_KEY_LIMITS], dtype=np.float)
                    if criteria_limits.size == 0:
                        continue

                    #criteria limit is only used to see if the limits are positive or negative.
                    if criteria_limit_str == 'worst':
                        criteria_limit = criteria_limits[2]
                    elif criteria_limit_str == 'middle':
                        criteria_limit = criteria_limits[1]
                    elif criteria_limit_str == 'best':
                        criteria_limit = criteria_limits[0]

                    value_sign_check = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region][
                        DataDictionaryConstants.DICT_KEY_CRITERIA][criteria][DataDictionaryConstants.DATA_KEY_VALUE]

                    forbidden_index = []
                    for i, x in enumerate(value_sign_check):
                        if x is None:
                            forbidden_index.append(i)
                            continue

                        if x < 0 and criteria_limit >= 0:
                            print("Warning: Sign problem for criteria: " + criteria + " in Simulation " +
                                  Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][
                                      i] + " in " + GROUP + " for the model " + MODEL)
                            Groups.groups_dict_[MODEL][GROUP][DataDictionaryConstants.DICT_KEY_WARNING][
                                Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][i]][body_region] = \
                                criteria
                            forbidden_index.append(i)
                        if x >= 0 and criteria_limit < 0:
                            print("Warning: Sign problem for criteria: " + criteria + " in Simulation " +
                                  Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][
                                      i] + " in " + GROUP + " for the model " + MODEL)
                            Groups.groups_dict_[MODEL][GROUP][DataDictionaryConstants.DICT_KEY_WARNING][
                                Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME][i]][body_region] = \
                                criteria
                            forbidden_index.append(i)

                    values = Groups.groups_dict_[MODEL][GROUP][body_region][criteria]

                    #Good values
                    counter_1 += [1 if x >= 1 else 0 for x in values]
                    for i, x in enumerate([1 if x >= 1 else 0 for x in values]):
                        if i in forbidden_index:
                            if x == 1:
                                counter_1[i] -= 1

                    #Poor values
                    counter_3 += [1 if x <= 0 else 0 for x in values]
                    for i, x in enumerate([1 if x <= 0 else 0 for x in values]):
                        if i in forbidden_index:
                            if x == 1:
                                counter_3[i] -= 1

                    #Marginal values
                    temp_list = 1 * ((0 < np.array(values)) & (np.array(values) < 1))
                    counter_2 += 1 * ((0 < np.array(values)) & (np.array(values) < 1))
                    for i, x in enumerate(temp_list):
                        if i in forbidden_index:
                            if x == 1:
                                counter_2[i] -= 1

        if GROUP == DataDictionaryConstants.GROUP_6:
            group = list(zip(counter_1, counter_2, counter_3))
            group = [tuple(list(map(int, x))) for x in group]
            Groups.groups_dict_[MODEL][GROUP][DataDictionaryConstants.OVERALL] = ["(%s; %s; %s)" % x for x in group]

        if GROUP == DataDictionaryConstants.GROUP_7:
            sensitivity = Globals.groups_info_dict_[MODEL][GROUP][DataDictionaryConstants.DICT_KEY_LIMIT_SENSITIVITY][0]
            group = list(zip(counter_1, counter_2, counter_3))
            group = [tuple(list(map(int, x))) for x in group]

            if Globals.visual_assessment not in Globals.working_dict_.keys():
                submarining_user = [0]*Globals.getNumOfSimulation()
            else:
                submarining_user = [1 if x == DataDictionaryConstants.YES else 0
                                    for x in Globals.working_dict_[Globals.visual_assessment]]

            submarining_1 = [x[2] for x in group]
            submarining_2 = [x[1] for x in group]

            if sensitivity == 0:
                submarining = np.array([DataDictionaryConstants.YES if x > 0 else DataDictionaryConstants.NO
                              if x == 0 else np.nan for x in np.array(submarining_1) + np.array(submarining_user)])
                submarining = submarining.tolist()
            else:
                submarining = np.array(submarining_1) + np.array(submarining_2) + np.array(submarining_user)
                submarining = [DataDictionaryConstants.YES if x > 0 else DataDictionaryConstants.NO
                               for x in submarining]

            Groups.groups_dict_[MODEL][GROUP][Globals.visual_assessment] = submarining
            submarining_array = ["(%s; %s; %s)" % x for x in group]

            if Globals.visual_assessment not in Globals.working_dict_.keys():
                submarining_user = ['(-)']*Globals.getNumOfSimulation()
            else:
                submarining_user = ["(y)" if x == DataDictionaryConstants.YES else "(n)"
                                    if x == DataDictionaryConstants.NO
                                    else  "(-)" for x in Globals.working_dict_[Globals.visual_assessment]]

            submarining_combined = [m+n+o for m,n,o in zip(submarining,submarining_array, submarining_user)]


            if len(Globals.groups_info_dict_[MODEL][GROUP][DataDictionaryConstants.DICT_KEY_CRITERIA].keys()) == 0:
                submarining_combined = ['YES(-)(y)' if x == '(y)' else 'NO(-)(n)' if x == '(n)'
                                        else '-' for x in submarining_user]
                Groups.groups_dict_[MODEL][GROUP][Globals.visual_assessment] = [x.split('(')[0]
                                                                                for x in submarining_combined]
            Groups.groups_dict_[MODEL][GROUP][DataDictionaryConstants.OVERALL] = submarining_combined



    @staticmethod
    def get_loadcase(MODEL):
        loadcase = np.zeros(Globals.nr_simulations)
        count_nan = np.zeros(Globals.nr_simulations)
        for group in Globals.groups_info_dict_[MODEL].keys():
            if group != DataDictionaryConstants.GROUP_5 and group != DataDictionaryConstants.GROUP_6 and \
                    group != DataDictionaryConstants.GROUP_7:
                group_overall = [0 if np.isnan(x) else x for x in Groups.groups_dict_[MODEL][group]
                                [DataDictionaryConstants.OVERALL]]
                group_weight = Globals.groups_info_dict_[MODEL][group][DataDictionaryConstants.DATA_KEY_WEIGHT][0]
                loadcase += np.array(group_overall) * group_weight
                if len(Groups.groups_dict_[MODEL][group].keys()) == 1:
                    count_nan += 1
            if group == DataDictionaryConstants.GROUP_5:
                group_overall = [0 if np.isnan(x) else x for x in Groups.groups_dict_[MODEL]
                                [DataDictionaryConstants.GROUP_5][DataDictionaryConstants.MODIFIED_RATING]]
                group_weight = Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5] \
                                [DataDictionaryConstants.DATA_KEY_WEIGHT][0]
                count_nan += [1 if np.isnan(x) else 0 for x in Groups.groups_dict_[MODEL]
                                [DataDictionaryConstants.GROUP_5][DataDictionaryConstants.MODIFIED_RATING]]
                loadcase += np.array(group_overall) * group_weight

        loadcase = [np.nan if (count_nan == 6)[i] == True else x for i, x in enumerate(loadcase)]

        if DataDictionaryConstants.DICT_KEY_LOADCASE not in Groups.groups_dict_[MODEL].keys():
            Groups.groups_dict_[MODEL][DataDictionaryConstants.DICT_KEY_LOADCASE] = {}
        Groups.groups_dict_[MODEL][DataDictionaryConstants.DICT_KEY_LOADCASE][DataDictionaryConstants.OVERALL] =\
            loadcase


    @staticmethod
    def modify_loadcase(MODEL):
        index_to_zero = []
        problematic_groups = {}

        for group in Globals.groups_info_dict_[MODEL]:
            if group != DataDictionaryConstants.GROUP_5 and group != DataDictionaryConstants.GROUP_6 and \
                    group != DataDictionaryConstants.GROUP_7:
                lc_modifier = Globals.groups_info_dict_[MODEL][group] \
                                [DataDictionaryConstants.DICT_KEY_LOADCASE_MODIFIER]
                values = Groups.groups_dict_[MODEL][group][DataDictionaryConstants.OVERALL]
                if len(Groups.groups_dict_[MODEL][group].keys()) == 1:
                    values = [np.nan if x == 0 else x for x in values]
                for i, x in enumerate(values):
                    if x == 0 and lc_modifier[0] == DataDictionaryConstants.ON:
                        if i not in problematic_groups.keys():
                            problematic_groups[i] = []
                        problematic_groups[i].append(group)
                        if i not in index_to_zero:
                            index_to_zero.append(i)
            elif group == DataDictionaryConstants.GROUP_5:
                lc_modifier = Globals.groups_info_dict_[MODEL][group][
                    DataDictionaryConstants.DICT_KEY_LOADCASE_MODIFIER]
                for i, x in enumerate(Groups.groups_dict_[MODEL][group][DataDictionaryConstants.MODIFIED_RATING]):
                    if x == 0 and lc_modifier[0] == DataDictionaryConstants.ON:
                        if i not in problematic_groups.keys():
                            problematic_groups[i] = []
                        problematic_groups[i].append(group)
                        if i not in index_to_zero:
                            index_to_zero.append(i)
            elif group == DataDictionaryConstants.GROUP_7:
                lc_modifier = Globals.groups_info_dict_[MODEL][group][
                    DataDictionaryConstants.DICT_KEY_LOADCASE_MODIFIER]
                values = Groups.groups_dict_[MODEL][group][Globals.visual_assessment]
                for i, x in enumerate(values):
                    if x == DataDictionaryConstants.YES and lc_modifier[0] == DataDictionaryConstants.ON:
                        if i not in problematic_groups.keys():
                            problematic_groups[i] = []
                        problematic_groups[i].append(group)
                        if i not in index_to_zero:
                            index_to_zero.append(i)

        bad_group_at_index = ""
        Groups.groups_dict_[MODEL][DataDictionaryConstants.DICT_KEY_LOADCASE][
            DataDictionaryConstants.DICT_KEY_LCM_GROUPS] = [None] * Globals.getNumOfSimulation()
        for i in range(Globals.nr_simulations):
            if i in index_to_zero:
                for x in problematic_groups[i]:
                    bad_group_at_index += Groups.get_group_name(x) + "/"
                bad_group_at_index = bad_group_at_index[:-1]
                Groups.groups_dict_[MODEL][DataDictionaryConstants.DICT_KEY_LOADCASE][
                    DataDictionaryConstants.DICT_KEY_LCM_GROUPS][i] = bad_group_at_index
                bad_group_at_index = ""

    @staticmethod
    def truncate_values(groups_dict):
        for model in Globals.model_list_:
            for group in Globals.group_list_:
                for body_part in groups_dict[model][group].keys():
                    if group != DataDictionaryConstants.GROUP_6 and group != DataDictionaryConstants.GROUP_7:
                        if body_part != DataDictionaryConstants.DICT_KEY_PJOINT and \
                                body_part != DataDictionaryConstants.PJOINT_OVER_BASELINERISK and \
                                body_part != DataDictionaryConstants.MODIFIED_RATING and \
                                body_part != DataDictionaryConstants.STAR_RATING:
                            if body_part == DataDictionaryConstants.OVERALL:
                                    groups_dict[model][group][body_part] = [Groups.truncate(x, 2)
                                                                            for x in groups_dict[model][group]
                                                                            [body_part]]
                            for criteria in groups_dict[model][group][body_part]:
                                if criteria == DataDictionaryConstants.OVERALL:
                                    groups_dict[model][group][body_part][criteria] = [Groups.truncate(x, 2) for x in
                                                                            groups_dict[model][group][body_part]
                                                                            [criteria]]
                        else:
                            if body_part != DataDictionaryConstants.STAR_RATING and body_part not in \
                                    Globals.body_regions_:
                                groups_dict[model][group][body_part] = [Groups.truncate(x, 2) for x in
                                                                        groups_dict[model][group][body_part]]
                    elif group == DataDictionaryConstants.GROUP_6:
                        for criteria in groups_dict[model][group][body_part]:
                            if criteria == DataDictionaryConstants.OVERALL:
                                groups_dict[model][group][body_part][criteria] = [Groups.truncate(x, 2) for x in
                                                                        groups_dict[model][group][body_part][criteria]]
                    elif group == DataDictionaryConstants.GROUP_7:
                        if body_part != DataDictionaryConstants.OVERALL and body_part != \
                                DataDictionaryConstants.DICT_KEY_WARNING and body_part != Globals.visual_assessment:
                            for criteria in groups_dict[model][group][body_part]:
                                if criteria == DataDictionaryConstants.OVERALL:
                                    groups_dict[model][group][body_part][criteria] = [Groups.truncate(x, 2) for x in
                                                                                      groups_dict[model][group][
                                                                                          body_part][criteria]]

        return groups_dict

    @staticmethod
    def truncate(number, digits) -> float:
        if number is None:
            number = np.nan
        if not np.isnan(number):
            stepper = 10.0 ** digits
            return math.trunc(stepper * number) / stepper

    @staticmethod
    def filter_for_model_info(MODEL):
        boolean_model_list = np.zeros(Globals.nr_simulations)

        for model_info_key in Globals.working_dict_.keys():
            if model_info_key in Globals.model_info_[MODEL].keys():
                list_to_compare = Globals.working_dict_[model_info_key]
                list_to_compare = ["None" if x is None else x for x in list_to_compare]
                new_list = np.zeros(len(list_to_compare))
                # which simulations do not fullfill the model_info in the groupfile
                for i in range(len(list_to_compare)):
                    if list_to_compare[i] in Globals.model_info_[MODEL][model_info_key]:
                        new_list[i] = 0
                    else:
                        new_list[i] = 1

                boolean_model_list += np.array(new_list)
        boolean_model_list = [1 if x == 0 else 0 for x in boolean_model_list]

        return boolean_model_list

    @staticmethod
    def change_rr_to_stars(MODEL):
        if DataDictionaryConstants.RATING_STEPS not in \
                Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5].keys() or\
                len(Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5]
                    [DataDictionaryConstants.RATING_STEPS]) != 4:
            brisk = Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5] \
                    [DataDictionaryConstants.BASELINE_RISK][0]
            step = 1 / (5 * brisk)
            limits = [1 / brisk - 4 * step, 1 / brisk - 3 * step, 1 / brisk - 2 * step, 1 / brisk - step]
            print("Warning: No rating steps given for model " + MODEL + ". Steps will be " + str(limits) +"  .")
        else:
            limits = [Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5]
                      [DataDictionaryConstants.RATING_STEPS][0],
                      Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5]
                      [DataDictionaryConstants.RATING_STEPS][1],
                      Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5]
                      [DataDictionaryConstants.RATING_STEPS][2],
                      Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5]
                      [DataDictionaryConstants.RATING_STEPS][3]]
            if not (0 < limits[0] < limits[1] < limits[2] < limits[3] <
                    np.divide(1,Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5]
                    [DataDictionaryConstants.BASELINE_RISK][0])):
                brisk = Globals.groups_info_dict_[MODEL][DataDictionaryConstants.GROUP_5] \
                        [DataDictionaryConstants.BASELINE_RISK][0]
                step = 1/(5*brisk)
                limits = [1/brisk-4*step, 1/brisk-3*step, 1/brisk-2*step, 1/brisk-step]
                print("Warning: Rating steps not in correct order for model " + MODEL +
                      ". Steps will be " + str(limits) +"  .")

        Groups.groups_dict_[MODEL][DataDictionaryConstants.GROUP_5][
            DataDictionaryConstants.PJOINT_OVER_BASELINERISK] = list(
            Groups.groups_dict_[MODEL][DataDictionaryConstants.GROUP_5][
                DataDictionaryConstants.PJOINT_OVER_BASELINERISK])
        Groups.groups_dict_[MODEL][DataDictionaryConstants.GROUP_5][
            DataDictionaryConstants.STAR_RATING] = copy.deepcopy(Groups.groups_dict_[MODEL]
                                                                         [DataDictionaryConstants.GROUP_5][
            DataDictionaryConstants.PJOINT_OVER_BASELINERISK])
        for value_list in Groups.groups_dict_[MODEL][DataDictionaryConstants.GROUP_5].keys():
            if value_list == DataDictionaryConstants.STAR_RATING:

                for i, value in enumerate(Groups.groups_dict_[MODEL][DataDictionaryConstants.GROUP_5][value_list]):
                    if value <= limits[0]:
                        Groups.groups_dict_[MODEL][DataDictionaryConstants.GROUP_5] \
                         [DataDictionaryConstants.STAR_RATING][i] = "*****"
                    if limits[0] < value <= limits[1]:
                        Groups.groups_dict_[MODEL][DataDictionaryConstants.GROUP_5][
                            DataDictionaryConstants.STAR_RATING][i] = "****"
                    if limits[1] < value <= limits[2]:
                        Groups.groups_dict_[MODEL][DataDictionaryConstants.GROUP_5][
                            DataDictionaryConstants.STAR_RATING][i] = "***"
                    if limits[2] < value <= limits[3]:
                        Groups.groups_dict_[MODEL][DataDictionaryConstants.GROUP_5] \
                        [DataDictionaryConstants.STAR_RATING][i] = "**"
                    if value > limits[3]:
                        Groups.groups_dict_[MODEL][DataDictionaryConstants.GROUP_5][
                            DataDictionaryConstants.STAR_RATING][i] = "*"


    @staticmethod
    def get_main_dataframe():
        dataframe_dict = copy.deepcopy(Groups.dataframe_dict_["Merged"])
        dataframe_list =[]
        for group_nr, group in enumerate(Globals.group_list_):
            df = dataframe_dict[group_nr]
            df.set_index(DataDictionaryConstants.SIMULATIONS, inplace=True)
            df = df.transpose()
            df.reset_index()
            if DataDictionaryConstants.OVERALL in df.columns:
                df = df[[DataDictionaryConstants.OVERALL]]
                df.columns = [group]
            elif DataDictionaryConstants.MODIFIED_RATING in df.columns:
                df = df[[DataDictionaryConstants.MODIFIED_RATING]]
                df.columns = [group]
            else:
                pass
            df.index.name = DataDictionaryConstants.SIMULATIONS
            df = df.reset_index()

            dataframe_list.append(df)

        df1 = reduce(lambda x, y: pd.merge(x, y, on=DataDictionaryConstants.SIMULATIONS), dataframe_list)
        df1.rename(columns=dict(zip(df1.columns, [DataDictionaryConstants.SIMULATIONS]+[Groups.get_group_name(x)
                                                                                        for x in Globals.group_list_])),
                   inplace=True)
        if DataDictionaryConstants.LOADCASE_LCA in df1.columns and DataDictionaryConstants.LOADCASE_RATING \
                not in df1.columns:
            df1[DataDictionaryConstants.LOADCASE_RATING] = df1[DataDictionaryConstants.LOADCASE_LCA].apply(lambda x: '-'
            if x == '-' or x == None else 'Poor' if x <= 0 else 'Good' if x >=1 else 'Marginal'
            if x > 0 and x < 1 else x)
        temp_df = dataframe_dict[8].T

        ####change column order#####
        if DataDictionaryConstants.LOADCASE_LCA in df1.columns and DataDictionaryConstants.LOADCASE_RATING \
                in df1.columns:
            df1[DataDictionaryConstants.LOADCASE_RATING] = df1[DataDictionaryConstants.LOADCASE_RATING] \
                                                           + temp_df[DataDictionaryConstants.DICT_KEY_LCM_GROUPS]\
                                                               .apply(lambda x: '' if x == '-' else ' (LCM)' )\
                                                               .reset_index(drop=True)
            df1[DataDictionaryConstants.LOADCASE_RATING] = df1[DataDictionaryConstants.LOADCASE_RATING].\
                apply(lambda x: '-' if x == '- (LCM)' else x)
        cols = df1.columns.tolist()
        if DataDictionaryConstants.LOADCASE_LCA in cols:
            index = cols.index(DataDictionaryConstants.LOADCASE_LCA)
            cols = [cols[index]]+cols[:index] + cols[index+1:]
            df1 = df1[cols]
        if DataDictionaryConstants.LOADCASE_RATING in cols:
            index = cols.index(DataDictionaryConstants.LOADCASE_RATING)
            cols = [cols[index]]+cols[:index] + cols[index+1:]
            df1 = df1[cols]
        if DataDictionaryConstants.SIMULATIONS in cols:
            index = cols.index(DataDictionaryConstants.SIMULATIONS)
            cols = [cols[index]]+cols[:index] + cols[index+1:]
            df1 = df1[cols]
        return df1

    @staticmethod
    def get_detailed_dataframe(GROUP):
        if GROUP == DataDictionaryConstants.DICT_KEY_LOADCASE:
            df = copy.deepcopy(Groups.dataframe_dict_["Merged"][8])
        else:
            df = copy.deepcopy(Groups.dataframe_dict_["Merged"][int(GROUP[-1])-1])

        df.set_index(DataDictionaryConstants.SIMULATIONS, inplace=True)
        df = df.transpose()
        df.reset_index()
        df.index.name = Groups.get_group_name(GROUP)
        if GROUP != DataDictionaryConstants.DICT_KEY_LOADCASE and GROUP != DataDictionaryConstants.GROUP_5 and\
                GROUP != DataDictionaryConstants.GROUP_7 and GROUP != DataDictionaryConstants.GROUP_6:
            df = df.apply(pd.to_numeric, errors = 'coerce')
        df = df.reset_index()
        df = df.fillna('-')
        cols = df.columns.tolist()
        #change order of columns
        if DataDictionaryConstants.OVERALL in cols:
            index = cols.index(DataDictionaryConstants.OVERALL)
            cols = [cols[index]]+cols[:index] + cols[index+1:]
            df = df[cols]
        if DataDictionaryConstants.PJOINT_OVER_BASELINERISK in cols:
            index = cols.index(DataDictionaryConstants.PJOINT_OVER_BASELINERISK)
            cols = cols[:index] + cols[index+1:] + [cols[index]]
            df = df[cols]
        if DataDictionaryConstants.STAR_RATING in cols:
            index = cols.index(DataDictionaryConstants.STAR_RATING)
            cols = cols[:index] + cols[index+1:] + [cols[index]]
            df = df[cols]
        if DataDictionaryConstants.MODIFIED_RATING in cols:
            index = cols.index(DataDictionaryConstants.MODIFIED_RATING)
            cols = cols[:index] + cols[index+1:] + [cols[index]]
            df = df[cols]

        return df


    @staticmethod
    def get_group_name(GROUP):
        if GROUP == DataDictionaryConstants.LOADCASE_RATING:
            return GROUP
        if GROUP == DataDictionaryConstants.DICT_KEY_LOADCASE:
            return DataDictionaryConstants.LOADCASE_LCA
        if GROUP not in Globals.group_list_:
            return GROUP
        if DataDictionaryConstants.NAME not in Globals.groups_info_dict_[Globals.model_list_[0]][GROUP].keys():
            return GROUP
        switcher ={
            DataDictionaryConstants.GROUP_1: Globals.groups_info_dict_[Globals.model_list_[0]][GROUP]
            [DataDictionaryConstants.NAME][0],
            DataDictionaryConstants.GROUP_2: Globals.groups_info_dict_[Globals.model_list_[0]][GROUP][
                DataDictionaryConstants.NAME][0],
            DataDictionaryConstants.GROUP_3: Globals.groups_info_dict_[Globals.model_list_[0]][GROUP][
                DataDictionaryConstants.NAME][0],
            DataDictionaryConstants.GROUP_4: Globals.groups_info_dict_[Globals.model_list_[0]][GROUP][
                DataDictionaryConstants.NAME][0],
            DataDictionaryConstants.GROUP_5: Globals.groups_info_dict_[Globals.model_list_[0]][GROUP][
                DataDictionaryConstants.NAME][0],
            DataDictionaryConstants.GROUP_6: Globals.groups_info_dict_[Globals.model_list_[0]][GROUP][
                DataDictionaryConstants.NAME][0],
            DataDictionaryConstants.GROUP_7: Globals.groups_info_dict_[Globals.model_list_[0]][GROUP][
                DataDictionaryConstants.NAME][0],
            DataDictionaryConstants.GROUP_8: Globals.groups_info_dict_[Globals.model_list_[0]][GROUP][
                DataDictionaryConstants.NAME][0]
        }
        return switcher.get(GROUP, GROUP)

    @staticmethod
    def print_rated_groups_to_csv(remaining_simulations, MODEL):
        poor_criteria = [set() for _ in range(Globals.getNumOfSimulation())]
        marginal_criteria = [set() for _ in range(Globals.getNumOfSimulation())]
        good_criteria = [set() for _ in range(Globals.getNumOfSimulation())]

        sim_list = Globals.working_dict_[DataDictionaryConstants.DICT_KEY_SIM_NAME]

        for i in range(0, Globals.getNumOfSimulation()):
            for group in Globals.group_list_[:-1]:
                for criteria in Globals.groups_info_dict_[MODEL][group][DataDictionaryConstants.DICT_KEY_CRITERIA]:
                    for detailed_criteria in Globals.groups_info_dict_[MODEL][group] \
                        [DataDictionaryConstants.DICT_KEY_CRITERIA][criteria]:
                        if group != DataDictionaryConstants.GROUP_5:
                            #if Groups.groups_dict_[MODEL][group][criteria][detailed_criteria][i] <= 0:
                            if Groups.groups_dict_[MODEL][group][criteria][DataDictionaryConstants.OVERALL][i] <= 0:
                                poor_criteria[i].add(criteria+
                                                     ":"+Globals.working_dict_[DataDictionaryConstants.PARENT_PART]
                                                     [criteria][DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                     [detailed_criteria][DataDictionaryConstants.DATA_KEY_TYPE]+
                                                     ":"+detailed_criteria+":"+
                                                     str(Globals.working_dict_[DataDictionaryConstants.PARENT_PART]
                                                         [criteria][DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                         [detailed_criteria][DataDictionaryConstants.DATA_KEY_LIMITS])+
                                                     ":"+str(Groups.truncate(Globals.working_dict_
                                                                             [DataDictionaryConstants.PARENT_PART]
                                                                             [criteria]
                                                                             [DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                                             [detailed_criteria]
                                                                             [DataDictionaryConstants.DATA_KEY_VALUE]
                                                                             [i], 3)))
                            #if Groups.groups_dict_[MODEL][group][criteria][detailed_criteria][i] >= 1:
                            if Groups.groups_dict_[MODEL][group][criteria][DataDictionaryConstants.OVERALL][i] >= 1:
                                good_criteria[i].add(criteria+
                                                     ":"+Globals.working_dict_[DataDictionaryConstants.PARENT_PART]
                                                     [criteria][DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                     [detailed_criteria][DataDictionaryConstants.DATA_KEY_TYPE]+
                                                     ":"+detailed_criteria+":"+
                                                     str(Globals.working_dict_[DataDictionaryConstants.PARENT_PART]
                                                         [criteria][DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                         [detailed_criteria][DataDictionaryConstants.DATA_KEY_LIMITS])+
                                                     ":"+str(Groups.truncate(Globals.working_dict_
                                                                             [DataDictionaryConstants.PARENT_PART]
                                                                             [criteria]
                                                                             [DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                                             [detailed_criteria]
                                                                             [DataDictionaryConstants.DATA_KEY_VALUE]
                                                                             [i],3)))
                            #if Groups.groups_dict_[MODEL][group][criteria][detailed_criteria][i] >= 1:
                            if 0 < Groups.groups_dict_[MODEL][group][criteria][DataDictionaryConstants.OVERALL][i] < 1:
                                marginal_criteria[i].add(criteria+
                                                         ":"+Globals.working_dict_[DataDictionaryConstants.PARENT_PART]
                                                         [criteria][DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                         [detailed_criteria][DataDictionaryConstants.DATA_KEY_TYPE]+
                                                         ":"+detailed_criteria+":"+
                                                         str(Globals.working_dict_[DataDictionaryConstants.PARENT_PART]
                                                             [criteria][DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                             [detailed_criteria]
                                                             [DataDictionaryConstants.DATA_KEY_LIMITS])+
                                                         ":"+str(Groups.truncate(
                                    Globals.working_dict_[DataDictionaryConstants.PARENT_PART][criteria]
                                    [DataDictionaryConstants.DICT_KEY_CRITERIA][detailed_criteria]
                                    [DataDictionaryConstants.DATA_KEY_VALUE][i],3)))
                        #TODO: Rewrite group 5 or get_groups to not have multiple code that does the same
                        # (quotient calculation)
                        else:
                            if len(Globals.groups_info_dict_[MODEL][group][
                                        DataDictionaryConstants.DATA_KEY_LIMITS]) != 2:
                                print("Error, please enter valid limit bounds.")
                            else:
                                criteria_limit_str_best = \
                                Globals.groups_info_dict_[MODEL][group][DataDictionaryConstants.DATA_KEY_LIMITS][0]
                                criteria_limit_str_worst = \
                                Globals.groups_info_dict_[MODEL][group][DataDictionaryConstants.DATA_KEY_LIMITS][1]
                                limits = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][criteria][
                                    DataDictionaryConstants.DICT_KEY_CRITERIA][detailed_criteria][
                                    DataDictionaryConstants.DATA_KEY_LIMITS]
                                if limits is not None:
                                    criteria_limits = np.array(limits, dtype=np.float)

                                    if criteria_limits.size == 0:
                                        continue

                                    if criteria_limit_str_worst == 'worst':
                                        criteria_limit_worst = criteria_limits[2]
                                    elif criteria_limit_str_worst == 'middle':
                                        criteria_limit_worst = criteria_limits[1]
                                    elif criteria_limit_str_worst == 'best':
                                        criteria_limit_worst = criteria_limits[0]

                                    if criteria_limit_str_best == 'worst':
                                        criteria_limit_best = criteria_limits[2]
                                    elif criteria_limit_str_best == 'middle':
                                        criteria_limit_best = criteria_limits[1]
                                    elif criteria_limit_str_best == 'best':
                                        criteria_limit_best = criteria_limits[0]

                                    values = np.array(
                                        Globals.working_dict_[DataDictionaryConstants.PARENT_PART][criteria][
                                            DataDictionaryConstants.DICT_KEY_CRITERIA][detailed_criteria][
                                            DataDictionaryConstants.DATA_KEY_VALUE], dtype=np.float)
                                    numerator = values - criteria_limit_worst
                                    denominator = criteria_limit_best - criteria_limit_worst
                                    quotient = numerator / denominator
                                    if quotient[i] <= 0:
                                        poor_criteria[i].add(criteria+
                                                             ":"+Globals.working_dict_[DataDictionaryConstants.PARENT_PART]
                                                             [criteria][DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                             [detailed_criteria][DataDictionaryConstants.DATA_KEY_TYPE] +
                                                             ":"+detailed_criteria+":"+
                                                             str(Globals.working_dict_[DataDictionaryConstants.PARENT_PART]
                                                                 [criteria][DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                                 [detailed_criteria]
                                                                 [DataDictionaryConstants.DATA_KEY_LIMITS])+
                                                             ":"+str(Groups.truncate(
                                            Globals.working_dict_[DataDictionaryConstants.PARENT_PART][criteria]
                                            [DataDictionaryConstants.DICT_KEY_CRITERIA][detailed_criteria]
                                            [DataDictionaryConstants.DATA_KEY_VALUE][i],3)))
                                    if quotient[i] >= 1:
                                        good_criteria[i].add(criteria+
                                                             ":"+Globals.working_dict_[DataDictionaryConstants.PARENT_PART]
                                                             [criteria][DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                             [detailed_criteria][DataDictionaryConstants.DATA_KEY_TYPE] +
                                                             ":"+detailed_criteria+":"+str(
                                            Globals.working_dict_[DataDictionaryConstants.PARENT_PART][criteria]
                                            [DataDictionaryConstants.DICT_KEY_CRITERIA][detailed_criteria]
                                            [DataDictionaryConstants.DATA_KEY_LIMITS])+
                                                             ":"+str(
                                            Groups.truncate(Globals.working_dict_[DataDictionaryConstants.PARENT_PART]
                                                            [criteria][DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                            [detailed_criteria][DataDictionaryConstants.DATA_KEY_VALUE]
                                                            [i],3)))
                                    if 0 < quotient[i] < 1:
                                        marginal_criteria[i].add(criteria+
                                                                 ":"+Globals.working_dict_
                                                                 [DataDictionaryConstants.PARENT_PART]
                                                                 [criteria][DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                                 [detailed_criteria][DataDictionaryConstants.DATA_KEY_TYPE]+
                                                                 ":"+detailed_criteria+":"+str(
                                            Globals.working_dict_[DataDictionaryConstants.PARENT_PART][criteria]
                                            [DataDictionaryConstants.DICT_KEY_CRITERIA][detailed_criteria]
                                            [DataDictionaryConstants.DATA_KEY_LIMITS])+
                                                                 ":"+str(
                                            Groups.truncate(Globals.working_dict_[DataDictionaryConstants.PARENT_PART]
                                                            [criteria][DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                            [detailed_criteria][DataDictionaryConstants.DATA_KEY_VALUE]
                                                            [i],3)))



        data_poor = {sim:list(poor_criteria[i]) for i, sim in enumerate(sim_list)}
        data_marginal = {sim:list(marginal_criteria[i]) for i, sim in enumerate(sim_list)}
        data_good = {sim:list(good_criteria[i]) for i, sim in enumerate(sim_list)}

        df_poor = pd.DataFrame.from_dict(data_poor, orient='index').T
        df_marginal = pd.DataFrame.from_dict(data_marginal, orient='index').T
        df_good = pd.DataFrame.from_dict(data_good, orient='index').T

        filter_list = Groups.filter_for_model_info(MODEL)
        allowed_sim_for_model = [x for i,x in enumerate(sim_list) if filter_list[i] == 1]
        for column in list(df_poor):
            if column not in remaining_simulations or column not in allowed_sim_for_model:
                df_poor.drop(column, axis=1, inplace=True)
        for column in list(df_marginal):
            if column not in remaining_simulations or column not in allowed_sim_for_model:
                df_marginal.drop(column, axis=1, inplace=True)
        for column in list(df_good):
            if column not in remaining_simulations or column not in allowed_sim_for_model:
                df_good.drop(column, axis=1, inplace=True)


        if not os.path.exists(os.path.join(Globals.groups_file_location, MODEL)):
            os.makedirs(os.path.join(Globals.groups_file_location, MODEL))
        df_poor.to_csv(os.path.join(Globals.groups_file_location, MODEL, "CriteriaPoor" + '.csv'), sep=';',
                       index = False)
        df_marginal.to_csv(os.path.join(Globals.groups_file_location, MODEL, "CriteriaMarginal" + '.csv'), sep=';',
                           index = False)
        df_good.to_csv(os.path.join(Globals.groups_file_location, MODEL, "CriteriaGood" + '.csv'), sep=';',
                       index = False)
