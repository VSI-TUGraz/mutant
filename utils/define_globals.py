import os
import os.path
import base64
import json
import sys
import argparse
import numpy as np

from utils.readfiles import Dictionary
from utils.constants import DataDictionaryConstants, ParsingConstants


class Globals:
    report_to_csv_button = 0
    working_dict_ = {}
    groups_info_dict_= {}
    criteria_to_show = []
    groups_file_location = None
    model_list_ = []
    group_list_ = []
    body_regions_ = []
    list_filter_criteria_range_slider_ids_ = []
    list_of_global_dropdown_ids_ = []
    nr_simulations = 0
    rootdir_ = None
    limits_file_ = None
    __tu_logo_dir = None
    __vsi_logo_dir = None
    tu_image_ = None
    vsi_image_ = None
    options_for_filter_criteria_dropdown_ = []
    options_for_filter_criteria_dropdown_additional_label_body_region_ = []
    option_for_simulation_dropdown_x_y_ = []
    clicked_data_ = {}
    clicked_data_string = ""
    criteria_data_2D_scatter_reset_highlights_clicks_counter = 0
    diagram_data_1_reset_highlights_clicks_counter = 0
    diagram_data_2_reset_highlights_clicks_counter = 0
    selected_simulation_info_reset_highlights_clicks_counter = 0
    info_reset_highlights_counter = 0
    reset_highlights_flag = False
    reset_highlights_flag_1 = False
    reset_highlights_flag_2 = False
    reset_highlights_flag_3 = False
    criteria_data_2D_scatter_clicked_data = ""
    diagram_data_graph_1_clicked_data = ""
    diagram_data_graph_2_clicked_data = ""
    criteria_data_2D_scatter_reset_clicked_data = ""
    diagram_data_graph_1_reset_clicked_data = ""
    diagram_data_graph_2_reset_clicked_data = ""
    criteria_data_2D_scatter_reset_flag = False
    diagram_data_graph_1_reset_flag = False
    diagram_data_graph_2_reset_flag = False
    COLORS = []
    table_style = []
    model_info_ = {}
    visual_assessment = ""
    args = None



    @staticmethod
    def init_globals():
        """ Parses command line arguments, calls read function (to check and read the destination directory) and saves
        in Globals variables information from input files.
        """
        parser = Globals.parse_args(sys.argv[1:])

        Globals.args = parser
        config_dictionary = Globals.create_config_dict_from_args(parser)

        if parser.groups is not None or 'groups_dir' in config_dictionary.keys():
            Globals.groups_file_location = config_dictionary['groups_dir']
            Globals.initialize_global_group_information()
        if parser.limits is not None:
            Globals.limits_file_ = os.path.abspath(config_dictionary["criteria_limits_file"])

        Globals.rootdir_ = os.path.abspath(config_dictionary["destination_dir"])
        __tu_logo_dir = os.path.join(os.getcwd(), 'img/tulogo.png')
        __vsi_logo_dir = os.path.join(os.getcwd(), 'img/logo_vsi.gif')
        __mutant_logo_dir = os.path.join(os.getcwd(), 'img/Logo_MUTANT.png')

        Globals.tu_image_ = base64.b64encode(open(__tu_logo_dir, 'rb').read())
        Globals.vsi_image_ = base64.b64encode(open(__vsi_logo_dir, 'rb').read())
        Globals.mutant_logo_ = base64.b64encode(open(__mutant_logo_dir, 'rb').read())

        Globals.working_dict_, Globals.nr_simulations = Dictionary.readFromFile(rootdir=Globals.rootdir_,
                                                                                limits_file=Globals.limits_file_)

        if Globals.working_dict_ is None:
            print("Execution aborted!")
            exit()

        if Globals.visual_assessment not in Globals.working_dict_.keys():
            print("Warning: Visual " + Globals.visual_assessment + " inspection missing")

        Globals.body_regions_ = [body_region for body_region in
                                 Globals.working_dict_[DataDictionaryConstants.PARENT_PART].keys()]

        Globals.options_for_filter_criteria_dropdown_ = [
            {'label': criteria, 'value': body_region + ":" + criteria} for body_region in
            Globals.working_dict_[DataDictionaryConstants.PARENT_PART].keys() for criteria in
            Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region]
                                 [DataDictionaryConstants.DICT_KEY_CRITERIA]]

        Globals.options_for_filter_criteria_dropdown_additional_label_body_region_ = [
            {'label': body_region + ":" + criteria, 'value': body_region + ":" + criteria} for
            body_region in Globals.working_dict_[DataDictionaryConstants.PARENT_PART].keys() for criteria in
            Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region]
                                 [DataDictionaryConstants.DICT_KEY_CRITERIA]]

        Globals.option_for_simulation_dropdown_x_y_ = [{'label': name, 'value': name} for body_region in
                                              Globals.working_dict_[DataDictionaryConstants.PARENT_PART].keys()
                                                       for name in
                                                       Globals.working_dict_[DataDictionaryConstants.PARENT_PART]
                                                       [body_region][DataDictionaryConstants.DICT_KEY_ONE_COLUMN]]

        Globals.criteria_to_show = [DataDictionaryConstants.OVERALL, Globals.visual_assessment,
                                    DataDictionaryConstants.DICT_KEY_LCM_GROUPS,
                                    DataDictionaryConstants.MODIFIED_RATING,
                                    DataDictionaryConstants.DICT_KEY_PJOINT, "Pjoint/Baseline_risk"] \
                                   + Globals.group_list_

    @staticmethod
    def parse_args(args):
        """ Converts command line arguments from a list to argparse namespace.
        Args:
            args (list): List of command line arguments
        Returns:
            parser.parse_args(args) (argparse.Namespace): Namespace with information parsed from command line arguments.
        """
        parser = argparse.ArgumentParser()
        parser.add_argument("--dest", help="destination directory", dest="dest", required=True)
        parser.add_argument("--limits", help="criteria limits file", dest="limits")
        parser.add_argument("--groups", help="groups file location", dest="groups")
        parser.add_argument("-p", "--server-port", help="server port", dest="server_port", default="8050")



        return parser.parse_args(args)

    @staticmethod
    def initialize_global_group_information():
        """ Opens the group input file and initializes global group information which are read from the group input
        file.
        """
        temp_dict = {}

        with open(os.path.join(Globals.groups_file_location, 'Groups'), 'r') as groups_file:
            json_object = groups_file.readlines()
            json_object_string = "".join(json_object)
            try:
                temp_dict = json.loads(json_object_string)
            except ValueError as e:
                print('Decoding JSON has failed! Check Groups file ')
                exit(e)

        for model in temp_dict.keys():
            Globals.model_info_[model] = temp_dict[model][DataDictionaryConstants.DICT_KEY_MODEL_INFO]
            Globals.model_list_.append(model)
            del temp_dict[model][DataDictionaryConstants.DICT_KEY_MODEL_INFO]

        if len(Globals.model_list_) != 0:
            Globals.group_list_ = [group for group in temp_dict[Globals.model_list_[0]].keys()]
            Globals.group_list_.append(DataDictionaryConstants.DICT_KEY_LOADCASE)

        for model in Globals.model_list_:
            Globals.groups_info_dict_[model] = {}
            for group in Globals.group_list_[:-1]:
                Globals.groups_info_dict_[model][group] = {}
                for keys in temp_dict[model][group]:
                    if keys == DataDictionaryConstants.DICT_KEY_CRITERIA:
                        Globals.groups_info_dict_[model][group][keys] = {}
                    else:
                        Globals.groups_info_dict_[model][group][keys] = temp_dict[model][group][keys]
                for i in range(0, len(temp_dict[model][group][DataDictionaryConstants.DICT_KEY_CRITERIA])):
                    temp_dict[model][group][DataDictionaryConstants.DICT_KEY_CRITERIA][i] = \
                        temp_dict[model][group][DataDictionaryConstants.DICT_KEY_CRITERIA][i].split(':')
                    del temp_dict[model][group][DataDictionaryConstants.DICT_KEY_CRITERIA][i][1]
                for key, *value in temp_dict[model][group][DataDictionaryConstants.DICT_KEY_CRITERIA]:
                    Globals.groups_info_dict_[model][group][DataDictionaryConstants.DICT_KEY_CRITERIA].\
                        setdefault(key,[]).extend(value)

        # Warning if the Weights of the groups don't sum up to 1, sanity checks
        if Globals.groups_file_location != None:
            group_weight_sum = np.zeros(len(Globals.groups_info_dict_.keys()))
            if DataDictionaryConstants.VISUAL_ASSESSMENT in \
                    Globals.groups_info_dict_[list(Globals.groups_info_dict_.keys())[0]][
                        DataDictionaryConstants.GROUP_7].keys():
                Globals.visual_assessment = \
                    Globals.groups_info_dict_[list(Globals.groups_info_dict_.keys())[0]][
                        DataDictionaryConstants.GROUP_7][
                        DataDictionaryConstants.VISUAL_ASSESSMENT]
            else:
                print(
                    "Warning: No Visual Assessment specified in the Group file. "
                    "Vissual Assessment preset to Submarining.")
                Globals.visual_assessment = "Submarining"

            for counter, model in enumerate(Globals.groups_info_dict_.keys()):
                group_weight = 0
                for group in Globals.groups_info_dict_[model].keys():
                    if group != DataDictionaryConstants.GROUP_6 and group != DataDictionaryConstants.GROUP_7:
                        group_weight += \
                        Globals.groups_info_dict_[model][group][DataDictionaryConstants.DATA_KEY_WEIGHT][0]
                group_weight_sum[counter] = group_weight
                if not group_weight_sum[counter] == 1:
                    print("Warning: sum of weighting factors not equal to 100% in model " + model +
                          ". MUTANT assumes an equal distribution of group weighting factors instead.")
                    for group in Globals.groups_info_dict_[model].keys():
                        if group != DataDictionaryConstants.GROUP_6 and group != DataDictionaryConstants.GROUP_7:
                            Globals.groups_info_dict_[model][group][DataDictionaryConstants.DATA_KEY_WEIGHT][0] = \
                                1 / (len(Globals.groups_info_dict_[model].keys()) - 2)

        for model in Globals.model_list_:
            if DataDictionaryConstants.BASELINE_RISK not in Globals.groups_info_dict_[model][
                DataDictionaryConstants.GROUP_5].keys():
                Globals.groups_info_dict_[model][DataDictionaryConstants.GROUP_5][
                    DataDictionaryConstants.BASELINE_RISK] = [1]
                print("Warning: No baseline risk in group file for model " + model + ", baseline risk set to 1.")
            if len(Globals.groups_info_dict_[model][DataDictionaryConstants.GROUP_5][
                       DataDictionaryConstants.BASELINE_RISK]) != 1:
                print("Warning: Length of baseline risk wrong, please check the group file. Baseline risk set to 1.")
                Globals.groups_info_dict_[model][DataDictionaryConstants.GROUP_5][
                    DataDictionaryConstants.BASELINE_RISK] = [1]

    @staticmethod
    def create_config_dict_from_args(args):
        """ Creates config dictionary from Namespace of command line arguments.
        Args:
            args (argparse.Namespace): Namespace with information parsed from command line arguments.
        Returns:
            config_dictionary (dict): Dictionary with destination directory and criteria limits file.
        """
        if not os.path.isdir(args.dest):
            print("Define the destination dir of the simulation results! Execution aborted")
            exit(-1)
        config_dictionary = dict()
        config_dictionary[ParsingConstants.DEST_DIR] = args.dest
        config_dictionary[ParsingConstants.LIMITS_FILE] = args.limits

        if args.groups is not None:
            if os.path.isdir(args.groups):
                config_dictionary[ParsingConstants.GROUPS_DIR] = args.groups
            else:
                print("[Warning]: No group file for the assessment specified! Starting without groups.")
                args.groups = None
        return config_dictionary

    @staticmethod
    def getNumOfSimulation():
        """ Returns the number of simulations loaded from the destination directory and saved in Globals.nr_simulations.
        Returns:
            Globals.nr_simulations (int): Globals variable in which is stored the number of simulations.
        """
        return Globals.nr_simulations

    @staticmethod
    def getLegendDropdowns():
        """ Returns a list of options for 'Group by' dropdown.
        Returns:
            [{'label': key.replace('dropdown_', ""), 'value': key} for key in Globals.list_of_global_dropdown_ids_]
             (list): List of options for 'Group by' dropdown
        """
        return [{'label': key.replace('dropdown_', ""), 'value': key} for key in Globals.list_of_global_dropdown_ids_]

    # Do NOT change order of styles
    @staticmethod
    def get_table_styles(df):
        """ Creates style for the table depending on the data from the group input file.
        Args:
            df (pandas.core.frame.DataFrame): Dataframe with information from the group input file to be used
            for the table.
        Returns:
            styles (list): List with styles information.
        """
        styles_0 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} >= 0'.format('{',x,'}')
                },
                'backgroundColor': '#FFD966',
                'color': 'black',
            } for x in df.columns]
        styles_1 = [{
            'if': {
                'column_id': x,
                'filter_query': '{}{}{} <= 0.0'.format('{',x,'}')
            },
            'backgroundColor': '#FF7C80',
            'color': 'black',
        } for x in df.columns]
        styles_2 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} >= 1.0'.format('{',x,'}')
                },
                'backgroundColor': '#A9D08E',
                'color': 'black',
            } for x in df.columns]
        styles_3 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} eq "-"'.format('{', x, '}')
                },
                'backgroundColor': '#FFFFFF',
                'color': 'black',
            } for x in df.columns]
        styles_4 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} contains "YES"'.format('{', x, '}')
                },
                'backgroundColor': '#FF7C80',
                'color': 'black',
            } for x in df.columns]
        styles_5 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} contains "NO"'.format('{', x, '}')
                },
                'backgroundColor': '#A9D08E',
                'color': 'black',
            } for x in df.columns]
        styles_6 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} eq "*"'.format('{', x, '}')
                },
                'backgroundColor': '#FF7C80',
                'color': 'black',
            } for x in df.columns]
        styles_7 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} eq "**"'.format('{', x, '}')
                },
                'backgroundColor': '#FFD966',
                'color': 'black',
            } for x in df.columns]
        styles_8 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} eq "***"'.format('{', x, '}')
                },
                'backgroundColor': '#FFD966',
                'color': 'black',
            } for x in df.columns]
        styles_9 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} eq "****"'.format('{', x, '}')
                },
                'backgroundColor': '#FFD966',
                'color': 'black',
            } for x in df.columns]
        styles_10 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} eq "*****"'.format('{', x, '}')
                },
                'backgroundColor': '#A9D08E',
                'color': 'black',
            } for x in df.columns]
        styles_11 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} contains "Poor"'.format('{', x, '}')
                },
                'backgroundColor': '#FF7C80',
                'color': 'black',
            } for x in df.columns]
        styles_12 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} contains "Marginal"'.format('{', x, '}')
                },
                'backgroundColor': '#FFD966',
                'color': 'black',
            } for x in df.columns]
        styles_13 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} contains "Good"'.format('{', x, '}')
                },
                'backgroundColor': '#A9D08E',
                'color': 'black',
            } for x in df.columns]


        styles = styles_0+styles_1+styles_2+styles_3+styles_4+styles_5+styles_6+styles_7+styles_8+styles_9+styles_10+\
                 styles_11+styles_12+styles_13
        return styles

    @staticmethod
    def get_table_styles_group_5(df):
        """ Creates style for the table 5 depending on the data from the group input file.
        Args:
            df (pandas.core.frame.DataFrame): Dataframe with information from the group input file to be used
            for the table 5.
        Returns:
            styles (list): List with styles information fot table 5.
        """
        styles_0 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} >= 0'.format('{',x,'}')
                },
                'backgroundColor': '#FFFFFF',
                'color': 'black',
            } for x in df.columns]
        styles_1 = [{
            'if': {
                'column_id': x,
                'filter_query': '{}{}{} <= 0.0'.format('{',x,'}')
            },
            'backgroundColor': '#FFFFFF',
            'color': 'black',
        } for x in df.columns]
        styles_2 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} >= 1.0'.format('{',x,'}')
                },
                'backgroundColor': '#FFFFFF',
                'color': 'black',
            } for x in df.columns]
        styles_3 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} eq "-"'.format('{', x, '}')
                },
                'backgroundColor': '#FFFFFF',
                'color': 'black',
            } for x in df.columns]
        styles_4 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} eq "*"'.format('{', x, '}')
                },
                'backgroundColor': '#FF7C80',
                'color': 'black',
            } for x in df.columns]
        styles_5 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} eq "**"'.format('{', x, '}')
                },
                'backgroundColor': '#FFD966',
                'color': 'black',
            } for x in df.columns]
        styles_6 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} eq "***"'.format('{', x, '}')
                },
                'backgroundColor': '#FFD966',
                'color': 'black',
            } for x in df.columns]
        styles_7 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} eq "****"'.format('{', x, '}')
                },
                'backgroundColor': '#FFD966',
                'color': 'black',
            } for x in df.columns]
        styles_8 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} eq "*****"'.format('{', x, '}')
                },
                'backgroundColor': '#A9D08E',
                'color': 'black',
            } for x in df.columns]
        styles_9 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} >=0'.format('{', x, '}')
                },
                'backgroundColor': '#FFD966',
                'color': 'black',
            } for x in ["Modified_Rating"]]
        styles_10= [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} >=1'.format('{', x, '}')
                },
                'backgroundColor': '#A9D08E',
                'color': 'black',
            } for x in ["Modified_Rating"]]
        styles_11 = [
            {
                'if': {
                    'column_id': x,
                    'filter_query': '{}{}{} <=0'.format('{', x, '}')
                },
                'backgroundColor': '#FF7C80',
                'color': 'black',
            } for x in ["Modified_Rating"]]

        styles = styles_0+styles_1+styles_2+styles_3+styles_4+styles_5+styles_6+styles_7+styles_8+styles_9+styles_10+\
                 styles_11
        return styles
