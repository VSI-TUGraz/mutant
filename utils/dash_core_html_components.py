from utils.group_computation import Groups
import numpy as np
from dash import dcc
from dash import html

from utils.utils import SliderUtils
from utils.constants import DataDictionaryConstants, DropdownConstants
from utils.define_globals import Globals


class HTMLBase:

    @staticmethod
    def get_layout_content():
        simulation_filter_meta_information_list = HTMLComponent.get_simulation_filter_meta_information_list\
            (Globals.working_dict_, Globals.list_of_global_dropdown_ids_)
        content_layout = html.Div(className="big_container", children=[
            html.Title('Mutant'),
            html.Div(className="app-header_head", children=[
                html.Div(className="mutant_logo", children=[
                    html.A(html.Img(className="mutant_logo_image",
                                    src='data:image/png;base64,{}'.format(Globals.mutant_logo_.decode())),
                           href='https://gitlab.com/VSI-TUGraz/mutant')]),
                html.Div(className="vsi_TU_logo", children=[
                    html.A(html.Img(className="vsi_logo_image",
                                    src='data:image/gif;base64,{}'.format(Globals.vsi_image_.decode())),
                           href='https://www.tugraz.at/institute/vsi/home/'),
                    html.A(html.Img(className="TU_logo_image",
                                    src='data:image/png;base64,{}'.format(Globals.tu_image_.decode())),
                           href='https://www.tugraz.at/home/')])]),

            html.Div(className="body_configuration", children=[
                html.Div(className='global_configuration',
                         ###############################################################################################
                         # Left Side
                         children=HTMLComponent.get_simulation_filter_meta_information_body_regions_dropdown
                                  (Globals.working_dict_) +
                                  HTMLComponent.get_filtered_simulation_info() +
                                  HTMLComponent.selected_simulation_info_ +
                                  simulation_filter_meta_information_list[2:] +
                                  simulation_filter_meta_information_list[:2] +
                                  HTMLComponent.buttons +

                                  HTMLComponent.get_simulation_without_limits()),
                html.Div(className="right_side_div", children=[
                    ####################################################################################################
                    # SimulationFilterCriteria
                    html.Div(className="simulation_filter_criteria", children=[
                        html.H6('Criteria'),
                        dcc.RadioItems(id='simulation_filter_criteria_dropdown_radiobuttons',
                                       options=HTMLComponent.get_all_criteria_types(),
                                       value=HTMLComponent.get_all_criteria_types()[0]['value'],
                                       labelStyle={'display': 'inline-block'}),
                        html.Div(className='to_change_size_of_value', id='to_change_size_of_value',
                                 children=[html.Div(className="simulation_filter_criteria_dropdown_multiple_selection",
                                                    children=[
                                                        dcc.Dropdown(
                                                            id='simulation_filter_criteria_dropdown_multiple_selection',
                                                            multi=True, value=[],
                                                            options=Globals.options_for_filter_criteria_dropdown_)]),
                                     html.Div(className="blank_space",
                                              id="simulation_filter_criteria_report_criteria_button"),
                                     html.Div(className="blank_space",
                                              id="simulation_filter_criteria_report_graphs_button")]),
                        html.Hr(className='horizontal_line'),
                        html.Div(className='simulation_filter_criteria_sliders_class',
                                 id='simulation_filter_criteria_sliders',
                                 children=HTMLComponent.get_simulation_filter_criteria_sliders_list())]),
                    ####################################################################################################
                    html.Hr(className='horizontal_line'),
                    ####################################################################################################
                    # DiagramDataGraph
                    html.Div(className="body_graph_div", children=[
                        html.Div(className="diagram_data_graph_class",
                                 children=HTMLComponent.get_diagram_data_graph(id_prefix="diagram_data_graph_1")),
                        html.Div(className="diagram_data_graph_class",
                                 children=HTMLComponent.get_diagram_data_graph(id_prefix="diagram_data_graph_2")),
                        ################################################################################################
                        # CriteriaDataBoxplot
                        html.Div(className='criteria_data_boxplot_class',
                                 children=HTMLComponent.get_criteria_data_boxplot()),
                        ################################################################################################
                        # CriteriaData2DScatter
                        html.Div(className="criteria_data_2D_scatter_class",
                                 children=HTMLComponent.get_criteria_data_2D_scatter()),
                    ]),
                    ####################################################################################################
                    # CriteriaDataGraph
                    html.Div(className="criteria_data_graph", children=[
                        html.P(),
                        html.Hr(className='horizontal_line'),
                        dcc.Graph(id='criteria_data_graph',
                                  config={'modeBarButtonsToRemove': ['sendDataToCloud', 'hoverCompareCartesian']}),
                        html.Br(),
                        html.Div(className = 'input_visualisation_maximum_class_1',
                                 children=[html.I("Set visualisation maximum of Y axis to:  "),
                                           dcc.Input(id="criteria_data_graph_input_visualisation_maximum",
                                                     type="number",
                                                     placeholder="100",
                                                     debounce=True)]),
                        html.Br(),
                        html.Div("Legend for the first parentheses () extension of the criterion name:"),
                        html.Div("(-) only negative limits and negative criterion values"),
                        html.Div("(+) only positive limits and positive criterion values"),
                        html.Div("() limits and criterion values are positive and negative"),
                        html.Br(),
                        html.Div("Legend for the second parentheses () extension of the criterion name:"),
                        html.Div("(b>) value of best limit greater then value of worst limit"),
                        html.Div(className = 'input_visualisation_maximum_class_2',
                                 children=["(b<) value of the best limit lower then value of worst limit"]),
                        html.Div(className = 'no_limits_defined_class', children=["() no limits defined"]),
                        html.Br(),
                        dcc.RadioItems(id='criteria_data_graph_radiobutton',
                                       options=HTMLComponent.get_all_criteria_types(),
                                       value=HTMLComponent.get_all_criteria_types()[0]['value'],
                                       labelStyle={'display': 'inline-block'}),
                        html.Div(className="criteria_data_graph_dropdown_class", children=[
                            dcc.Dropdown(id='criteria_data_graph_dropdown',
                                         multi=True,
                                         value=[],
                                         options=Globals.options_for_filter_criteria_dropdown_)
                        ]),
                        html.Br(),
                        html.Div(className="dropdown_legend_selection", children=[
                            dcc.Dropdown(id='criteria_data_graph_group_by',
                                         placeholder='Group by',
                                         options=Globals.getLegendDropdowns(),
                                         disabled=False)
                        ]),
                        html.Br(),
                        html.Div(className="dropdown_legend_selection", children=[
                            dcc.Dropdown(id='criteria_data_graph_dropdown_operation',
                                         placeholder='Normalization',
                                         options=HTMLComponent.get_criteria_graph_normalization_factor(),
                                         value=HTMLComponent.get_criteria_graph_normalization_factor()[0]['value'],
                                         disabled=False)
                        ]),
                        html.P(),
                        html.Hr(className='horizontal_line'),
                    ]),
                    ####################################################################################################
                    # GroupAssessment
                    html.H6('Group Assessment (GA):'),
                    html.Div(className="group_assessment_report_groups_button_class", children=[html.Button(
                        "Report Groups", id="group_assessment_report_groups_button", n_clicks=0)]),
                    html.Div(id='group_assessment_groups_button'),
                    html.Br(),
                    html.Div(id='groups_radio_selector'),
                    dcc.RadioItems(id='group_assessment_filter_groups_radiobutton',
                                   options=HTMLComponent.get_all_groups_types(),
                                   value=HTMLComponent.get_all_groups_types()[0]['value'],
                                   labelStyle={'display': 'inline-block'}),
                    html.Br(),
                    html.Div(className="group_assessment_groups_dropdown_div", children=[
                        dcc.Dropdown(id='group_assessment_groups_dropdown',
                                     multi=True,
                                     value=[group for group in Globals.group_list_] + [
                                         DataDictionaryConstants.LOADCASE_RATING],
                                     options=Groups.options_for_group_dropdown_)]),
                    html.Br(),
                    html.Div(id="group_assessment_tables_div"),
                    html.Br(),
                    html.H4('Detailed GA results:'),
                    html.Br(),
                    html.Div(className="group_assessment_groups_dropdown_div",
                             children=[dcc.Dropdown(id='group_assessment_in_depth_dropdown',
                                                    multi=True,
                                                    value=[],
                                                    options=Groups.options_for_in_depth_group_dropdown_)]),
                    html.Br(),
                    html.Div(id="group_assessment_in_depth_tables"),
                    html.Br()
                ]),
            ]),
            ############################################################################################################
            html.Div(id="helper_div_1"),
            html.Div(id="helper_div_2"),
            html.Div(id="helper_div_3"),
            html.Div(id="helper_div_4")
        ])
        return content_layout


class HTMLComponent:

    selected_simulation_info_ = [
        html.Br(),
        html.Div(className= 'selected_simulation', id='selected_simulation_info'),
        html.Br()
    ]

    buttons = [
        html.Hr(className='horizontal_line'),
        html.Div(className='button', children=[html.Button("Reset Highlights", id="reset_button", n_clicks=0)]),
        html.Div(className='button', children=[html.Button("Report Criteria", id="button_report_criteria")]),
        html.Div(className='button', children=[html.Button("Report Graphs", id="button_report_criteria_graphs")])
    ]

    @staticmethod
    def get_all_criteria_types():
        """ Creates a list of all types of criteria
        Returns:
            types_ret (list): List of criteria types
        """
        types_ret = [{'label': 'all', 'value': 'all'}, {'label': 'customize', 'value': ''}]
        types_ = []
        for key in Globals.working_dict_[DataDictionaryConstants.PARENT_PART].keys():
            crits = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][key] \
                [DataDictionaryConstants.DICT_KEY_CRITERIA]
            for crit in crits:
                type_ = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][key] \
                    [DataDictionaryConstants.DICT_KEY_CRITERIA][crit][DataDictionaryConstants.DATA_KEY_TYPE]
                if type_ not in types_:
                    types_.append(type_)

        types_ret = types_ret + [{'label': t, 'value': t} for t in types_]
        return types_ret

    @staticmethod
    def get_simulation_filter_criteria_sliders_list():
        """ Creates the initial list of sliders (depending on the criteria).
        Returns:
            slider_list (list): List of sliders
        """
        slider_list = []
        for body_region in Globals.working_dict_[DataDictionaryConstants.PARENT_PART].keys():
            for name_of_criteria in Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region] \
                    [DataDictionaryConstants.DICT_KEY_CRITERIA].keys():
                criteria_dict = Globals.working_dict_[DataDictionaryConstants.PARENT_PART][body_region] \
                    [DataDictionaryConstants.DICT_KEY_CRITERIA][name_of_criteria]
                float_boolean = [isinstance(v, float) for v in criteria_dict[DataDictionaryConstants.DATA_KEY_VALUE]]
                if any(float_boolean):
                    Globals.list_filter_criteria_range_slider_ids_.append(body_region + ':' + name_of_criteria)
                    slider_list.append(
                        HTMLComponent.get_simulation_filter_criteria_slider_div(Globals.working_dict_, body_region,
                                                                                name_of_criteria))
        return slider_list

    @staticmethod
    def get_criteria_graph_normalization_factor():
        """ Creates a list of operations (normalization factors) that can be selected in the dropdown  of the
         criteria graph.
        Returns:
            types_ret (list): List of normalization factors
        """
        types_ret = [{'label': DropdownConstants.BEST_WORST, 'value': DropdownConstants.BEST_WORST},
                     {'label': DropdownConstants.MAX_MEAN, 'value': DropdownConstants.MAX_MEAN},
                     {'label': DropdownConstants.MAX_MIN, 'value': DropdownConstants.MAX_MIN},
                     {'label': DropdownConstants.MEAN_MIN, 'value': DropdownConstants.MEAN_MIN}]
        return types_ret

    @staticmethod
    def get_all_groups_types():
        """ Creates a list of group types that can be selected in the Group Assessment section.
        Returns:
            list: List of group types
        """
        return [{'label': 'all', 'value': 'all'}, {'label': 'none', 'value': 'none'}]

    @staticmethod
    def get_simulation_filter_criteria_slider_div(working_dict, body_region, name_of_criteria, disabled=False):
        """ Creates a dash_html_components Div with data for a specific slider.
        Args:
            working_dict (dict): Dictionary with data from input files
            body_region (str): Specific body region
            name_of_criteria (str): Specific criteria name
        Returns:
            div (dash_html_components Div): Div for a specific slider
        """
        slider_config = SliderUtils.get_slider_config([np.nan if i is None or not np.isfinite(i) else i for i in
                                                       working_dict[DataDictionaryConstants.PARENT_PART][body_region]
                                                                   [DataDictionaryConstants.DICT_KEY_CRITERIA]
                                                                   [name_of_criteria]
                                                                   [DataDictionaryConstants.DATA_KEY_VALUE]])

        paragraph = body_region + ':' + name_of_criteria
        if len(paragraph) > 24:
            paragraph = ('\n'.join(paragraph[i:i + 24] for i in range(0, len(paragraph), 24)))
        if disabled == True:
            div = html.Div(className='simulation_filter_criteria_sliders_part_1', children=[
                html.P(className='simulation_filter_criteria_sliders_paragraph', children=[paragraph]),
                html.Div(className='simulation_filter_criteria_sliders', children=[
                    dcc.RangeSlider(id=body_region + ':' + name_of_criteria,
                                    disabled=disabled,
                                    step=slider_config['step'],
                                    allowCross=False,
                                    min=slider_config['min'],  # int(minimum) - 2,
                                    max=slider_config['max'],  # int(maximum) + 2,
                                    value=slider_config['value'],
                                    updatemode='mouseup',
                                    dots=True,
                                    marks=slider_config['marks']
                                    )]),
                html.Br([])])

        else:
            div = html.Div(className='simulation_filter_criteria_sliders_part_2',children=[
                html.P(className='simulation_filter_criteria_sliders', children=[paragraph]),
                html.Div(className='simulation_filter_criteria_sliders', children=[
                    dcc.RangeSlider(id=body_region + ':' + name_of_criteria,
                                    disabled=disabled,
                                    step=slider_config['step'],
                                    allowCross=False,
                                    min=slider_config['min'],  # int(minimum) - 2,
                                    max=slider_config['max'],  # int(maximum) + 2,
                                    value=slider_config['value'],
                                    updatemode='mouseup',
                                    dots=True,
                                    marks=slider_config['marks']
                                    )]),
                html.Br([])])
        return div

    @staticmethod
    def get_simulation_filter_meta_information_list(working_dict, _list_of_global_dropdown_ids):
        """ Creates a dash_html_components Div with data for a specific slider.
        Args:
            working_dict (dict): Dictionary with data from input files
            _list_of_global_dropdown_ids (list): List of global ids to which dropdown values are added
        Returns:
            global_selection_list (list): List of grouping types and values for specific grouping types
        """
        global_selection_list = []
        range_slider_keys = [slider for slider in working_dict.keys() if slider != DataDictionaryConstants.PARENT_PART]

        for key in range_slider_keys:
                html_div = dcc.Dropdown(id='dropdown_' + key,
                                        options=[{'label': value, 'value': value} for value in
                                                 set([i for i in working_dict[key] if i is not None])],
                                        multi = True,
                                        value=list(set([i for i in working_dict[key] if i is not None])))
                global_selection_list.append(html.P(key))
                global_selection_list.append(html_div)
                _list_of_global_dropdown_ids.append('dropdown_' + key)
        return global_selection_list

    @staticmethod
    def get_filtered_simulation_info():
        """ Creates a list of html parts needed to initialize the filtered simulation info section.
        Returns:
            statistics_selection_list (list): List of html components for the filtered simulation info section.
        """
        statistics_selection_list = []
        statistics_selection_list.append(html.Hr(className='horizontal_line'))
        statistics_selection_list.append(html.Div(className='simulation_number', id='simulation_number'))
        return statistics_selection_list


    @staticmethod
    def get_simulation_without_limits():
        """ Creates a list of html components needed to initialize the simulation without limits section.
        Returns:
            crit_with_missing_limits (list): List of html components for the simulation without limits section.
        """
        crit_with_missing_limits = []
        crit_with_missing_limits.append(html.Div(className='selected_simulation',
                                                 id='selected_simulation_info_criteria_without_limits'))
        return crit_with_missing_limits

    @staticmethod
    def get_diagram_data_graph(id_prefix):
        """ Creates a data diagram graph, a dropdown for selecting a criteria, a "Group by" dropdown in which the type
         of grouping is selected, a check box in which the "separate x-y" option can be selected and 2 dropdowns, that
         are available in case the check box, to select criteria for x and y.
        Args:
            id_prefix (str): The name (id) of the graph being created, diagram_data_graph_1 or diagram_data_graph_2
        Returns:
            children (list): List of html components required for diagram data graph section.
        """
        children = [
            dcc.Graph(id=id_prefix, config={'modeBarButtonsToRemove': ['sendDataToCloud', 'hoverCompareCartesian']}),
            html.Div(className="dropdown_simulationen_selection_div", children=[

                html.Div(className="dropdown_simulation_combined", children=[
                    dcc.Dropdown(id=id_prefix +'_dropdown',
                                 placeholder='Select',
                                 disabled=False)
                ]),
                html.Br(),
                html.Div(className="dropdown_x_y", children=[
                    html.Div(className="switch_button", children=[
                        dcc.Checklist(id=id_prefix + '_check_box_x_y',
                                      options=[{'label': 'separate x-y', 'value': 'selected'}])
                    ]),
                    html.Br(),
                    html.Div(className="dropdown_x", children=[
                        dcc.Dropdown(id=id_prefix + '_dropdown_x',
                                     placeholder='x',
                                     options=Globals.option_for_simulation_dropdown_x_y_,
                                     disabled=False)]),
                    html.Div(className="dropdown_y", children=[
                        dcc.Dropdown(id= id_prefix + '_dropdown_y',
                                     placeholder='y',
                                     options=Globals.option_for_simulation_dropdown_x_y_,
                                     disabled=False)])])
            ]),
            html.Br(),
            html.Div(className="dropdown_legend_selection", children=[
                dcc.Dropdown(id= id_prefix + '_group_by',
                             placeholder='Group by',
                             options=Globals.getLegendDropdowns(),
                             disabled=False)
            ])
        ]
        return children

    @staticmethod
    def get_criteria_data_boxplot():
        """ Creates a criteria data boxplot, a dropdown for selecting criteria and  "Group by" dropdown in which the
        type of grouping is selected.
        Returns:
            children (list): List of html components required for criteria data boxplot
        """
        children = [
            html.Div(className="boxplot_part_div", children=[
                dcc.Graph(id="criteria_data_boxplot",
                          config={'modeBarButtonsToRemove': ['sendDataToCloud', 'hoverCompareCartesian']})]),

            html.Div(className="boxplot_dropdown", children=[
                dcc.Dropdown(id="criteria_data_boxplot_dropdown", multi=True, value=[], disabled=False,
                              options=Globals.options_for_filter_criteria_dropdown_),
                         #html.Br(),
                         #html.Div(className="dropdown_legend_selection_boxplot",
                         #         children=[dcc.Dropdown(id='criteria_data_boxplot_dropdown_group_by',
                         #                   placeholder='Group by',
                         #                   options=Globals.getLegendDropdowns(),
                         #                   disabled=False)])
                      ])]
        return children

    @staticmethod
    def get_criteria_data_2D_scatter():
        """ Creates a criteria data 2D scatter plot, 2 dropdowns to select x and y criteria and  "Group by" dropdown
         in which the type of grouping is selected.
        Returns:
            children (list): List of html components required for criteria data 2D scatter plot.
        """
        children = [
            html.Div(className="criteria_data_2D_scatter", children=[
                dcc.Graph(id='criteria_data_2D_scatter',
                          config={'modeBarButtonsToRemove': ['sendDataToCloud',
                                                             'hoverCompareCartesian']})
            ]),
            html.Div(className="plot_2d_part_div_dropdown", children=[
                dcc.Dropdown(id='criteria_data_2D_scatter_dropdown_x',
                             multi=False,
                             value=[],
                             options=Globals.options_for_filter_criteria_dropdown_additional_label_body_region_),

                html.Br(),
                dcc.Dropdown(id='criteria_data_2D_scatter_dropdown_y',
                             multi=False,
                             value=[],
                             options=Globals.options_for_filter_criteria_dropdown_additional_label_body_region_),
                html.Br(),
                html.Div(className="dropdown_criteria_legend_selection", children=[
                    dcc.Dropdown(id='criteria_data_2D_scatter_group_by',
                                 placeholder='Group by',
                                 options=Globals.getLegendDropdowns(),
                                 disabled=False)
                ])
            ])
        ]
        return children

    @staticmethod
    def get_simulation_filter_meta_information_body_regions_dropdown(working_dict):
        """ Creates html components (radio buttons and dropdown) in the simulation filter meta information section
        that serves to select body regions.
         Args:
             working_dict (dict): Dictionary with data from input files
        Returns:
             list: List of html components required for simulation filter meta information body regions dropdown.
        """
        return [
            html.H6('Part of "Object": '),
            dcc.RadioItems(id='simulation_filter_meta_information_dropdown_radiobuttons',
                           options=[
                               {'label': 'All ', 'value': 'all'},
                               {'label': 'Customize ', 'value': 'custom'}
                           ],
                           value='all',
                           labelStyle={'display': 'inline-block'}),
            dcc.Dropdown(id='simulation_filter_meta_information_dropdown',
                         multi=True,
                         value=[],
                         options=[{'label': key, 'value': key} for key in
                                  list(working_dict[DataDictionaryConstants.PARENT_PART].keys())]),
            html.Hr(className='horizontal_line')]


"""  @staticmethod
    def define_in_depth_tables_divs():
        children = []
        for group_nr, group in enumerate(Globals.group_list_):
            children.append(html.Div(className="table_"+group))"""






